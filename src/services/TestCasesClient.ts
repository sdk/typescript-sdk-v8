/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class TestCasesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 删除测试用例
   * 删除测试用例
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdTestCasesTestCaseId({
    enterpriseId,
    programId,
    testCaseId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 项目 ID
     */
    programId: number,
    /**
     * 测试用例 ID
     */
    testCaseId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 查询方式 (ident/id)
     */
    qt?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/test_cases/{test_case_id}',
      path: {
        'enterprise_id': enterpriseId,
        'test_case_id': testCaseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
        'qt': qt,
      },
    });
  }

}
