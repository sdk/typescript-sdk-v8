/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class TestPlanClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 删除测试计划
   * 删除测试计划
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdTestPlansTestPlanId({
    enterpriseId,
    programId,
    testPlanId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 项目 ID
     */
    programId: number,
    testPlanId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/test_plans/{test_plan_id}',
      path: {
        'enterprise_id': enterpriseId,
        'test_plan_id': testPlanId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
      },
    });
  }

}
