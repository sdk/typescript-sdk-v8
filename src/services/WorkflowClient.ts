/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { TreeNote } from '../models/TreeNote';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class WorkflowClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取任务类型的工作流列表
   * 获取任务类型的工作流列表
   * @returns TreeNote 获取任务类型的工作流列表
   * @throws ApiError
   */
  public getEnterpriseIdWorkflowList({
    enterpriseId,
    accessToken,
    issueTypeIds,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务状态的 id，如存在多个使用英文逗号 (,) 分割
     */
    issueTypeIds?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<TreeNote> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/workflow/list',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'issue_type_ids': issueTypeIds,
        'page': page,
        'per_page': perPage,
      },
    });
  }

}
