/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Issue } from '../models/Issue';
import type { Project } from '../models/Project';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class KooderClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 仓库查询
   * 仓库查询
   * @returns Project 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdKooderProject({
    enterpriseId,
    search,
    accessToken,
    page = 1,
    perPage,
    language,
    projectIds,
    namespaceIds,
    visibility,
    scope,
    scene,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 搜索内容
     */
    search: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 仓库语言
     */
    language?: string,
    /**
     * 仓库 id，用英文逗号分隔
     */
    projectIds?: string,
    /**
     * 仓库命名空间 id，用英文逗号分隔
     */
    namespaceIds?: string,
    /**
     * 仓库开源属性，公开：0，私有：1，内源：2，可多选，用英文逗号分隔
     */
    visibility?: string,
    /**
     * 检索范围，null：默认，仓库名、仓库描述，human_name：仓库名称
     */
    scope?: string,
    /**
     * 触发场景，用户做分析统计
     */
    scene?: number,
  }): CancelablePromise<Project> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/kooder/project',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'page': page,
        'per_page': perPage,
        'language': language,
        'project_ids': projectIds,
        'namespace_ids': namespaceIds,
        'visibility': visibility,
        'scope': scope,
        'scene': scene,
      },
    });
  }

  /**
   * issue 查询
   * issue 查询
   * @returns Issue 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdKooderIssue({
    enterpriseId,
    search,
    accessToken,
    page = 1,
    perPage,
    issueTypeIds,
    projectId,
    programId,
    scope,
    scene,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 搜索内容
     */
    search: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 任务类型 id，用英文逗号分隔
     */
    issueTypeIds?: string,
    /**
     * 仓库 id，用英文逗号分隔
     */
    projectId?: string,
    /**
     * 项目 id，用英文逗号分隔
     */
    programId?: string,
    /**
     * 检索范围，null：默认，任务标题、任务描述，title：任务标题
     */
    scope?: string,
    /**
     * 触发场景，用户做分析统计
     */
    scene?: number,
  }): CancelablePromise<Issue> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/kooder/issue',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'page': page,
        'per_page': perPage,
        'issue_type_ids': issueTypeIds,
        'project_id': projectId,
        'program_id': programId,
        'scope': scope,
        'scene': scene,
      },
    });
  }

  /**
   * code 查询
   * code 查询
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdKooderCode({
    enterpriseId,
    search,
    accessToken,
    page = 1,
    perPage,
    language,
    projectId,
    visibility,
    scope,
    scene,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 搜索内容
     */
    search: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 仓库语言
     */
    language?: string,
    /**
     * 仓库 id，用英文逗号分隔
     */
    projectId?: string,
    /**
     * 仓库开源属性，公开：0，私有：1，内源：2，可多选，用英文逗号分隔
     */
    visibility?: string,
    /**
     * 检索范围，null：默认，仓库文件名称、仓库文件内容，location：仓库文件名称
     */
    scope?: string,
    /**
     * 触发场景，用户做分析统计
     */
    scene?: number,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/kooder/code',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'page': page,
        'per_page': perPage,
        'language': language,
        'project_id': projectId,
        'visibility': visibility,
        'scope': scope,
        'scene': scene,
      },
    });
  }

  /**
   * 知识库查询
   * 知识库查询
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdKooderDocs({
    enterpriseId,
    search,
    accessToken,
    page = 1,
    perPage,
    programId,
    scene,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 搜索内容
     */
    search: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 项目 id，用英文逗号分隔
     */
    programId?: string,
    /**
     * 触发场景，用户做分析统计
     */
    scene?: number,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/kooder/docs',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'page': page,
        'per_page': perPage,
        'program_id': programId,
        'scene': scene,
      },
    });
  }

}
