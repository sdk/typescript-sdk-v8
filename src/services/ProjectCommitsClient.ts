/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CommitBranch } from '../models/CommitBranch';
import type { CommitCompare } from '../models/CommitCompare';
import type { CommitDetail } from '../models/CommitDetail';
import type { CommitList } from '../models/CommitList';
import type { CommitNote } from '../models/CommitNote';
import type { CommitNoteDetail } from '../models/CommitNoteDetail';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ProjectCommitsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * commits 列表
   * commits 列表
   * @returns CommitList commits 列表
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCommitsRef({
    enterpriseId,
    projectId,
    ref,
    accessToken,
    qt,
    author,
    startDate,
    endDate,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 提交起始的 SHA 值或者分支名及文件路径。默认：仓库的默认分支
     */
    ref: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 提交作者的邮箱或个人空间地址 (username/login)
     */
    author?: string,
    /**
     * 提交的起始时间，时间格式为 ISO 8601
     */
    startDate?: string,
    /**
     * 提交的最后时间，时间格式为 ISO 8601
     */
    endDate?: string,
    /**
     * Commit message 搜索关键字
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<CommitList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/commits/{ref}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'ref': ref,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'author': author,
        'start_date': startDate,
        'end_date': endDate,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * commit 详情
   * commit 详情
   * @returns CommitDetail commit 详情
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCommitCommitId({
    enterpriseId,
    projectId,
    commitId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<CommitDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * commit revert
   * commit revert
   * @returns any 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdCommitCommitIdRevert({
    enterpriseId,
    projectId,
    commitId,
    startBranch,
    accessToken,
    qt,
    createPullRequest,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * start branch
     */
    startBranch: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 是否创建代码评审
     */
    createPullRequest?: boolean,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}/revert',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'start_branch': startBranch,
        'create_pull_request': createPullRequest,
      },
    });
  }

  /**
   * Commit 评论列表
   * Commit 评论列表
   * @returns CommitNote Commit 评论列表
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCommitCommitIdNotes({
    enterpriseId,
    projectId,
    commitId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<CommitNote> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}/notes',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 评论 Commit
   * 评论 Commit
   * @returns CommitNote 评论 Commit
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdCommitCommitIdNotes({
    enterpriseId,
    projectId,
    commitId,
    body,
    accessToken,
    qt,
    lineCode,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * 评论内容
     */
    body: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 代码行标记值
     */
    lineCode?: string,
  }): CancelablePromise<CommitNote> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}/notes',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'body': body,
        'line_code': lineCode,
      },
    });
  }

  /**
   * Commit 评论列表树
   * Commit 评论列表树
   * @returns any Commit 评论列表树
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCommitCommitIdNotesTrees({
    enterpriseId,
    projectId,
    commitId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}/notes/trees',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 评论、回复 Commit
   * 评论、回复 Commit
   * @returns CommitNoteDetail 评论、回复 Commit
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdCommitCommitIdNotesTrees({
    enterpriseId,
    projectId,
    commitId,
    body,
    accessToken,
    qt,
    lineCode,
    replyId,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * 评论内容
     */
    body: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 代码行标记值
     */
    lineCode?: string,
    /**
     * 回复的上级评论 id
     */
    replyId?: number,
  }): CancelablePromise<CommitNoteDetail> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}/notes/trees',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'body': body,
        'line_code': lineCode,
        'reply_id': replyId,
      },
    });
  }

  /**
   * 获取 commit 详情中差异较大的文件内容
   * 获取 commit 详情中差异较大的文件内容
   * @returns any 获取 commit 详情中差异较大的文件内容
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCommitCommitIdDiffForPath({
    enterpriseId,
    projectId,
    commitId,
    fileIdentifier,
    newPath,
    oldPath,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * git 文件标识符
     */
    fileIdentifier: string,
    /**
     * 旧路径
     */
    newPath: string,
    /**
     * 新路径
     */
    oldPath: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}/diff_for_path',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'file_identifier': fileIdentifier,
        'new_path': newPath,
        'old_path': oldPath,
      },
    });
  }

  /**
   * 获取 commit 的分支和 tag
   * 获取 commit 的分支和 tag
   * @returns CommitBranch 获取 commit 的分支和 tag
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCommitCommitIdBranches({
    enterpriseId,
    projectId,
    commitId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * commit sha
     */
    commitId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查��参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<CommitBranch> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/commit/{commit_id}/branches',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'commit_id': commitId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 对比 commit
   * 对比 commit
   * @returns CommitCompare 对比 commit
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCompareFromTo({
    enterpriseId,
    projectId,
    from,
    to,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 起始 commit
     */
    from: string,
    /**
     * 结束 commit
     */
    to: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<CommitCompare> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/compare/{from}...{to}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'from': from,
        'to': to,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

}
