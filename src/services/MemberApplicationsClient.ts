/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BulkResponseWithKey } from '../models/BulkResponseWithKey';
import type { Enterprise } from '../models/Enterprise';
import type { MemberApplication } from '../models/MemberApplication';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class MemberApplicationsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 企业/仓库/团队的成员加入申请列表
   * 企业/仓库/团队的成员加入申请列表
   * @returns MemberApplication 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMemberApplications({
    targetType,
    targetId,
    enterpriseId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 申请加入的类型：Enterprise,Project,Group
     */
    targetType: string,
    /**
     * 申请加入的主体 ID
     */
    targetId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<MemberApplication>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/member_applications',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'target_type': targetType,
        'target_id': targetId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 强制审核开关
   * 强制审核开关
   * @returns Enterprise 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdMemberApplicationsSetForceVerify({
    on,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 是否开启强制审核：开启此选项后，所有邀请 (包括之前已生成的邀请) 都将需要管理员审核通过后才可加入企业
     */
    on: boolean,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Enterprise> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/member_applications/set_force_verify',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'on': on,
      },
    });
  }

  /**
   * 处理申请记录
   * 处理申请记录
   * @returns MemberApplication 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMemberApplicationsMemberApplicationId({
    memberApplicationId,
    state,
    enterpriseId,
    accessToken,
    roleId,
  }: {
    /**
     * 申请记录 id
     */
    memberApplicationId: number,
    /**
     * 通过/拒绝
     */
    state: 'pass' | 'reject',
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 角色 id。企业角色列表 scope: can_invite
     */
    roleId?: number,
  }): CancelablePromise<MemberApplication> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/member_applications/{member_application_id}',
      path: {
        'member_application_id': memberApplicationId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'role_id': roleId,
        'state': state,
      },
    });
  }

  /**
   * 批量处理申请记录
   * 批量处理申请记录
   * @returns BulkResponseWithKey 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdMemberApplicationsBulkOperate({
    selectedIds,
    state,
    enterpriseId,
    accessToken,
    roleId,
  }: {
    /**
     * 申请记录 id, 逗号隔开
     */
    selectedIds: string,
    /**
     * 通过/拒绝
     */
    state: 'pass' | 'reject',
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 角色 id。企业角色列表 scope: can_invite
     */
    roleId?: number,
  }): CancelablePromise<BulkResponseWithKey> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/member_applications/bulk_operate',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'selected_ids': selectedIds,
        'role_id': roleId,
        'state': state,
      },
    });
  }

}
