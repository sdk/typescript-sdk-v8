/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Enterprise } from '../models/Enterprise';
import type { EnterpriseAuth } from '../models/EnterpriseAuth';
import type { EnterpriseNavigate } from '../models/EnterpriseNavigate';
import type { EnterpriseQuota } from '../models/EnterpriseQuota';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class EnterprisesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 企业导航栏设置
   * 企业导航栏设���
   * @returns EnterpriseNavigate 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdNavigates({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<EnterpriseNavigate>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/navigates',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新企业导航栏设置
   * 更新企业导航栏设置
   * @returns EnterpriseNavigate 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdNavigates({
    enterpriseId,
    accessToken,
    navigatesId,
    navigatesIcon,
    navigatesActive,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 导航卡 id
     */
    navigatesId?: Array<number>,
    /**
     * 导航卡 icon
     */
    navigatesIcon?: Array<number>,
    /**
     * 是否显示
     */
    navigatesActive?: Array<boolean>,
  }): CancelablePromise<Array<EnterpriseNavigate>> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/navigates',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'navigates[id]': navigatesId,
        'navigates[icon]': navigatesIcon,
        'navigates[active]': navigatesActive,
      },
    });
  }

  /**
   * 获取授权用户的企业列表
   * 获取授权用户的企业列表
   * @returns Enterprise 返回格式
   * @throws ApiError
   */
  public getList({
    accessToken,
    sort,
    direction = 'desc',
    page = 1,
    perPage,
  }: {
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (name || created_at)
     */
    sort?: 'name' | 'created_at',
    /**
     * 可选。升序/降序
     */
    direction?: 'asc' | 'desc',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Enterprise>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/list',
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取企业基础信息
   * 获取企业基础信息
   * @returns Enterprise 返回格式
   * @throws ApiError
   */
  public getEnterpriseId({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Enterprise> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新企业基础信息
   * 更新企业基础信息
   * @returns Enterprise 返回格式
   * @throws ApiError
   */
  public putEnterpriseId({
    enterpriseId,
    accessToken,
    name,
    _public,
    email,
    website,
    description,
    phone,
    watermark,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 企业名称
     */
    name?: string,
    /**
     * 企业属性 (0: 不公开 1: 公开）
     */
    _public?: string,
    /**
     * 企业邮箱
     */
    email?: string,
    /**
     * 企业网站
     */
    website?: string,
    /**
     * 企业简介
     */
    description?: string,
    /**
     * 手机号码
     */
    phone?: string,
    /**
     * 企业水印
     */
    watermark?: boolean,
  }): CancelablePromise<Enterprise> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'public': _public,
        'email': email,
        'website': website,
        'description': description,
        'phone': phone,
        'watermark': watermark,
      },
    });
  }

  /**
   * 更新企业公告内容
   * 更新企业公告内容
   * @returns Enterprise 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdNotices({
    enterpriseId,
    content,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 企业公告的内容
     */
    content: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Enterprise> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/notices',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'content': content,
      },
    });
  }

  /**
   * 获取当前企业的配额信息
   * 获取当前企业的配额信息
   * @returns EnterpriseQuota 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdQuota({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<EnterpriseQuota> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/quota',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取授权用户在企业拥有的权限
   * 获取授权用户在企业拥有的权限
   * @returns EnterpriseAuth 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdAuths({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<EnterpriseAuth> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/auths',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
