/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DeployKeyLog } from '../models/DeployKeyLog';
import type { DocNodeLog } from '../models/DocNodeLog';
import type { EnterpriseRoleLog } from '../models/EnterpriseRoleLog';
import type { GroupManageLog } from '../models/GroupManageLog';
import type { MemberManageLog } from '../models/MemberManageLog';
import type { ProgramManageLog } from '../models/ProgramManageLog';
import type { ProjectAccessLog } from '../models/ProjectAccessLog';
import type { ProjectGroupManageLog } from '../models/ProjectGroupManageLog';
import type { ProjectManageLog } from '../models/ProjectManageLog';
import type { SecuritySettingLog } from '../models/SecuritySettingLog';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class AdminLogsStatisticClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 仓库管理日志
   * 仓库管理日志
   * @returns ProjectManageLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsProjectsLog({
    enterpriseId,
    accessToken,
    projectName,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 仓库 path_with_namespace: namespace.path/project.path
     */
    projectName?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProjectManageLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/projects_log',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'project_name': projectName,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 仓库代码日志
   * 仓库代码日志
   * @returns ProjectAccessLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsProjectsAccessLog({
    enterpriseId,
    accessToken,
    groupPath,
    projectName,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 团队 path
     */
    groupPath?: string,
    /**
     * 仓库 path_with_namespace: namespace.path/project.path
     */
    projectName?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProjectAccessLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/projects_access_log',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'group_path': groupPath,
        'project_name': projectName,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 项目管理日志
   * 项目管理日志
   * @returns ProgramManageLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsProgramsLog({
    enterpriseId,
    accessToken,
    member,
    programName,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 项目名称
     */
    programName?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProgramManageLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/programs_log',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'member': member,
        'program_name': programName,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 仓库组管理日志
   * 仓库组管理日志
   * @returns ProjectGroupManageLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsProjectGroupsLog({
    enterpriseId,
    accessToken,
    member,
    projectGroupId,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 仓库组 id
     */
    projectGroupId?: number,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProjectGroupManageLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/project_groups_log',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'member': member,
        'project_group_id': projectGroupId,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 成员管理日志
   * 成员管理日志
   * @returns MemberManageLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsMembersLog({
    enterpriseId,
    accessToken,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<MemberManageLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/members_log',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 团队管理日志
   * 团队管理日志
   * @returns GroupManageLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsGroupsLog({
    enterpriseId,
    accessToken,
    groupName,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 团队名称
     */
    groupName?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<GroupManageLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/groups_log',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'group_name': groupName,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 安全与告警管理日志
   * 安全与告警管理日志
   * @returns SecuritySettingLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsSecuritySettingLog({
    enterpriseId,
    accessToken,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<SecuritySettingLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/security_setting_log',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 企业角色管理日志
   * 企业角色管理日志
   * @returns EnterpriseRoleLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsEnterpriseRoleLogs({
    enterpriseId,
    accessToken,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<EnterpriseRoleLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/enterprise_role_logs',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 企业部署公钥管理日志
   * 企业部署公钥管理日志
   * @returns DeployKeyLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsDeployKeyLogs({
    enterpriseId,
    accessToken,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<DeployKeyLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/deploy_key_logs',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 企业文档管理日志
   * 企业文档管理日志
   * @returns DocNodeLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLogStatisticsDocNodeLogs({
    enterpriseId,
    accessToken,
    member,
    startDate,
    endDate,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 username
     */
    member?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<DocNodeLog>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/log_statistics/doc_node_logs',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'member': member,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
      },
    });
  }

}
