/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { UserWorkloadsList } from '../models/UserWorkloadsList';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class WorkloadsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 查询成员工时明细
   * 查询成员工时明细
   * @returns UserWorkloadsList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWorkloads({
    enterpriseId,
    accessToken,
    programIds,
    userIds,
    registeredDates,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 ID，以逗号分隔。例："1,2,3"
     */
    programIds?: string,
    /**
     * 用户 ID，以逗号分隔。例："1,2,3"
     */
    userIds?: string,
    /**
     * 工时登记日期范围。以逗号分隔。例："2022-01-01,2022-02-01"
     */
    registeredDates?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<UserWorkloadsList> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/workloads',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_ids': programIds,
        'user_ids': userIds,
        'registered_dates': registeredDates,
        'page': page,
        'per_page': perPage,
      },
    });
  }

}
