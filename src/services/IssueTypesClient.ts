/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Issue } from '../models/Issue';
import type { IssueState } from '../models/IssueState';
import type { IssueStateForMigration } from '../models/IssueStateForMigration';
import type { IssueType } from '../models/IssueType';
import type { IssueTypeInEnterprise } from '../models/IssueTypeInEnterprise';
import type { IssueTypeInProgram } from '../models/IssueTypeInProgram';
import type { IssueTypeWithStateRef } from '../models/IssueTypeWithStateRef';
import type { Program } from '../models/Program';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class IssueTypesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取任务类型列表
   * 获取任务类型列表
   * @returns IssueType 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypes({
    enterpriseId,
    accessToken,
    sort,
    direction,
    programId,
    scope,
    category,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (created_at、updated_at、serial)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 项目 ID
     */
    programId?: number,
    /**
     * 查询范围：all, customize
     */
    scope?: string,
    /**
     * 任务类型属性
     */
    category?: 'task' | 'bug' | 'requirement' | 'feature',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<IssueType>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'program_id': programId,
        'scope': scope,
        'category': category,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新增任务类型
   * 新增任务类型
   * @returns IssueType 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdIssueTypes({
    enterpriseId,
    title,
    accessToken,
    stateIds,
    progressingIds,
    closedIds,
    description,
    rejectedIds,
    template,
    category,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型名称
     */
    title: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务状态 ID(注意预期顺序排列), 逗号隔开
     */
    stateIds?: string,
    /**
     * 状态属性：进行中。任务状态 ID, 逗号隔开
     */
    progressingIds?: string,
    /**
     * 状态属性：已完成。任务状态 ID, 逗号隔开
     */
    closedIds?: string,
    /**
     * 任务类型描述
     */
    description?: string,
    /**
     * 状态属性：已拒绝。任务状态 ID, 逗号隔开
     */
    rejectedIds?: string,
    /**
     * 任务类型模版
     */
    template?: string,
    /**
     * 任务类型属性
     */
    category?: string,
  }): CancelablePromise<IssueType> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issue_types',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'state_ids': stateIds,
        'progressing_ids': progressingIds,
        'closed_ids': closedIds,
        'description': description,
        'rejected_ids': rejectedIds,
        'template': template,
        'category': category,
        'title': title,
      },
    });
  }

  /**
   * 获取企业任务类型列表
   * 获取企业任务类型列表
   * @returns IssueTypeInEnterprise 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesEnterpriseIssueTypes({
    enterpriseId,
    accessToken,
    sort,
    direction,
    category,
    search,
    state,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (created_at、updated_at、serial)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 任务类型属性，多个分类以英文逗号分隔
     */
    category?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 项目的任务类型的状态：1-开启，0-关闭
     */
    state?: 1 | 0,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<IssueTypeInEnterprise>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/enterprise_issue_types',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'category': category,
        'search': search,
        'state': state,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取项目任务类型列表
   * 获取项目任务类型列表
   * @returns IssueTypeInProgram 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesProgramIssueTypes({
    enterpriseId,
    programId,
    accessToken,
    sort,
    direction,
    category,
    requirementNeedTask,
    search,
    unused,
    state,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 项目 ID
     */
    programId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (created_at、updated_at、serial)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 任务类型属性
     */
    category?: string,
    /**
     * 需求是否需要返回任务类型
     */
    requirementNeedTask?: boolean,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 是否只获取未添加到指定项目的任务类型
     */
    unused?: boolean,
    /**
     * 项目的任务类型的状态：1-开启，0-关闭
     */
    state?: 1 | 0,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<IssueTypeInProgram>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/program_issue_types',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'program_id': programId,
        'category': category,
        'requirement_need_task': requirementNeedTask,
        'search': search,
        'unused': unused,
        'state': state,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取项目下的默认类型
   * 获取项目下的默认类型
   * @returns IssueTypeInProgram 获取项目下的默认类型
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesDefault({
    enterpriseId,
    programId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 项目 ID
     */
    programId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueTypeInProgram> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/default',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
      },
    });
  }

  /**
   * 获取单个任务类型下的任务状态引用
   * 获取单个任务类型下的任务状态引用
   * @returns IssueState 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdIssueStateRefs({
    enterpriseId,
    issueTypeId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型的 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<IssueState>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/issue_state_refs',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取单个任务类型下的任务状态列表
   * 获取单个任务类型下的任务状态列表
   * @returns IssueState 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdIssueStates({
    enterpriseId,
    issueTypeId,
    accessToken,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型的 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (created_at、updated_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<IssueState>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/issue_states',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 更新任务类型排序
   * 更新任务类型排序
   * @returns IssueType 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdIssueTypesChangeSerial({
    enterpriseId,
    typeIds,
    accessToken,
    issueTypeCategory,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 ids(预期顺序), 逗号隔开
     */
    typeIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务类型属性
     */
    issueTypeCategory?: 'task' | 'bug' | 'requirement',
  }): CancelablePromise<IssueType> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issue_types/change_serial',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'type_ids': typeIds,
        'issue_type_category': issueTypeCategory,
      },
    });
  }

  /**
   * 更新项目下任务类型排序
   * 更新项目下任务类型排序
   * @returns any 更新项目下任务类型排序
   * @throws ApiError
   */
  public putEnterpriseIdIssueTypesChangeProgramSerial({
    enterpriseId,
    issueTypeIds,
    programId,
    accessToken,
    issueTypeCategory,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 ids(预期顺序), 英文逗号隔开
     */
    issueTypeIds: string,
    /**
     * 项目 ID
     */
    programId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务类型属性
     */
    issueTypeCategory?: 'task' | 'bug' | 'requirement',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issue_types/change_program_serial',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'issue_type_ids': issueTypeIds,
        'program_id': programId,
        'issue_type_category': issueTypeCategory,
      },
    });
  }

  /**
   * 添加多个任务类型到项目
   * 添加多个任务类型到项目
   * @returns IssueTypeInProgram 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdIssueTypesPrograms({
    enterpriseId,
    issueTypeIds,
    programId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 ids, 英文逗号隔开
     */
    issueTypeIds: string,
    /**
     * 项目 ID
     */
    programId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueTypeInProgram> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issue_types/programs',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'issue_type_ids': issueTypeIds,
        'program_id': programId,
      },
    });
  }

  /**
   * 任务类型详情 (状态管理)
   * 任务类型详情 (状态管理)
   * @returns IssueTypeWithStateRef 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeId({
    enterpriseId,
    issueTypeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueTypeWithStateRef> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新任务类型
   * 更新任务类型
   * @returns IssueType 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdIssueTypesIssueTypeId({
    enterpriseId,
    issueTypeId,
    accessToken,
    stateIds,
    progressingIds,
    closedIds,
    description,
    rejectedIds,
    template,
    category,
    title,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务状态 ID(注意预期顺序排列), 逗号隔开
     */
    stateIds?: string,
    /**
     * 状态属性：进行中。任务状态 ID, 逗号隔开
     */
    progressingIds?: string,
    /**
     * 状态属性：已完成。任务状态 ID, 逗号隔开
     */
    closedIds?: string,
    /**
     * 任务类型描述
     */
    description?: string,
    /**
     * 状态属性：已拒绝。任务状态 ID, 逗号隔开
     */
    rejectedIds?: string,
    /**
     * 任务类型模版
     */
    template?: string,
    /**
     * 任务类型属性
     */
    category?: string,
    /**
     * 任务类型名称
     */
    title?: string,
  }): CancelablePromise<IssueType> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issue_types/{issue_type_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      formData: {
        'access_token': accessToken,
        'state_ids': stateIds,
        'progressing_ids': progressingIds,
        'closed_ids': closedIds,
        'description': description,
        'rejected_ids': rejectedIds,
        'template': template,
        'category': category,
        'title': title,
      },
    });
  }

  /**
   * 删除任务类型
   * 删除任务类型
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssueTypesIssueTypeId({
    enterpriseId,
    issueTypeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issue_types/{issue_type_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 指定任务类型，任务状态下任务列表
   * 指定任务类型，任务状态下任务列表
   * @returns Issue 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdIssueStateIdIssues({
    enterpriseId,
    issueTypeId,
    issueStateId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 任务状态 id
     */
    issueStateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Issue> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/{issue_state_id}/issues',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
        'issue_state_id': issueStateId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取企业任务状态的流转关系
   * 获取企业任务状态的流转关系
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdWorkflow({
    enterpriseId,
    issueTypeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/workflow',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 变更任务状态流转关系
   * 变更任务状态流转关系
   * @returns any 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdIssueTypesIssueTypeIdWorkflow({
    enterpriseId,
    issueTypeId,
    accessToken,
    relationsCurrentStateId,
    relationsNextStateId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前状态 id
     */
    relationsCurrentStateId?: Array<number>,
    /**
     * 下一个任务状态的 id
     */
    relationsNextStateId?: Array<number>,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/workflow',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      formData: {
        'access_token': accessToken,
        'relations[current_state_id]': relationsCurrentStateId,
        'relations[next_state_id]': relationsNextStateId,
      },
    });
  }

  /**
   * 获取单个任务类型下的项目列表
   * 获取单个任务类型下的项目列表
   * @returns Program 获取单个任务类型下的项目列表
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdPrograms({
    enterpriseId,
    issueTypeId,
    accessToken,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Program>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/programs',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 添加单个任务类型到项目
   * 添加单个任务类型到项目
   * @returns Program 添加单个任务类型到项目
   * @throws ApiError
   */
  public postEnterpriseIdIssueTypesIssueTypeIdPrograms({
    enterpriseId,
    issueTypeId,
    programIds,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 项目 ids, 英文逗号隔开
     */
    programIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<Program>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/programs',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      formData: {
        'access_token': accessToken,
        'program_ids': programIds,
      },
    });
  }

  /**
   * 获取任务类型下未添加的项目列表
   * 获取任务类型下未添加的项目列表
   * @returns Program 获取任务类型下未添加的项目列表
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdProgramsUnused({
    enterpriseId,
    issueTypeId,
    accessToken,
    search,
    targetTypeId,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 目标类型 ID
     */
    targetTypeId?: number,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Program>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/programs_unused',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'target_type_id': targetTypeId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 移除项目下任务类型
   * 移除项目下任务类型
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssueTypesIssueTypeIdProgramSettingProgramId({
    enterpriseId,
    issueTypeId,
    programId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    programId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/program_setting/{program_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
        'program_id': programId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 设置为项目下的默认类型
   * 设置为项目下的默认类型
   * @returns IssueTypeInProgram 设置为项目下的默认类型
   * @throws ApiError
   */
  public putEnterpriseIdIssueTypesIssueTypeIdSetDefault({
    enterpriseId,
    issueTypeId,
    programId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 项目 ID
     */
    programId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueTypeInProgram> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/set_default',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      formData: {
        'access_token': accessToken,
        'program_id': programId,
      },
    });
  }

  /**
   * 获取状态替换目标状态可选下拉列表
   * 获取状态替换目标状态可选下拉列表
   * @returns IssueStateForMigration 数据返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdIssueStateMigrateTargetStates({
    enterpriseId,
    issueTypeId,
    sourceStateId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 源任务状态 ID
     */
    sourceStateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<IssueStateForMigration> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/issue_state_migrate/target_states',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
        'source_state_id': sourceStateId,
      },
    });
  }

  /**
   * 获取状态迁移自动分配的状态
   * 获取状态迁移自动分配的状态
   * @returns IssueState 数据返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdIssueStateMigrateSubstituteState({
    enterpriseId,
    issueTypeId,
    sourceStateId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 源任务状态 ID
     */
    sourceStateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueState> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/issue_state_migrate/substitute_state',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
        'source_state_id': sourceStateId,
      },
    });
  }

  /**
   * 工作项状态替换
   * 工作项状态替换
   * @returns any 数据返回格式
   * @throws ApiError
   */
  public postEnterpriseIdIssueTypesIssueTypeIdIssueStateMigrateMigrate({
    enterpriseId,
    issueTypeId,
    sourceStateId,
    targetStateId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 源任务状态 ID
     */
    sourceStateId: number,
    /**
     * 目标任务状态 ID
     */
    targetStateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/issue_state_migrate/migrate',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      formData: {
        'access_token': accessToken,
        'source_state_id': sourceStateId,
        'target_state_id': targetStateId,
      },
    });
  }

  /**
   * 获取状态替换进度
   * 获取状态替换进度
   * @returns any 数据返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueTypesIssueTypeIdIssueStateMigrateFetchProgress({
    enterpriseId,
    issueTypeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务类型 id
     */
    issueTypeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_types/{issue_type_id}/issue_state_migrate/fetch_progress',
      path: {
        'enterprise_id': enterpriseId,
        'issue_type_id': issueTypeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
