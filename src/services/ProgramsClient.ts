/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Event } from '../models/Event';
import type { Group } from '../models/Group';
import type { Issue } from '../models/Issue';
import type { Member } from '../models/Member';
import type { Program } from '../models/Program';
import type { ProgramComponent } from '../models/ProgramComponent';
import type { ProgramList } from '../models/ProgramList';
import type { ProgramWithCollection } from '../models/ProgramWithCollection';
import type { Project } from '../models/Project';
import type { ProjectsList } from '../models/ProjectsList';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ProgramsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取项目列表
   * 获取项目列表
   * @returns ProgramList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdPrograms({
    enterpriseId,
    accessToken,
    type,
    sort,
    priorityTopped,
    direction,
    status,
    category,
    assigneeIds,
    issueTypeId,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 筛选不同类型的项目列表。我参与的：joined; 我负责的：assigned; 我创建的：created; 我星标的：only_star
     */
    type?: string,
    /**
     * 排序字段 (created_at: 创建时间 updated_at: 更新时间 users_count: 成员数 projects_count: 仓库数 issues_count: 任务数 accessed_at: 访问时间 name: 项目名称)
     */
    sort?: string,
    /**
     * 是否按照置顶优先排序
     */
    priorityTopped?: boolean,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 项目状态（0:开始 1:暂停 2:关闭）, 逗号分隔，如：0,1
     */
    status?: string,
    /**
     * 项目类别（all: 所有，common: 普通，kanban: 看板）, 支持多种类型，以，分隔，如：common,kanban
     */
    category?: string,
    /**
     * 项目负责人 ID，多个负责人使用英文 , 隔开
     */
    assigneeIds?: string,
    /**
     * 任务类型 ID
     */
    issueTypeId?: number,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProgramList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'type': type,
        'sort': sort,
        'priority_topped': priorityTopped,
        'direction': direction,
        'status': status,
        'category': category,
        'assignee_ids': assigneeIds,
        'issue_type_id': issueTypeId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新建项目
   * 新建项目
   * @returns Program 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdPrograms({
    name,
    enterpriseId,
    accessToken,
    description,
    assigneeId,
    outsourced,
    status,
    color,
    ident,
    programExtraFieldsProgramFieldId,
    programExtraFieldsValue,
    category,
    projectIds,
    importProjectUsers,
    importProjectIssues,
    importProjectMilestones,
    userIds,
    groupIds,
  }: {
    /**
     * 项目名称
     */
    name: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目简介
     */
    description?: string,
    /**
     * 负责人 ID
     */
    assigneeId?: string,
    /**
     * 项目类型：内部 (false)/外包 (true) 项目
     */
    outsourced?: boolean,
    /**
     * 项目状态:（0:开始 1:暂停 2:关闭）
     */
    status?: 0 | 1 | 2,
    /**
     * 颜色
     */
    color?: string,
    /**
     * 项目编号
     */
    ident?: string,
    /**
     * 项目自定义字段 id（项目字段设置列表的字段 id）
     */
    programExtraFieldsProgramFieldId?: Array<number>,
    /**
     * 自定义字段的值（options 类型的字段传对应选项的 id，使用 , 隔开，如"1,2,3"）
     */
    programExtraFieldsValue?: Array<string>,
    /**
     * 项目类型 (standard、scrum、kanban)
     */
    category?: string,
    /**
     * 关联仓库 ids，逗号隔开
     */
    projectIds?: string,
    /**
     * 是否导入仓库成员
     */
    importProjectUsers?: boolean,
    /**
     * 是否导入仓库任务
     */
    importProjectIssues?: boolean,
    /**
     * 是否导入仓库里程碑
     */
    importProjectMilestones?: boolean,
    /**
     * 成员 ids，逗号隔开
     */
    userIds?: string,
    /**
     * 团队 ids，逗号隔开
     */
    groupIds?: string,
  }): CancelablePromise<Program> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/programs',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'description': description,
        'assignee_id': assigneeId,
        'outsourced': outsourced,
        'status': status,
        'color': color,
        'ident': ident,
        'program_extra_fields[program_field_id]': programExtraFieldsProgramFieldId,
        'program_extra_fields[value]': programExtraFieldsValue,
        'category': category,
        'project_ids': projectIds,
        'import_project_users': importProjectUsers,
        'import_project_issues': importProjectIssues,
        'import_project_milestones': importProjectMilestones,
        'user_ids': userIds,
        'group_ids': groupIds,
      },
    });
  }

  /**
   * 项目列表 - 支持自定义字段的筛选
   * 项目列表 - 支持自定义字段的筛选
   * @returns ProgramList 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProgramsFilter({
    property,
    filterConditionsProperty,
    enterpriseId,
    accessToken,
    direction,
    sortField,
    programFieldId,
    priorityTopped,
    search,
    filterConditionsComparator,
    filterConditionsValue,
    filterConditionsProgramFieldType,
    filterConditionsProgramFieldId,
    page = 1,
    perPage,
  }: {
    /**
     * 排序的字段类型：项目自定义字段-program_field, 表字段-table_field
     */
    property: 'table_field' | 'program_field',
    /**
     * 筛选类型
     */
    filterConditionsProperty: Array<string>,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 排序字段 (created_at: 创建时间|updated_at: 更新时间|name: 项目名称|property 为 table_field 时生效)
     */
    sortField?: string,
    /**
     * 项目自定义字段 ID, property 为 program_field 时需要填
     */
    programFieldId?: number,
    /**
     * 是否按照置顶优先排序
     */
    priorityTopped?: boolean,
    /**
     * 搜索关键词
     */
    search?: string,
    /**
     * 比较符
     */
    filterConditionsComparator?: Array<string>,
    /**
     * 值
     */
    filterConditionsValue?: Array<string>,
    /**
     * 自定义字段值类型
     */
    filterConditionsProgramFieldType?: Array<string>,
    /**
     * 自定义字段 id
     */
    filterConditionsProgramFieldId?: Array<number>,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProgramList>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/programs/filter',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'direction': direction,
        'sort_field': sortField,
        'program_field_id': programFieldId,
        'property': property,
        'priority_topped': priorityTopped,
        'search': search,
        'filter_conditions[property]': filterConditionsProperty,
        'filter_conditions[comparator]': filterConditionsComparator,
        'filter_conditions[value]': filterConditionsValue,
        'filter_conditions[program_field_type]': filterConditionsProgramFieldType,
        'filter_conditions[program_field_id]': filterConditionsProgramFieldId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取用户最近浏览的项目集合
   * 获取用户最近浏览的项目集合
   * @returns ProgramList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsMine({
    enterpriseId,
    accessToken,
    programIds,
    status,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目的 id 列表，如有多个用逗号分割。eg: 1,2,3
     */
    programIds?: string,
    /**
     * 项目状态（0:开始 1:暂停 2:关闭）, 逗号分隔，如：0,1
     */
    status?: string,
  }): CancelablePromise<Array<ProgramList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/mine',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_ids': programIds,
        'status': status,
      },
    });
  }

  /**
   * 获取未立项项目
   * 获取未立项项目
   * @returns any 获取未立项项目
   * @throws ApiError
   */
  public getEnterpriseIdProgramsUnset({
    enterpriseId,
    accessToken,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/unset',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取项目详情
   * 获取项目详情
   * @returns ProgramWithCollection 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramId({
    programId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<ProgramWithCollection>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新项目
   * 更新项目
   * @returns Program 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdProgramsProgramId({
    programId,
    name,
    enterpriseId,
    accessToken,
    description,
    assigneeId,
    outsourced,
    status,
    color,
    ident,
    programExtraFieldsProgramFieldId,
    programExtraFieldsValue,
    issueModule,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 项目名称
     */
    name: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目简介
     */
    description?: string,
    /**
     * 负责人 ID
     */
    assigneeId?: string,
    /**
     * 项目类型：内部 (false)/外包 (true) 项目
     */
    outsourced?: boolean,
    /**
     * 项目状态:（0:开始 1:暂停 2:关闭）
     */
    status?: 0 | 1 | 2,
    /**
     * 颜色
     */
    color?: string,
    /**
     * 项目编号
     */
    ident?: string,
    /**
     * 项目自定义字段 id（项目字段设置列表的字段 id）
     */
    programExtraFieldsProgramFieldId?: Array<number>,
    /**
     * 自定义字段的值（options 类型的字段传对应选项的 id，使用 , 隔开，如"1,2,3"）
     */
    programExtraFieldsValue?: Array<string>,
    /**
     * 工作项组件模式
     */
    issueModule?: 'professional' | 'simple',
  }): CancelablePromise<Program> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/programs/{program_id}',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'description': description,
        'assignee_id': assigneeId,
        'outsourced': outsourced,
        'status': status,
        'color': color,
        'ident': ident,
        'program_extra_fields[program_field_id]': programExtraFieldsProgramFieldId,
        'program_extra_fields[value]': programExtraFieldsValue,
        'issue_module': issueModule,
      },
    });
  }

  /**
   * 删除项目
   * 删除项目
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProgramsProgramId({
    programId,
    enterpriseId,
    accessToken,
    deleteMilestones,
    deleteIssues,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 是否删除里程碑
     */
    deleteMilestones?: boolean,
    /**
     * 是否删除任务
     */
    deleteIssues?: boolean,
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/programs/{program_id}',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'delete_milestones': deleteMilestones,
        'delete_issues': deleteIssues,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 获取项目组件信息
   * 获取项目组件信息
   * @returns ProgramComponent 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdComponents({
    programId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<ProgramComponent>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/components',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 修改项目组件
   * 修改项目组件
   * @returns ProgramComponent 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdProgramsProgramIdUpdateComponents({
    programId,
    componentsIdent,
    componentsEnabled,
    enterpriseId,
    accessToken,
    componentsCondition,
    componentsRoleIdsRoleId,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 组件 ident
     */
    componentsIdent: 'plans' | 'requirements' | 'tasks' | 'bugs' | 'sprints' | 'versions' | 'pull_requests' | 'code_repos' | 'pipelines' | 'tests' | 'wikis' | 'metrics' | 'members' | 'activities' | 'milestones',
    /**
     * 是否开启
     */
    componentsEnabled: Array<boolean>,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 0 为属于，1 为不属于
     */
    componentsCondition?: 0 | 1,
    /**
     * 角色 id
     */
    componentsRoleIdsRoleId?: Array<number>,
  }): CancelablePromise<Array<ProgramComponent>> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/programs/{program_id}/update_components',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'components[ident]': componentsIdent,
        'components[enabled]': componentsEnabled,
        'components[condition]': componentsCondition,
        'components[role_ids][role_id]': componentsRoleIdsRoleId,
      },
    });
  }

  /**
   * 获取项目下的成员列表
   * 获取项目下的成员列表
   * @returns Member 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdMembers({
    programId,
    enterpriseId,
    accessToken,
    search,
    sort,
    direction,
    flattenGroupMember = true,
    page = 1,
    perPage,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 排序字段 (created_at: 创建时间 remark: 在企业的备注)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 是否展开团队成员
     */
    flattenGroupMember?: boolean,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Member>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/members',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'sort': sort,
        'direction': direction,
        'flatten_group_member': flattenGroupMember,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 添加企业成员或团队进项目
   * 添加企业成员或团队进项目
   * @returns Member 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProgramsProgramIdMembers({
    programId,
    enterpriseId,
    accessToken,
    userIds,
    groupIds,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 ids，英文逗号 (,) 隔开：1,2
     */
    userIds?: string,
    /**
     * 团队 ids，英文逗号 (,) 隔开：1,2
     */
    groupIds?: string,
  }): CancelablePromise<Member> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/programs/{program_id}/members',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'user_ids': userIds,
        'group_ids': groupIds,
      },
    });
  }

  /**
   * 移出项目下成员
   * 移出项目下成员
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProgramsProgramIdMembersMemberUserId({
    programId,
    memberUserId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 成员 id
     */
    memberUserId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/programs/{program_id}/members/{member_user_id}',
      path: {
        'program_id': programId,
        'member_user_id': memberUserId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取项目下的仓库列表
   * 获取项目下的仓库列表
   * @returns ProjectsList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdProjects({
    programId,
    enterpriseId,
    accessToken,
    scope,
    search,
    type,
    status,
    creatorId,
    parentId,
    forkFilter,
    outsourced,
    groupId,
    sort,
    direction,
    programPipelineEnabled,
    page = 1,
    perPage,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 范围筛选
     */
    scope?: 'private' | 'public' | 'internal-open' | 'not-belong-any-program' | 'all',
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库，personal_namespace: 企业下个人空间地址下的仓库
     */
    type?: 'joined' | 'created' | 'star' | 'template' | 'personal_namespace',
    /**
     * 状态：0: 开始，1: 暂停，2: 关闭
     */
    status?: 0 | 1 | 2,
    /**
     * 负责人
     */
    creatorId?: number,
    /**
     * form_from 仓库 id
     */
    parentId?: number,
    /**
     * 非 fork 的 (not_fork), 只看 fork 的 (only_fork)
     */
    forkFilter?: 'not_fork' | 'only_fork',
    /**
     * 是否外包：0：内部，1：外包
     */
    outsourced?: 0 | 1,
    /**
     * 团队 id
     */
    groupId?: number,
    /**
     * 排序字段 (created_at、last_push_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 是否开启项目流水线支持
     */
    programPipelineEnabled?: boolean,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProjectsList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/projects',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'scope': scope,
        'search': search,
        'type': type,
        'status': status,
        'creator_id': creatorId,
        'parent_id': parentId,
        'fork_filter': forkFilter,
        'outsourced': outsourced,
        'group_id': groupId,
        'sort': sort,
        'direction': direction,
        'program_pipeline_enabled': programPipelineEnabled,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 项目关联仓库
   * 项目关联仓库
   * @returns Project 项目关联仓库
   * @throws ApiError
   */
  public postEnterpriseIdProgramsProgramIdProjects({
    programId,
    addProjectIds,
    enterpriseId,
    accessToken,
    importProjectUsers = 1,
    importProjectIssues = 0,
    importProjectMilestones = 0,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 仓库 ids
     */
    addProjectIds: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 是否导入仓库成员
     */
    importProjectUsers?: 0 | 1,
    /**
     * 是否导入仓库任务
     */
    importProjectIssues?: 0 | 1,
    /**
     * 是否导入仓库里程碑
     */
    importProjectMilestones?: 0 | 1,
  }): CancelablePromise<Project> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/programs/{program_id}/projects',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'add_project_ids': addProjectIds,
        'import_project_users': importProjectUsers,
        'import_project_issues': importProjectIssues,
        'import_project_milestones': importProjectMilestones,
      },
    });
  }

  /**
   * 移出项目下仓库
   * 移出项目下仓库
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProgramsProgramIdProjectsProjectId({
    programId,
    projectId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 仓库 id
     */
    projectId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/programs/{program_id}/projects/{project_id}',
      path: {
        'program_id': programId,
        'project_id': projectId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取项目下动态列表
   * 获取项目下动态列表
   * @returns Event 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdEvents({
    programId,
    enterpriseId,
    accessToken,
    startDate,
    endDate,
    scope,
    page = 1,
    perPage,
    prevId,
    limit,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 项目范围：所有，仓库，任务，外部，我的
     */
    scope?: 'all' | 'project' | 'issue' | 'outsource' | 'my',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 上一次动态列表中最小动态 ID (返回列表不包含该 ID 记录)
     */
    prevId?: number,
    /**
     * 每次获取动态的条数
     */
    limit?: number,
  }): CancelablePromise<Array<Event>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/events',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'start_date': startDate,
        'end_date': endDate,
        'scope': scope,
        'page': page,
        'per_page': perPage,
        'prev_id': prevId,
        'limit': limit,
      },
    });
  }

  /**
   * 获取项目的操作权限
   * 获取项目的操作权限
   * @returns any 获取项目的操作权限
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdOperateAuths({
    programId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/operate_auths',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取项目下的任务列表
   * 获取项目下的任务列表
   * @returns Issue 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdIssues({
    programId,
    enterpriseId,
    accessToken,
    kanbanColumnId,
    kanbanIds,
    projectId,
    milestoneId,
    state,
    onlyRelatedMe,
    assigneeId,
    authorId,
    collaboratorIds,
    createdAt,
    finishedAt,
    planStartedAt,
    deadline,
    search,
    filterChild,
    issueStateIds,
    issueTypeId,
    labelIds,
    priority,
    scrumSprintIds,
    scrumVersionIds,
    kanbanColumnIds,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 项目 id（可多选，用英文逗号分隔）
     */
    programId: string,
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 看板栏 ID
     */
    kanbanColumnId?: number,
    /**
     * 看板 id(可多选，用英文逗号分隔)
     */
    kanbanIds?: string,
    /**
     * 仓库 id
     */
    projectId?: string,
    /**
     * 里程碑 id（可多选，用英文逗号分隔）
     */
    milestoneId?: string,
    /**
     * 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中：progressing）
     */
    state?: string,
    /**
     * 是否仅列出与授权用户相关的任务（0: 否 1: 是）
     */
    onlyRelatedMe?: string,
    /**
     * 负责人 id
     */
    assigneeId?: string,
    /**
     * 创建者 id
     */
    authorId?: string,
    /**
     * 协作者。(,分隔的 id 字符串)
     */
    collaboratorIds?: string,
    /**
     * 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）<yyyymmddTHH:MM:SS+08:00，（大于指定日期）>yyyymmddTHH:MM:SS+08:00
     */
    createdAt?: string,
    /**
     * 任务完成日期，格式同上
     */
    finishedAt?: string,
    /**
     * 计划开始时间，(格式：yyyy-mm-dd)
     */
    planStartedAt?: string,
    /**
     * 任务截止日期，(格式：yyyy-mm-dd)
     */
    deadline?: string,
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 是否过滤子任务 (0: 否，1: 是)
     */
    filterChild?: string,
    /**
     * 任务状态 id，多选，用英文逗号分隔。
     */
    issueStateIds?: string,
    /**
     * 任务类型
     */
    issueTypeId?: string,
    /**
     * 标签 id（可多选，用英文逗号分隔）
     */
    labelIds?: string,
    /**
     * 优先级（可多选，用英文逗号分隔）
     */
    priority?: string,
    /**
     * 迭代 id(可多选，用英文逗号分隔)
     */
    scrumSprintIds?: string,
    /**
     * 版本 id(可多选，用英文逗号分隔)
     */
    scrumVersionIds?: string,
    /**
     * 看板栏 id(可多选，用英文逗号分隔)
     */
    kanbanColumnIds?: string,
    /**
     * 排序字段 (created_at、updated_at、deadline、priority)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Issue>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/issues',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'kanban_column_id': kanbanColumnId,
        'kanban_ids': kanbanIds,
        'project_id': projectId,
        'milestone_id': milestoneId,
        'state': state,
        'only_related_me': onlyRelatedMe,
        'assignee_id': assigneeId,
        'author_id': authorId,
        'collaborator_ids': collaboratorIds,
        'created_at': createdAt,
        'finished_at': finishedAt,
        'plan_started_at': planStartedAt,
        'deadline': deadline,
        'search': search,
        'filter_child': filterChild,
        'issue_state_ids': issueStateIds,
        'issue_type_id': issueTypeId,
        'label_ids': labelIds,
        'priority': priority,
        'scrum_sprint_ids': scrumSprintIds,
        'scrum_version_ids': scrumVersionIds,
        'kanban_column_ids': kanbanColumnIds,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取项目下未被规划的工作项数量
   * 获取项目下未被规划的工作项数量
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdWithoutPlanedIssuesCount({
    programId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/without_planed_issues_count',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取项目下的 Pull Request
   * 获取项目下的 Pull Request
   * @returns Issue 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdPullRequests({
    programId,
    enterpriseId,
    accessToken,
    state,
    scope,
    authorId,
    assigneeId,
    testerId,
    search,
    sort,
    direction,
    groupId,
    milestoneId,
    labels,
    labelIds,
    canBeMerged,
    projectId,
    needStateCount,
    page = 1,
    perPage,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * PR 状态
     */
    state?: 'opened' | 'closed' | 'merged',
    /**
     * 范围筛选。指派我的：assigned_or_test，我创建或指派给我的：related_to_me，
     * 我参与仓库的 PR: participate_in，草稿 PR: draft，create：我发起的，assign：我评审的，test：我测试的
     */
    scope?: 'assigned_or_test' | 'related_to_me' | 'participate_in' | 'draft' | 'create' | 'assign' | 'test',
    /**
     * 筛选 PR 创建者
     */
    authorId?: string,
    /**
     * 筛选 PR 审查者
     */
    assigneeId?: string,
    /**
     * 筛选 PR 测试人员
     */
    testerId?: string,
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 排序字段 (created_at、closed_at、priority、updated_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 团队 id
     */
    groupId?: number,
    /**
     * 里程碑 id
     */
    milestoneId?: number,
    /**
     * 标签名称。多个标签逗号 (,) 隔开
     */
    labels?: string,
    /**
     * 标签 ID，多个标签逗号 (,) 隔开
     */
    labelIds?: string,
    /**
     * 是否可合并
     */
    canBeMerged?: 0 | 1,
    /**
     * 仓库 id
     */
    projectId?: number,
    /**
     * 是否需要状态统计数
     */
    needStateCount?: 0 | 1,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Issue>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/pull_requests',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'state': state,
        'scope': scope,
        'author_id': authorId,
        'assignee_id': assigneeId,
        'tester_id': testerId,
        'search': search,
        'sort': sort,
        'direction': direction,
        'group_id': groupId,
        'milestone_id': milestoneId,
        'labels': labels,
        'label_ids': labelIds,
        'can_be_merged': canBeMerged,
        'project_id': projectId,
        'need_state_count': needStateCount,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取项目下的团队
   * 获取项目下的团队
   * @returns Group 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProgramsProgramIdGroups({
    programId,
    enterpriseId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Group>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/programs/{program_id}/groups',
      path: {
        'program_id': programId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 移出项目下的团队
   * 移出项目下的团队
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProgramsProgramIdGroupsGroupId({
    programId,
    groupId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 项目 id
     */
    programId: number,
    /**
     * 团队 id
     */
    groupId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/programs/{program_id}/groups/{group_id}',
      path: {
        'program_id': programId,
        'group_id': groupId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
