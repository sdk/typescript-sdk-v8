/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Tag } from '../models/Tag';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ProjectTagsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取仓库的标签列表
   * 获取仓库的标签列表
   * @returns Tag 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdTags({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    search,
    exact,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 是否精确查找 true:是 false:否
     */
    exact?: boolean,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Tag> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/tags',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'search': search,
        'exact': exact,
        'page': page,
        'per_page': perPage,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 新建标签
   * 新建标签
   * @returns Tag 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdTags({
    enterpriseId,
    projectId,
    name,
    refs,
    accessToken,
    qt,
    description,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * tag 名称
     */
    name: string,
    /**
     * 起点
     */
    refs: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 描述
     */
    description?: string,
  }): CancelablePromise<Tag> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/tags',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'name': name,
        'refs': refs,
        'description': description,
      },
    });
  }

  /**
   * 删除标签
   * 删除标签
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdTagsDestroy({
    enterpriseId,
    projectId,
    name,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 标签名称
     */
    name: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/tags/destroy',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'name': name,
      },
    });
  }

}
