/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Group } from '../models/Group';
import type { GroupMember } from '../models/GroupMember';
import type { Member } from '../models/Member';
import type { Project } from '../models/Project';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class GroupsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取企业团队列表
   * 获取企业团队列表
   * @returns Group 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdGroups({
    enterpriseId,
    accessToken,
    qt,
    sort,
    programId,
    direction,
    search,
    scope,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 排序字段 (created_at: 创建时间 updated_at: 更新时间)
     */
    sort?: string,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 搜索字符串
     */
    search?: string,
    /**
     * admin: 获取用户管理的团队
     */
    scope?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Group>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/groups',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'sort': sort,
        'program_id': programId,
        'direction': direction,
        'search': search,
        'scope': scope,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新建企业团队
   * 新建企业团队
   * @returns Group 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdGroups({
    enterpriseId,
    path,
    name,
    _public,
    ownerId,
    accessToken,
    description,
    userIds,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 团队 path
     */
    path: string,
    /**
     * 名称
     */
    name: string,
    /**
     * 类型，0:内部，1:公开，2:外包
     */
    _public: number,
    /**
     * 负责人 id
     */
    ownerId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 简介
     */
    description?: string,
    /**
     * 成员 ids, 逗号隔开：1,2,3
     */
    userIds?: string,
  }): CancelablePromise<Group> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/groups',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'path': path,
        'name': name,
        'description': description,
        'public': _public,
        'owner_id': ownerId,
        'user_ids': userIds,
      },
    });
  }

  /**
   * 获取团队详情
   * 获取团队详情
   * @returns Group 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdGroupsGroupId({
    enterpriseId,
    groupId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 团队 id/path
     */
    groupId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Group> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/groups/{group_id}',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 更新企业团队
   * 更新企业团队
   * @returns Group 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdGroupsGroupId({
    enterpriseId,
    groupId,
    accessToken,
    userIds,
    name,
    description,
    _public,
    ownerId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 团队 id
     */
    groupId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 id
     */
    userIds?: string,
    /**
     * 名称
     */
    name?: string,
    /**
     * 简介
     */
    description?: string,
    /**
     * 类型，0:内部，1:公开，2:外包
     */
    _public?: number,
    /**
     * 负责人 id
     */
    ownerId?: number,
  }): CancelablePromise<Group> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/groups/{group_id}',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      formData: {
        'access_token': accessToken,
        'user_ids': userIds,
        'name': name,
        'description': description,
        'public': _public,
        'owner_id': ownerId,
      },
    });
  }

  /**
   * 删除企业团队
   * 删除企业团队
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdGroupsGroupId({
    enterpriseId,
    groupId,
    accessToken,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 团队 id
     */
    groupId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/groups/{group_id}',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      query: {
        'access_token': accessToken,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 获取企业团队下的成员列表
   * 获取企业团队下的成员列表
   * @returns Member 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdGroupsGroupIdMembers({
    enterpriseId,
    groupId,
    accessToken,
    qt,
    search,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 团队 id/path
     */
    groupId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 排序字段 (created_at: 创建时间 remark: 在企业的备注)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Member>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/groups/{group_id}/members',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'search': search,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 添加团队成员
   * 添加团队成员
   * @returns GroupMember 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdGroupsGroupIdMembers({
    enterpriseId,
    groupId,
    accessToken,
    qt,
    masterIds,
    developerIds,
    viewerIds,
    reporterIds,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 团队 id/path
     */
    groupId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 管理员 ID 列表，多个使用英文 , 隔开
     */
    masterIds?: string,
    /**
     * 开发者 ID 列表，多个使用英文 , 隔开
     */
    developerIds?: string,
    /**
     *  观察者 ID 列表，多个使用英文 , 隔开
     */
    viewerIds?: string,
    /**
     * 报告者 ID 列表，多个使用英文 , 隔开
     */
    reporterIds?: string,
  }): CancelablePromise<Array<GroupMember>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/groups/{group_id}/members',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'master_ids': masterIds,
        'developer_ids': developerIds,
        'viewer_ids': viewerIds,
        'reporter_ids': reporterIds,
      },
    });
  }

  /**
   * 移除团队成员
   * 移除团队成员
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdGroupsGroupIdMembers({
    enterpriseId,
    groupId,
    accessToken,
    userIds,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 团队 id
     */
    groupId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 id，多个成员 id 通过英文逗号分隔
     */
    userIds?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/groups/{group_id}/members',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      query: {
        'access_token': accessToken,
        'user_ids': userIds,
      },
    });
  }

  /**
   * 变更团队成员角色
   * 变更团队成员角色
   * @returns GroupMember 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdGroupsGroupIdMembersAccessLevel({
    userIds,
    accessLevel,
    enterpriseId,
    groupId,
    accessToken,
  }: {
    /**
     * 用户 ID
     */
    userIds: string,
    /**
     * 团队成员角色
     */
    accessLevel: 40 | 30 | 25 | 15,
    enterpriseId: number,
    groupId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<GroupMember>> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/groups/{group_id}/members/access_level',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      formData: {
        'access_token': accessToken,
        'user_ids': userIds,
        'access_level': accessLevel,
      },
    });
  }

  /**
   * 企业团队下仓库列表
   * 企业团队下仓库列表
   * @returns Project 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdGroupsGroupIdProjects({
    enterpriseId,
    groupId,
    accessToken,
    qt,
    scope,
    search,
    type,
    status,
    creatorId,
    parentId,
    forkFilter,
    outsourced,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 团队 id/path
     */
    groupId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 范围筛选
     */
    scope?: 'private' | 'public' | 'internal-open' | 'not-belong-any-program' | 'all',
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库，personal_namespace: 企业下个人空间地址下的仓库
     */
    type?: 'joined' | 'created' | 'star' | 'template' | 'personal_namespace',
    /**
     * 状态：0: 开始，1: 暂停，2: 关闭
     */
    status?: 0 | 1 | 2,
    /**
     * 负责人
     */
    creatorId?: number,
    /**
     * form_from 仓库 id
     */
    parentId?: number,
    /**
     * 非 fork 的 (not_fork), 只看 fork 的 (only_fork)
     */
    forkFilter?: 'not_fork' | 'only_fork',
    /**
     * 是否外包：0：内部，1：外包
     */
    outsourced?: 0 | 1,
    /**
     * 排序字段 (created_at: 创建时间 last_push_at: 最近 push 时间)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Project>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/groups/{group_id}/projects',
      path: {
        'enterprise_id': enterpriseId,
        'group_id': groupId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'scope': scope,
        'search': search,
        'type': type,
        'status': status,
        'creator_id': creatorId,
        'parent_id': parentId,
        'fork_filter': forkFilter,
        'outsourced': outsourced,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

}
