/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { FileVersion } from '../models/FileVersion';
import type { WikiSort } from '../models/WikiSort';
import type { WikiSortDetail } from '../models/WikiSortDetail';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class DocsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取文档下的文件列表
   * 获取文档下的文件列表
   * @returns WikiSort 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocsDocIdFiles({
    enterpriseId,
    docId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<WikiSort>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/docs/{doc_id}/files',
      path: {
        'enterprise_id': enterpriseId,
        'doc_id': docId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取文档历史版本
   * 获取文档历史版本
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocsDocIdVersions({
    docId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/docs/{doc_id}/versions',
      path: {
        'doc_id': docId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取版本历史详情
   * 获取版本历史详情
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocsDocIdCommit({
    docId,
    commitId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件历史
     */
    commitId: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/docs/{doc_id}/commit',
      path: {
        'doc_id': docId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'commit_id': commitId,
      },
    });
  }

  /**
   * 获取文档备份进度
   * 获取文档备份进度
   * @returns any 获取文档备份进度
   * @throws ApiError
   */
  public getEnterpriseIdDocsDocIdBackupProgress({
    enterpriseId,
    docId,
    accessToken,
  }: {
    enterpriseId: number,
    docId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/docs/{doc_id}/backup_progress',
      path: {
        'enterprise_id': enterpriseId,
        'doc_id': docId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取文件详情
   * 获取文件详情
   * @returns WikiSortDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocsDocIdFilesFileId({
    docId,
    fileId,
    enterpriseId,
    accessToken,
    versionId,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件 id(docs 获取文件列表的 id 字段)
     */
    fileId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 指定历史版本
     */
    versionId?: string,
  }): CancelablePromise<WikiSortDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/docs/{doc_id}/files/{file_id}',
      path: {
        'doc_id': docId,
        'file_id': fileId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'version_id': versionId,
      },
    });
  }

  /**
   * 更新文件
   * 更新文件
   * @returns WikiSortDetail 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocsDocIdFilesFileId({
    docId,
    fileId,
    enterpriseId,
    accessToken,
    name,
    content,
    message,
    mentionedUserIds,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件 id(docs 获取文件列表的 id 字段)
     */
    fileId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 文件标题
     */
    name?: string,
    /**
     * 文件内容
     */
    content?: string,
    /**
     * 提交
     */
    message?: string,
    /**
     * 提及的用户 id，多个使用 英文 , 分隔
     */
    mentionedUserIds?: string,
  }): CancelablePromise<WikiSortDetail> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/docs/{doc_id}/files/{file_id}',
      path: {
        'doc_id': docId,
        'file_id': fileId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'content': content,
        'message': message,
        'mentioned_user_ids': mentionedUserIds,
      },
    });
  }

  /**
   * 删除文件
   * 删除文件
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdDocsDocIdFilesFileId({
    docId,
    fileId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件 id(docs 获取文件列表的 id 字段)
     */
    fileId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/docs/{doc_id}/files/{file_id}',
      path: {
        'doc_id': docId,
        'file_id': fileId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 创建文件
   * 创建文件
   * @returns WikiSortDetail 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocsDocIdCreateFile({
    docId,
    enterpriseId,
    accessToken,
    name,
    parentId,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 文件名称（不填则由系统自动生成文件名）
     */
    name?: string,
    /**
     * 父级 id(默认为 0)
     */
    parentId?: number,
  }): CancelablePromise<Array<WikiSortDetail>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/docs/{doc_id}/create_file',
      path: {
        'doc_id': docId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'parent_id': parentId,
      },
    });
  }

  /**
   * 创建文件夹
   * 创建文件夹
   * @returns WikiSortDetail 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocsDocIdCreateFolder({
    docId,
    name,
    enterpriseId,
    accessToken,
    parentId,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件夹名称
     */
    name: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 父级 id(默认为 0)
     */
    parentId?: number,
  }): CancelablePromise<Array<WikiSortDetail>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/docs/{doc_id}/create_folder',
      path: {
        'doc_id': docId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'parent_id': parentId,
      },
    });
  }

  /**
   * 更改文件夹名称
   * 更改文件夹名称
   * @returns any 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocsDocIdFilesFileIdRename({
    docId,
    fileId,
    name,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件 id(docs 获取文件列表的 id 字段)
     */
    fileId: number,
    /**
     * 文件夹名称
     */
    name: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/docs/{doc_id}/files/{file_id}/rename',
      path: {
        'doc_id': docId,
        'file_id': fileId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
      },
    });
  }

  /**
   * 移动文件
   * 移动文件
   * @returns WikiSort 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocsDocIdFilesFileIdMove({
    docId,
    fileId,
    parentId,
    prevId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件 id(docs 获取文件列表的 id 字段)
     */
    fileId: number,
    /**
     * 文件移动后的父级的 file_id
     */
    parentId: number,
    /**
     * 移动后的同级上一个节点的 file_id（默认：0）
     */
    prevId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<WikiSort> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/docs/{doc_id}/files/{file_id}/move',
      path: {
        'doc_id': docId,
        'file_id': fileId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'parent_id': parentId,
        'prev_id': prevId,
      },
    });
  }

  /**
   * 获取文件的历史版本列表
   * 获取文件的历史版本列表
   * @returns FileVersion 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocsDocIdFilesFileIdVersions({
    docId,
    fileId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 文档 id(doc_nodes 文档列表接口的 file_id 字段)
     */
    docId: number,
    /**
     * 文件 id(docs 获取文件列表的 id 字段)
     */
    fileId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<FileVersion> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/docs/{doc_id}/files/{file_id}/versions',
      path: {
        'doc_id': docId,
        'file_id': fileId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
