/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AttachFile } from '../models/AttachFile';
import type { Issue } from '../models/Issue';
import type { IssueAuth } from '../models/IssueAuth';
import type { IssueDetail } from '../models/IssueDetail';
import type { IssueDetailWithRelatedInfos } from '../models/IssueDetailWithRelatedInfos';
import type { IssueMemberSelect } from '../models/IssueMemberSelect';
import type { IssueNote } from '../models/IssueNote';
import type { IssueOperateLog } from '../models/IssueOperateLog';
import type { IssueReaction } from '../models/IssueReaction';
import type { IssueTypeStateRef } from '../models/IssueTypeStateRef';
import type { LinkIssue } from '../models/LinkIssue';
import type { Object } from '../models/Object';
import type { UserWithRemark } from '../models/UserWithRemark';
import type { WithWorkload } from '../models/WithWorkload';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class IssuesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 可选的关联任务集
   * 可选的关联任务集
   * @returns Issue 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesLinkIssues({
    enterpriseId,
    accessToken,
    issueId,
    programId,
    search,
    issueTypeCategory,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务 id
     */
    issueId?: number,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 以英文逗号分隔的类型，类型可取值枚举：task、bug、requirement
     */
    issueTypeCategory?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Issue>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/link_issues',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'issue_id': issueId,
        'program_id': programId,
        'search': search,
        'issue_type_category': issueTypeCategory,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取任务指派的成员列表
   * 获取任务指派的成员列表
   * @returns IssueMemberSelect 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesMemberSelect({
    enterpriseId,
    accessToken,
    programId,
    projectId,
    issueId,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 ID
     */
    programId?: string,
    /**
     * 仓库 ID
     */
    projectId?: string,
    /**
     * 工作项 ID
     */
    issueId?: number,
  }): CancelablePromise<Array<IssueMemberSelect>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/member_select',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
        'project_id': projectId,
        'issue_id': issueId,
      },
    });
  }

  /**
   * 获取成员看板的成员列表
   * 获取成员看板的成员列表
   * @returns UserWithRemark 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesBoardMembers({
    enterpriseId,
    accessToken,
    assigneeIds,
    projectId,
    groupIds,
    programId,
    milestoneId,
    issueTypeId,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 ID
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 以，分隔的负责人 ID 字符串，如：23,12
     */
    assigneeIds?: string,
    /**
     * 仓库 ID
     */
    projectId?: number,
    /**
     * 以，分隔的组织 ID 字符串
     */
    groupIds?: string,
    /**
     * 项目 ID
     */
    programId?: number,
    /**
     * 里程碑 ID
     */
    milestoneId?: number,
    /**
     * 任务类型 ID
     */
    issueTypeId?: number,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<UserWithRemark>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/board_members',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'assignee_ids': assigneeIds,
        'project_id': projectId,
        'group_ids': groupIds,
        'program_id': programId,
        'milestone_id': milestoneId,
        'issue_type_id': issueTypeId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取任务列表
   * 获取任务列表
   * @returns WithWorkload 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssues({
    enterpriseId,
    accessToken,
    projectId,
    programId,
    milestoneId,
    state,
    onlyRelatedMe,
    assigneeId,
    authorId,
    collaboratorIds,
    createdAt,
    finishedAt,
    planStartedAt,
    deadline,
    search,
    filterChild,
    issueStateIds,
    issueTypeId,
    labelIds,
    priority,
    scrumSprintIds,
    scrumVersionIds,
    kanbanIds,
    kanbanColumnIds,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 仓库 id
     */
    projectId?: string,
    /**
     * 项目 id（可多选，用英文逗号分隔）
     */
    programId?: string,
    /**
     * 里程碑 id（可多选，用英文逗号分隔）
     */
    milestoneId?: string,
    /**
     * 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中：progressing）
     */
    state?: string,
    /**
     * 是否仅列出与授权用户相关的任务（0: 否 1: 是）
     */
    onlyRelatedMe?: string,
    /**
     * 负责人 id
     */
    assigneeId?: string,
    /**
     * 创建者 id
     */
    authorId?: string,
    /**
     * 协作者。(,分隔的 id 字符串)
     */
    collaboratorIds?: string,
    /**
     * 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）<yyyymmddTHH:MM:SS+08:00，（大于指定日期）>yyyymmddTHH:MM:SS+08:00
     */
    createdAt?: string,
    /**
     * 任务完成日期，格式同上
     */
    finishedAt?: string,
    /**
     * 计划开始时间，(格式：yyyy-mm-dd)
     */
    planStartedAt?: string,
    /**
     * 任务截止日期，(格式：yyyy-mm-dd)
     */
    deadline?: string,
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * ��否过滤子任务 (0: 否，1: 是)
     */
    filterChild?: string,
    /**
     * 任务状态 id，多选，用英文逗号分隔。
     */
    issueStateIds?: string,
    /**
     * 任务类型
     */
    issueTypeId?: string,
    /**
     * 标签 id（可多选，用英文逗号分隔）
     */
    labelIds?: string,
    /**
     * 优先级（可多选，用英文逗号分隔）
     */
    priority?: string,
    /**
     * 迭代 id(可多选，用英文逗号分隔)
     */
    scrumSprintIds?: string,
    /**
     * 版本 id(可多选，用英文逗号分隔)
     */
    scrumVersionIds?: string,
    /**
     * 看板 id(可多选，用英文逗号分隔)
     */
    kanbanIds?: string,
    /**
     * 看板栏 id(可多选，用英文逗号分隔)
     */
    kanbanColumnIds?: string,
    /**
     * 排序字段 (created_at、updated_at、deadline、priority)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<WithWorkload>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'project_id': projectId,
        'program_id': programId,
        'milestone_id': milestoneId,
        'state': state,
        'only_related_me': onlyRelatedMe,
        'assignee_id': assigneeId,
        'author_id': authorId,
        'collaborator_ids': collaboratorIds,
        'created_at': createdAt,
        'finished_at': finishedAt,
        'plan_started_at': planStartedAt,
        'deadline': deadline,
        'search': search,
        'filter_child': filterChild,
        'issue_state_ids': issueStateIds,
        'issue_type_id': issueTypeId,
        'label_ids': labelIds,
        'priority': priority,
        'scrum_sprint_ids': scrumSprintIds,
        'scrum_version_ids': scrumVersionIds,
        'kanban_ids': kanbanIds,
        'kanban_column_ids': kanbanColumnIds,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新建任务
   * 新建任务
   * @returns IssueDetail 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdIssues({
    enterpriseId,
    title,
    accessToken,
    copiedIssueId,
    description,
    assigneeId,
    collaboratorIds,
    issueTypeId,
    programId,
    projectId,
    milestoneId,
    labelIds,
    priority,
    parentId,
    branch,
    planStartedAt,
    deadline,
    startedAt,
    finishedAt,
    attachmentIds,
    securityHole,
    extraFieldsIssueFieldId,
    extraFieldsValue,
    kanbanId,
    kanbanColumnId,
    scrumSprintId,
    scrumVersionId,
    estimatedDuration,
    duration,
    testPlanCaseId,
    pullRequestId,
    category,
    linkIssueId,
    linkRefType,
    linkDirection,
    linkResultType,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务标题
     */
    title: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务复制的源任务 ID
     */
    copiedIssueId?: number,
    /**
     * 任务内容
     */
    description?: string,
    /**
     * 负责人的 user id
     */
    assigneeId?: number,
    /**
     * 协作者的 user id，如有多个请用英文逗号分割。eg: 1,2,3
     */
    collaboratorIds?: string,
    /**
     * 任务类型的 id
     */
    issueTypeId?: number,
    /**
     * 关联项目的 id
     */
    programId?: number,
    /**
     * 关联仓库的 id
     */
    projectId?: number,
    /**
     * 关联里程碑的 id
     */
    milestoneId?: number,
    /**
     * 关联标签的 id，如有多个请用英文逗号分割。eg: 1,2,3
     */
    labelIds?: string,
    /**
     * 优先级 (0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重)
     */
    priority?: 0 | 1 | 2 | 3 | 4,
    /**
     * 父级任务的 id
     */
    parentId?: number,
    /**
     * 关联分支的名称
     */
    branch?: string,
    /**
     * 计划开始日期。格式：yyyy-mm-ddTHH:MM:SS
     */
    planStartedAt?: string,
    /**
     * 计划完成日期。格式：yyyy-mm-ddTHH:MM:SS
     */
    deadline?: string,
    /**
     * 实际开始时间。格式：yyyy-mm-ddTHH:MM:SS
     */
    startedAt?: string,
    /**
     * 实际完成时间。格式：yyyy-mm-ddTHH:MM:SS
     */
    finishedAt?: string,
    /**
     * 附件 id，如有多个请用英文逗号分割。eg: 1,2,3
     */
    attachmentIds?: string,
    /**
     * 是否是私有 issue, 0:否，1:是
     */
    securityHole?: 0 | 1,
    /**
     * 任务字段 id
     */
    extraFieldsIssueFieldId?: Array<number>,
    /**
     * 自定义字段的值（options 类型的字段传对应选项的 id，使用 , 隔开，如"1,2,3"）
     */
    extraFieldsValue?: Array<string>,
    /**
     * 看板 ID
     */
    kanbanId?: number,
    /**
     * 看板的栏 ID
     */
    kanbanColumnId?: number,
    /**
     * 迭代 ID
     */
    scrumSprintId?: number,
    /**
     * 版本 ID
     */
    scrumVersionId?: number,
    /**
     * 预估工时 (单位：小时，支持两位小数)
     */
    estimatedDuration?: number,
    /**
     * 预估工时。（单位：分钟）
     */
    duration?: number,
    /**
     * 用例 ID
     */
    testPlanCaseId?: string,
    /**
     * PullRequest ID
     */
    pullRequestId?: number,
    /**
     * 任务类型属性
     */
    category?: 'task' | 'bug' | 'requirement',
    /**
     * 需要关联的任务 id
     */
    linkIssueId?: number,
    /**
     * 关联关系 (normal, finish_to_finish, finish_to_start, start_to_start, start_to_finish)
     */
    linkRefType?: string,
    /**
     * 关联关系的方向 (none, pre, latter)
     */
    linkDirection?: string,
    /**
     * 返回结果类型：包括 issue, dependence
     */
    linkResultType?: string,
  }): CancelablePromise<IssueDetail> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issues',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'copied_issue_id': copiedIssueId,
        'title': title,
        'description': description,
        'assignee_id': assigneeId,
        'collaborator_ids': collaboratorIds,
        'issue_type_id': issueTypeId,
        'program_id': programId,
        'project_id': projectId,
        'milestone_id': milestoneId,
        'label_ids': labelIds,
        'priority': priority,
        'parent_id': parentId,
        'branch': branch,
        'plan_started_at': planStartedAt,
        'deadline': deadline,
        'started_at': startedAt,
        'finished_at': finishedAt,
        'attachment_ids': attachmentIds,
        'security_hole': securityHole,
        'extra_fields[issue_field_id]': extraFieldsIssueFieldId,
        'extra_fields[value]': extraFieldsValue,
        'kanban_id': kanbanId,
        'kanban_column_id': kanbanColumnId,
        'scrum_sprint_id': scrumSprintId,
        'scrum_version_id': scrumVersionId,
        'estimated_duration': estimatedDuration,
        'duration': duration,
        'test_plan_case_id': testPlanCaseId,
        'pull_request_id': pullRequestId,
        'category': category,
        'link_issue_id': linkIssueId,
        'link_ref_type': linkRefType,
        'link_direction': linkDirection,
        'link_result_type': linkResultType,
      },
    });
  }

  /**
   * 获取任务列表 - 筛选器查询
   * 获取任务列表 - 筛选器查询
   * @returns Issue 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdIssuesFilter({
    filterConditionsProperty,
    enterpriseId,
    accessToken,
    kanbanColumnIds,
    filterId,
    filterConditionsComparator,
    filterConditionsValue,
    filterConditionsIssueFieldType,
    filterConditionsIssueFieldId,
    priorityFiltersIssueType,
    priorityFiltersIssueState,
    priorityFiltersIssueAssignee,
    priorityFiltersIssueLabel,
    search,
    sort,
    direction,
    issueTypeCategory,
    page = 1,
    perPage,
  }: {
    /**
     * 筛选类型
     */
    filterConditionsProperty: Array<string>,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 看板栏 id(可多选，用英文逗号分隔)
     */
    kanbanColumnIds?: string,
    /**
     * 筛选器 id
     */
    filterId?: number,
    /**
     * 比较符
     */
    filterConditionsComparator?: Array<string>,
    /**
     * 值
     */
    filterConditionsValue?: Array<string>,
    /**
     * 自定义字段���类型
     */
    filterConditionsIssueFieldType?: Array<string>,
    /**
     * 自定义字段 id
     */
    filterConditionsIssueFieldId?: Array<number>,
    /**
     * 任务类型
     */
    priorityFiltersIssueType?: string,
    /**
     * 任务状态
     */
    priorityFiltersIssueState?: string,
    /**
     * 任务负责人
     */
    priorityFiltersIssueAssignee?: string,
    /**
     * 任务标签
     */
    priorityFiltersIssueLabel?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 排序字段 (created_at、updated_at、deadline、priority)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 任务类型属性
     */
    issueTypeCategory?: 'task' | 'bug' | 'requirement',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Issue>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issues/filter',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'kanban_column_ids': kanbanColumnIds,
        'filter_id': filterId,
        'filter_conditions[property]': filterConditionsProperty,
        'filter_conditions[comparator]': filterConditionsComparator,
        'filter_conditions[value]': filterConditionsValue,
        'filter_conditions[issue_field_type]': filterConditionsIssueFieldType,
        'filter_conditions[issue_field_id]': filterConditionsIssueFieldId,
        'priority_filters[issue_type]': priorityFiltersIssueType,
        'priority_filters[issue_state]': priorityFiltersIssueState,
        'priority_filters[issue_assignee]': priorityFiltersIssueAssignee,
        'priority_filters[issue_label]': priorityFiltersIssueLabel,
        'search': search,
        'sort': sort,
        'direction': direction,
        'issue_type_category': issueTypeCategory,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取企业下用户任务相关数量数据
   * 获取企业下用户任务相关数量数据
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueStats({
    enterpriseId,
    accessToken,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/issue_stats',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取任务详情
   * 获取任务详情
   * @returns IssueDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueId({
    enterpriseId,
    issueId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务的 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 查询方式 (ident/id)
     */
    qt?: string,
  }): CancelablePromise<IssueDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 更新任务
   * 更新任务
   * @returns IssueDetail 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdIssuesIssueId({
    enterpriseId,
    issueId,
    accessToken,
    title,
    description,
    assigneeId,
    collaboratorIds,
    issueTypeId,
    issueStateId,
    programId,
    projectId,
    milestoneId,
    labelIds,
    priority,
    parentId,
    branch,
    planStartedAt,
    deadline,
    startedAt,
    finishedAt,
    securityHole,
    scrumSprintId,
    scrumVersionId,
    kanbanId,
    kanbanColumnId,
    estimatedDuration,
    duration,
    changeStates,
    hierarchyChangeMethod,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务标题
     */
    title?: string,
    /**
     * 任务内容
     */
    description?: string,
    /**
     * 负责人的 user id
     */
    assigneeId?: number,
    /**
     * 协作者的 user id，如有多个请用英文逗号分割。eg: 1,2,3
     */
    collaboratorIds?: string,
    /**
     * 任务类型的 id
     */
    issueTypeId?: number,
    /**
     * 任务状态的 id
     */
    issueStateId?: number,
    /**
     * 关联项目的 id
     */
    programId?: number,
    /**
     * 关联仓库的 id
     */
    projectId?: number,
    /**
     * 关联里程碑的 id
     */
    milestoneId?: number,
    /**
     * 关联标签的 id，如有多个请用英文逗号分割。eg: 1,2,3
     */
    labelIds?: string,
    /**
     * 优先级 (0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重)
     */
    priority?: number,
    /**
     * 父级任务的 id
     */
    parentId?: number,
    /**
     * 关联分支的名称
     */
    branch?: string,
    /**
     * 计划开始日期。格式：yyyy-mm-ddTHH:MM:SS
     */
    planStartedAt?: string,
    /**
     * 计划完成日期。格式：yyyy-mm-ddTHH:MM:SS
     */
    deadline?: string,
    /**
     * 实际开始时间。格式：yyyy-mm-ddTHH:MM:SS
     */
    startedAt?: string,
    /**
     * 实际完成时间。格式：yyyy-mm-ddTHH:MM:SS
     */
    finishedAt?: string,
    /**
     * 是否是私有 issue, 0:否，1:是
     */
    securityHole?: 0 | 1,
    /**
     * 迭代 ID
     */
    scrumSprintId?: number,
    /**
     * 版本 ID
     */
    scrumVersionId?: number,
    /**
     * 看板 ID
     */
    kanbanId?: number,
    /**
     * 看板的栏 ID
     */
    kanbanColumnId?: number,
    /**
     * 预估工时 (单位：小时，支持两位小数)
     */
    estimatedDuration?: number,
    /**
     * 预估工时。（单位：分钟）
     */
    duration?: number,
    /**
     * 变更的状态 {source_id1: target_id1, source_id2: target_id2}
     */
    changeStates?: Object,
    /**
     * 选择的层级结构（relevance: 改为关联关系，independence: 取消父子关系）
     */
    hierarchyChangeMethod?: 'relevance' | 'independence',
  }): CancelablePromise<IssueDetail> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issues/{issue_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      formData: {
        'access_token': accessToken,
        'title': title,
        'description': description,
        'assignee_id': assigneeId,
        'collaborator_ids': collaboratorIds,
        'issue_type_id': issueTypeId,
        'issue_state_id': issueStateId,
        'program_id': programId,
        'project_id': projectId,
        'milestone_id': milestoneId,
        'label_ids': labelIds,
        'priority': priority,
        'parent_id': parentId,
        'branch': branch,
        'plan_started_at': planStartedAt,
        'deadline': deadline,
        'started_at': startedAt,
        'finished_at': finishedAt,
        'security_hole': securityHole,
        'scrum_sprint_id': scrumSprintId,
        'scrum_version_id': scrumVersionId,
        'kanban_id': kanbanId,
        'kanban_column_id': kanbanColumnId,
        'estimated_duration': estimatedDuration,
        'duration': duration,
        'change_states': changeStates,
        'hierarchy_change_method': hierarchyChangeMethod,
      },
    });
  }

  /**
   * 删除任务
   * 删除任务
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssuesIssueId({
    enterpriseId,
    issueId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issues/{issue_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 新增任务表态
   * 新增任务表态
   * @returns IssueReaction 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdIssuesIssueIdReaction({
    enterpriseId,
    issueId,
    text,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务的 id
     */
    issueId: string,
    /**
     * 表情
     */
    text: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueReaction> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issues/{issue_id}/reaction',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      formData: {
        'access_token': accessToken,
        'text': text,
      },
    });
  }

  /**
   * 星标任务
   * 星标任务
   * @returns any 星标任务
   * @throws ApiError
   */
  public postEnterpriseIdIssuesIssueIdStar({
    enterpriseId,
    issueId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issues/{issue_id}/star',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      formData: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 取消星标任务
   * 取消星标任务
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssuesIssueIdStar({
    enterpriseId,
    issueId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issues/{issue_id}/star',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取任务的操作日志列表
   * 获取任务的操作日志列表
   * @returns IssueOperateLog 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueIdOperateLogs({
    enterpriseId,
    issueId,
    accessToken,
    prevId,
    lastId,
    limit,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务的 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 前一个日志 id, 用作分页
     */
    prevId?: number,
    /**
     * 后一个日志 id, 用作分页
     */
    lastId?: number,
    /**
     * 限制多少个日志记录
     */
    limit?: number,
  }): CancelablePromise<IssueOperateLog> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}/operate_logs',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
        'prev_id': prevId,
        'last_id': lastId,
        'limit': limit,
      },
    });
  }

  /**
   * 获取授权用户对任务的权限
   * 获取授权用户对任务的权限
   * @returns IssueAuth 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueIdAuths({
    enterpriseId,
    issueId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务的 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueAuth> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}/auths',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取当前任务可流转的下一状态
   * 获取当前任务可流转的下一状态
   * @returns IssueTypeStateRef 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueIdIssueStates({
    enterpriseId,
    issueId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务的 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueTypeStateRef> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}/issue_states',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取任务的关联任务
   * 获取任务的关联任务
   * @returns LinkIssue 获取任务的关联任务
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueIdLinkIssues({
    enterpriseId,
    issueId,
    accessToken,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 关联顺序 (none -> 普通，pre -> 紧前，latter -> 紧后)
     */
    direction?: 'none' | 'pre' | 'latter',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<LinkIssue> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}/link_issues',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 为指定任务添加关联任务
   * 为指定任务添加关联任务
   * @returns IssueDetailWithRelatedInfos 为指定任务添加关联任务
   * @throws ApiError
   */
  public postEnterpriseIdIssuesIssueIdRelatedIssue({
    enterpriseId,
    issueId,
    linkIssueId,
    refType,
    accessToken,
    direction,
    resultType,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 需要关联的任务 id
     */
    linkIssueId: number,
    /**
     * 关联关系 (normal, finish_to_finish, finish_to_start, start_to_start, start_to_finish)
     */
    refType: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 关联关系的方向 (none, pre, latter)
     */
    direction?: string,
    /**
     * 返回结果类型：包括 issue, dependence
     */
    resultType?: string,
  }): CancelablePromise<IssueDetailWithRelatedInfos> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issues/{issue_id}/related_issue',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      formData: {
        'access_token': accessToken,
        'link_issue_id': linkIssueId,
        'ref_type': refType,
        'direction': direction,
        'result_type': resultType,
      },
    });
  }

  /**
   * 更新任务的关联关系
   * 更新任务的关联关系
   * @returns IssueDetailWithRelatedInfos 更新任务的关联关系
   * @throws ApiError
   */
  public putEnterpriseIdIssuesIssueIdRelatedIssueLinkIssueId({
    enterpriseId,
    issueId,
    linkIssueId,
    accessToken,
    refType,
    direction,
    resultType,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 需要关联的任务 id
     */
    linkIssueId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 关联关系 (normal, finish_to_finish, finish_to_start, start_to_start, start_to_finish)
     */
    refType?: string,
    /**
     * 关联关系的方向 (none, pre, latter)
     */
    direction?: string,
    /**
     * 返回结果类型：包括 issue, dependence
     */
    resultType?: string,
  }): CancelablePromise<IssueDetailWithRelatedInfos> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issues/{issue_id}/related_issue/{link_issue_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
        'link_issue_id': linkIssueId,
      },
      formData: {
        'access_token': accessToken,
        'ref_type': refType,
        'direction': direction,
        'result_type': resultType,
      },
    });
  }

  /**
   * 移除任务的关联关系
   * 移除任务的关联关系
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssuesIssueIdRelatedIssueLinkIssueId({
    enterpriseId,
    issueId,
    linkIssueId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 待删除的关联的任务 id
     */
    linkIssueId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issues/{issue_id}/related_issue/{link_issue_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
        'link_issue_id': linkIssueId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取任务关联的 Pull Request
   * 获取任务关联的 Pull Request
   * @returns any 获取任务关联的 Pull Request
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueIdLinkPullRequest({
    enterpriseId,
    issueId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}/link_pull_request',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 任务关联 Pull Request
   * 任务关联 Pull Request
   * @returns any 任务关联 Pull Request
   * @throws ApiError
   */
  public postEnterpriseIdIssuesIssueIdLinkPullRequestPullRequestId({
    enterpriseId,
    issueId,
    pullRequestId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * Pull Request id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issues/{issue_id}/link_pull_request/{pull_request_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 解除任务与 Pull Request 的关联
   * 解除任务与 Pull Request 的关联
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssuesIssueIdLinkPullRequestPullRequestId({
    enterpriseId,
    issueId,
    pullRequestId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * Pull Request id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issues/{issue_id}/link_pull_request/{pull_request_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取任务下的评论列表
   * 获取任务下的评论列表
   * @returns IssueNote 获取任务下的评论列表
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueIdNotes({
    enterpriseId,
    issueId,
    accessToken,
    sort,
    direction = 'desc',
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (name || created_at)
     */
    sort?: 'name' | 'created_at',
    /**
     * 可选。升序/降序
     */
    direction?: 'asc' | 'desc',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<IssueNote> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}/notes',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 评论任务
   * 评论任务
   * @returns IssueNote 评论任务
   * @throws ApiError
   */
  public postEnterpriseIdIssuesIssueIdNotes({
    enterpriseId,
    issueId,
    body,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 评论内容
     */
    body: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueNote> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issues/{issue_id}/notes',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      formData: {
        'access_token': accessToken,
        'body': body,
      },
    });
  }

  /**
   * 修改任务下的评论
   * 修改任务下的评论
   * @returns IssueNote 修改任务下的评论
   * @throws ApiError
   */
  public putEnterpriseIdIssuesIssueIdNotesNoteId({
    enterpriseId,
    issueId,
    body,
    noteId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 评论内容
     */
    body: string,
    noteId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueNote> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issues/{issue_id}/notes/{note_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
        'note_id': noteId,
      },
      formData: {
        'access_token': accessToken,
        'body': body,
      },
    });
  }

  /**
   * 删除某任务下评论
   * 删除某任务下评论
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssuesIssueIdNotesNoteId({
    enterpriseId,
    issueId,
    noteId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务 id
     */
    issueId: string,
    /**
     * 评论的 id
     */
    noteId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issues/{issue_id}/notes/{note_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
        'note_id': noteId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取任务附件
   * 获取任务附件
   * @returns AttachFile 获取任务附件
   * @throws ApiError
   */
  public getEnterpriseIdIssuesIssueIdAttachFiles({
    enterpriseId,
    issueId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务的 id
     */
    issueId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<AttachFile> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issues/{issue_id}/attach_files',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 删除任务附件
   * 删除任务附件
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssuesIssueIdAttachFilesAttachFileId({
    enterpriseId,
    issueId,
    attachFileId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务的 id
     */
    issueId: string,
    /**
     * 附件 id
     */
    attachFileId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issues/{issue_id}/attach_files/{attach_file_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_id': issueId,
        'attach_file_id': attachFileId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
