/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Reaction } from '../models/Reaction';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ReactionClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 新增表态
   * 新增表态
   * @returns Reaction 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdReactions({
    targetType,
    targetId,
    text,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 目标类型 (PullRequest, Note, Issue, TestNote)
     */
    targetType: string,
    /**
     * 目标对象的 id
     */
    targetId: number,
    /**
     * 表态内容
     */
    text: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<Reaction>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/reactions',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'target_type': targetType,
        'target_id': targetId,
        'text': text,
      },
    });
  }

  /**
   * 删除表态
   * 删除表态
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdReactionsReactionId({
    reactionId,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 表态的 id
     */
    reactionId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/reactions/{reaction_id}',
      path: {
        'reaction_id': reactionId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
