/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { User } from '../models/User';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class UsersClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取授权用户的资料
   * 获取授权用户的资料
   * @returns User 返回格式
   * @throws ApiError
   */
  public getUsers({
    accessToken,
  }: {
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<User> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/users',
      query: {
        'access_token': accessToken,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

}
