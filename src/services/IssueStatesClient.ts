/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { IssueState } from '../models/IssueState';
import type { IssueStateDetail } from '../models/IssueStateDetail';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class IssueStatesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取任务状态列表
   * 获取任务状态列表
   * @returns IssueState 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueStates({
    enterpriseId,
    accessToken,
    sort,
    direction,
    search,
    issueTypeCategory,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (created_at、updated_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 任务类型属性
     */
    issueTypeCategory?: 'task' | 'bug' | 'requirement' | 'feature',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<IssueState>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_states',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'search': search,
        'issue_type_category': issueTypeCategory,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新增任务状态
   * 新增任务状态
   * @returns IssueState 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdIssueStates({
    enterpriseId,
    title,
    icon,
    accessToken,
    command,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务状态名称
     */
    title: string,
    /**
     * 任务状态图标
     */
    icon: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务状态指令
     */
    command?: string,
  }): CancelablePromise<IssueState> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/issue_states',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'title': title,
        'icon': icon,
        'command': command,
      },
    });
  }

  /**
   * 任务状态详情
   * 任务状态详情
   * @returns IssueStateDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdIssueStatesIssueStateId({
    enterpriseId,
    issueStateId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务状态 id
     */
    issueStateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<IssueStateDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/issue_states/{issue_state_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_state_id': issueStateId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新任务状态
   * 更新任务状态
   * @returns IssueState 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdIssueStatesIssueStateId({
    enterpriseId,
    title,
    icon,
    issueStateId,
    accessToken,
    command,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 任务状态名称
     */
    title: string,
    /**
     * 任务状态图标
     */
    icon: string,
    issueStateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 任务状态指令
     */
    command?: string,
  }): CancelablePromise<IssueState> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/issue_states/{issue_state_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_state_id': issueStateId,
      },
      formData: {
        'access_token': accessToken,
        'title': title,
        'icon': icon,
        'command': command,
      },
    });
  }

  /**
   * 删除任务状态
   * 删除任务状态
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdIssueStatesIssueStateId({
    enterpriseId,
    issueStateId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    issueStateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/issue_states/{issue_state_id}',
      path: {
        'enterprise_id': enterpriseId,
        'issue_state_id': issueStateId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
