/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EmailInvitation } from '../models/EmailInvitation';
import type { Event } from '../models/Event';
import type { GroupWithAuth } from '../models/GroupWithAuth';
import type { Member } from '../models/Member';
import type { MemberBulkModify } from '../models/MemberBulkModify';
import type { ProgramWithAuth } from '../models/ProgramWithAuth';
import type { Project } from '../models/Project';
import type { ProjectWithAuth } from '../models/ProjectWithAuth';
import type { User } from '../models/User';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class MembersClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取企业成员列表
   * 获取企业成员列表
   * @returns Member 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembers({
    enterpriseId,
    accessToken,
    ident,
    isBlock,
    groupId,
    roleId,
    search,
    sort,
    direction,
    includeMemberHistories,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 角色类型。(viewer: 观察者; member: 普通成员; outsourced_member: 外���成员; human_resources: 人事管理员; admin: 管理员; super_admin: 超级管理员; enterprise_owner: 企业拥有者;)
     */
    ident?: string,
    /**
     * 1: 筛选已锁定的用户。
     */
    isBlock?: number,
    /**
     * 筛选团队的成员列表
     */
    groupId?: number,
    /**
     * 筛选指定角色的成员列表
     */
    roleId?: number,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 排序字段 (created_at: 创建时间 remark: 在企业的备注)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 是否包含离职成员历史（true/false）
     */
    includeMemberHistories?: boolean,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Member>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'ident': ident,
        'is_block': isBlock,
        'group_id': groupId,
        'role_id': roleId,
        'search': search,
        'sort': sort,
        'direction': direction,
        'include_member_histories': includeMemberHistories,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 成员批量移出企业
   * 成员批量移出企业
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdMembers({
    enterpriseId,
    accessToken,
    userIds,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 成员 ids, 逗号 (,) 隔开空
     */
    userIds?: string,
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/members',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'user_ids': userIds,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 通过邮箱获取成员详情
   * 通过邮箱获取成员详情
   * @returns Member 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembersByEmail({
    email,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 用户邮箱
     */
    email: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Member> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members/by_email',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'email': email,
      },
    });
  }

  /**
   * 获取企业成员详情
   * 获取企业成员详情
   * @returns Member 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembersUserId({
    enterpriseId,
    userId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户 id
     */
    userId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Array<Member>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members/{user_id}',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 更新企业成员信息
   * 更新企业成员信息
   * @returns Member 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMembersUserId({
    enterpriseId,
    userId,
    accessToken,
    remark,
    occupation,
    phone,
    roleId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 企业备注
     */
    remark?: string,
    /**
     * 职位信息
     */
    occupation?: string,
    /**
     * 手机号码
     */
    phone?: string,
    /**
     * 成员角色
     */
    roleId?: string,
  }): CancelablePromise<Array<Member>> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/members/{user_id}',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      formData: {
        'access_token': accessToken,
        'remark': remark,
        'occupation': occupation,
        'phone': phone,
        'role_id': roleId,
      },
    });
  }

  /**
   * 把成员移出企业
   * 把成员移出企业
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdMembersUserId({
    enterpriseId,
    userId,
    accessToken,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/members/{user_id}',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      query: {
        'access_token': accessToken,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 给成员添加/移出仓库
   * 给成员添加/移出仓库
   * @returns MemberBulkModify 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMembersUserIdMemberProjects({
    enterpriseId,
    userId,
    accessToken,
    addIds,
    removeIds,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 加入 ids, 逗号 (,) 隔开。add_ids，remove_ids 至少填一项，不能都为空
     */
    addIds?: string,
    /**
     * 退出 ids, 逗号 (,) 隔开
     */
    removeIds?: string,
  }): CancelablePromise<MemberBulkModify> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/members/{user_id}/member_projects',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      formData: {
        'access_token': accessToken,
        'add_ids': addIds,
        'remove_ids': removeIds,
      },
    });
  }

  /**
   * 给成员添加/移出团队
   * 给成员添加/移出团队
   * @returns MemberBulkModify 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMembersUserIdMemberGroups({
    enterpriseId,
    userId,
    accessToken,
    addIds,
    removeIds,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 加入 ids, 逗号 (,) 隔开。add_ids，remove_ids 至少填一项，不能都为空
     */
    addIds?: string,
    /**
     * 退出 ids, 逗号 (,) 隔开
     */
    removeIds?: string,
  }): CancelablePromise<MemberBulkModify> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/members/{user_id}/member_groups',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      formData: {
        'access_token': accessToken,
        'add_ids': addIds,
        'remove_ids': removeIds,
      },
    });
  }

  /**
   * 给成员添加/移出项目
   * 给成员添加/移出项目
   * @returns MemberBulkModify 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMembersUserIdMemberPrograms({
    enterpriseId,
    userId,
    accessToken,
    addIds,
    removeIds,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 加入 ids, 逗号 (,) 隔开。add_ids，remove_ids 至少填一项，不能都为空
     */
    addIds?: string,
    /**
     * 退出 ids, 逗号 (,) 隔开
     */
    removeIds?: string,
  }): CancelablePromise<MemberBulkModify> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/members/{user_id}/member_programs',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      formData: {
        'access_token': accessToken,
        'add_ids': addIds,
        'remove_ids': removeIds,
      },
    });
  }

  /**
   * 添加成员到企业 - 邮件邀请
   * 添加成员到企业 - 邮件邀请
   * @returns EmailInvitation 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdMembersEmailInvitation({
    enterpriseId,
    emails,
    accessToken,
    roleId,
    needCheck,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 邮件列表，逗号 (,) 隔开。每次邀请最多只能发送 20 封邀请邮件
     */
    emails: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 企业角色的 ID
     */
    roleId?: number,
    /**
     * 是否需要管理员审核。1：需要，0：不需要
     */
    needCheck?: 0 | 1,
  }): CancelablePromise<EmailInvitation> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/members/email_invitation',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'role_id': roleId,
        'need_check': needCheck,
        'emails': emails,
      },
    });
  }

  /**
   * 批量修改成员角色
   * 批量修改成员角色
   * @returns any 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMembersBatchChangeRole({
    enterpriseId,
    roleId,
    userIds,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员角色 ID
     */
    roleId: number,
    /**
     * 成员 ids, 逗号 (,) 隔开空
     */
    userIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/members/batch_change_role',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'role_id': roleId,
        'user_ids': userIds,
      },
    });
  }

  /**
   * 发送密码重置邮件
   * 发送密码重置邮件
   * @returns any 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdMembersUserIdResetPasswordEmail({
    enterpriseId,
    userId,
    accessToken,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/members/{user_id}/reset_password_email',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      formData: {
        'access_token': accessToken,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 锁定/解除锁定用户
   * 锁定/解除锁定用户
   * @returns Member 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMembersUserIdBlock({
    enterpriseId,
    userId,
    isBlock,
    accessToken,
    blockMsg,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * true: 锁定，false: 解除锁定
     */
    isBlock: boolean,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 锁定原因
     */
    blockMsg?: string,
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<Array<Member>> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/members/{user_id}/block',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      formData: {
        'access_token': accessToken,
        'is_block': isBlock,
        'block_msg': blockMsg,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 企业成员动态
   * 企业成员动态
   * @returns Event 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembersUserIdEvents({
    enterpriseId,
    userId,
    accessToken,
    qt,
    startDate,
    endDate,
    page = 1,
    perPage,
    prevId,
    limit,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户 id
     */
    userId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 上一次动态列表中最小动态 ID (返回列表不包含该 ID 记录)
     */
    prevId?: number,
    /**
     * 每次获取动态的条数
     */
    limit?: number,
  }): CancelablePromise<Array<Event>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members/{user_id}/events',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
        'prev_id': prevId,
        'limit': limit,
      },
    });
  }

  /**
   * 获取成员加入的仓库列表
   * 获取成员加入的仓库列表
   * @returns ProjectWithAuth 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembersUserIdProjects({
    enterpriseId,
    userId,
    accessToken,
    qt,
    notFork,
    scope,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户 id
     */
    userId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * true: 非 fork 仓库，false: 不过滤
     */
    notFork?: boolean,
    /**
     * 筛选项 (not_joined: 未加入的仓库列表)
     */
    scope?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProjectWithAuth>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members/{user_id}/projects',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'not_fork': notFork,
        'scope': scope,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取成员 fork 的企业仓库列表
   * 获取成员 fork 的企业仓库列表
   * @returns Project 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembersUserIdForkProjects({
    enterpriseId,
    userId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户 id
     */
    userId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Array<Project>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members/{user_id}/fork_projects',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 把成员移出仓库
   * 把成员移出仓库
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdMembersUserIdProjectsProjectId({
    enterpriseId,
    userId,
    projectId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 仓库 id
     */
    projectId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/members/{user_id}/projects/{project_id}',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取成员加入的项目列表
   * 获取成员加入的项目列表
   * @returns ProgramWithAuth 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembersUserIdPrograms({
    enterpriseId,
    userId,
    accessToken,
    qt,
    page = 1,
    perPage,
    scope,
    search,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户 id
     */
    userId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 筛选项 (not_joined: 未加入的项目列表)
     */
    scope?: string,
    /**
     * 搜索关键字
     */
    search?: string,
  }): CancelablePromise<Array<ProgramWithAuth>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members/{user_id}/programs',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
        'scope': scope,
        'search': search,
      },
    });
  }

  /**
   * 获取成员加入的团队列表
   * 获取成员加入的团队列表
   * @returns GroupWithAuth 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMembersUserIdGroups({
    enterpriseId,
    userId,
    accessToken,
    qt,
    page = 1,
    perPage,
    scope,
    search,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户 id
     */
    userId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 筛选项 (not_joined: 未加入的团队列表，admin: 成员管理的团队列表)
     */
    scope?: string,
    /**
     * 搜索关键字
     */
    search?: string,
  }): CancelablePromise<Array<GroupWithAuth>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/members/{user_id}/groups',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
        'scope': scope,
        'search': search,
      },
    });
  }

  /**
   * 把成员移出团队
   * 把成员移出团队
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdMembersUserIdGroupsGroupId({
    enterpriseId,
    userId,
    groupId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 团队 id
     */
    groupId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/members/{user_id}/groups/{group_id}',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
        'group_id': groupId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 发送密码重置邮件
   * 发送密码重置邮件
   * @returns any 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdMembersUserIdResetDefaultPassword({
    enterpriseId,
    userId,
    accessToken,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 成员 id
     */
    userId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/members/{user_id}/reset_default_password',
      path: {
        'enterprise_id': enterpriseId,
        'user_id': userId,
      },
      formData: {
        'access_token': accessToken,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 企业外的成员 - 从企业组织或仓库中移除 (单个/批量)
   * 企业外的成员 - 从企业组织或仓库中移除 (单个/批量)
   * @returns User 返回格式
   * @throws ApiError
   */
  public deleteEnterpriseIdOutsideMembers({
    enterpriseId,
    userIds,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 企业外成员用户 id，多个 id 逗号 (,) 隔开
     */
    userIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<User>> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/outside-members',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'user_ids': userIds,
      },
    });
  }

}
