/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { MilestoneDetail } from '../models/MilestoneDetail';
import type { MilestoneList } from '../models/MilestoneList';
import type { UserWithRemark } from '../models/UserWithRemark';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class MilestonesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取里程碑列表
   * 获取里程碑列表
   * @returns MilestoneList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMilestones({
    enterpriseId,
    accessToken,
    programId,
    projectId,
    state,
    assigneeIds,
    authorIds,
    search,
    sort,
    direction,
    scope,
    onlyComponentEnabled,
    dropboxGroup,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 仓库 id
     */
    projectId?: number,
    /**
     * 状态，开启的:active，待开始:pending，进行中:processing，已关闭:closed，已逾期:overdued，未过期：not_overdue
     */
    state?: string,
    /**
     * 负责人 id 以，分隔的字符串
     */
    assigneeIds?: string,
    /**
     * 创建者 id 以，分隔的字符串
     */
    authorIds?: string,
    /**
     * 搜索字符串
     */
    search?: string,
    /**
     * 排序字段 (title、created_at、start_date、due_date)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 筛选当前用户参与的里程碑
     */
    scope?: 'joined',
    /**
     * 是否只列出启用里程碑组件的项目下的里程碑
     */
    onlyComponentEnabled?: boolean,
    /**
     * 是否需要按开启和关闭两个状态在下拉框内分组
     */
    dropboxGroup?: boolean,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<MilestoneList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/milestones',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
        'project_id': projectId,
        'state': state,
        'assignee_ids': assigneeIds,
        'author_ids': authorIds,
        'search': search,
        'sort': sort,
        'direction': direction,
        'scope': scope,
        'only_component_enabled': onlyComponentEnabled,
        'dropbox_group': dropboxGroup,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新建里程碑
   * 新建里程碑
   * @returns MilestoneDetail 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdMilestones({
    enterpriseId,
    title,
    dueDate,
    accessToken,
    startDate,
    description,
    programId,
    projectIds,
    assigneeId,
  }: {
    /**
     * 企业 ID
     */
    enterpriseId: number,
    /**
     * 里程碑标题
     */
    title: string,
    /**
     * 里程碑结束日期如：2020-08-13
     */
    dueDate: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 里程碑开始日期如：2020-08-13
     */
    startDate?: string,
    /**
     * 里程碑描述
     */
    description?: string,
    /**
     * 关联项目 ID
     */
    programId?: number,
    /**
     * 关联仓库 ID, 以，分隔的字符串
     */
    projectIds?: string,
    /**
     * 里程碑负责人 ID
     */
    assigneeId?: number,
  }): CancelablePromise<MilestoneDetail> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/milestones',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'title': title,
        'due_date': dueDate,
        'start_date': startDate,
        'description': description,
        'program_id': programId,
        'project_ids': projectIds,
        'assignee_id': assigneeId,
      },
    });
  }

  /**
   * 获取里程碑信息
   * 获取里程碑信息
   * @returns MilestoneDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMilestonesMilestoneId({
    enterpriseId,
    milestoneId,
    accessToken,
  }: {
    /**
     * 企业 ID
     */
    enterpriseId: number,
    /**
     * 里程碑 ID
     */
    milestoneId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<MilestoneDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/milestones/{milestone_id}',
      path: {
        'enterprise_id': enterpriseId,
        'milestone_id': milestoneId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 编辑里程碑
   * 编辑里程碑
   * @returns MilestoneDetail 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdMilestonesMilestoneId({
    enterpriseId,
    milestoneId,
    accessToken,
    title,
    dueDate,
    startDate,
    stateEvent,
    description,
    programId,
    projectIds,
    assigneeId,
    top,
  }: {
    /**
     * 企业 ID
     */
    enterpriseId: number,
    /**
     * 里程碑 ID
     */
    milestoneId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 里程碑标题
     */
    title?: string,
    /**
     * 里程碑结束日期如：2020-08-13
     */
    dueDate?: string,
    /**
     * 里程碑开始日期如：2020-08-13
     */
    startDate?: string,
    /**
     * 关闭或重新开启里程碑
     */
    stateEvent?: 'activate' | 'close',
    /**
     * 里程碑描述
     */
    description?: string,
    /**
     * 关联项目 ID
     */
    programId?: number,
    /**
     * 关联仓库 ID, 以，分隔的字符串
     */
    projectIds?: string,
    /**
     * 里程碑负责人 ID
     */
    assigneeId?: number,
    /**
     * 是否置顶
     */
    top?: 0 | 1,
  }): CancelablePromise<MilestoneDetail> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/milestones/{milestone_id}',
      path: {
        'enterprise_id': enterpriseId,
        'milestone_id': milestoneId,
      },
      formData: {
        'access_token': accessToken,
        'title': title,
        'due_date': dueDate,
        'start_date': startDate,
        'state_event': stateEvent,
        'description': description,
        'program_id': programId,
        'project_ids': projectIds,
        'assignee_id': assigneeId,
        'top': top,
      },
    });
  }

  /**
   * 删除里程碑
   * 删除里程碑
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdMilestonesMilestoneId({
    enterpriseId,
    milestoneId,
    accessToken,
  }: {
    /**
     * 企业 ID
     */
    enterpriseId: number,
    /**
     * 里程碑 ID
     */
    milestoneId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/milestones/{milestone_id}',
      path: {
        'enterprise_id': enterpriseId,
        'milestone_id': milestoneId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 里程碑参与人列表
   * 里程碑参与人列表
   * @returns UserWithRemark 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdMilestonesMilestoneIdParticipants({
    enterpriseId,
    milestoneId,
    accessToken,
  }: {
    /**
     * 企业 ID
     */
    enterpriseId: number,
    /**
     * 里程碑 ID
     */
    milestoneId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<UserWithRemark> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/milestones/{milestone_id}/participants',
      path: {
        'enterprise_id': enterpriseId,
        'milestone_id': milestoneId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
