/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EnterpriseDeployKey } from '../models/EnterpriseDeployKey';
import type { Project } from '../models/Project';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class DeployKeyClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 查看企业部署公钥
   * 查看企业部署公钥
   * @returns EnterpriseDeployKey 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDeployKeys({
    enterpriseId,
    accessToken,
    projectId,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 仓库 id
     */
    projectId?: number,
    /**
     * 公钥 SHA256 指纹或标题
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<EnterpriseDeployKey> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/deploy_keys',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'project_id': projectId,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 添加部署公钥
   * 添加部署公钥
   * @returns EnterpriseDeployKey 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDeployKeys({
    title,
    key,
    projectIds,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 标题
     */
    title: string,
    /**
     * 公钥
     */
    key: string,
    /**
     * 部署仓库 id 列表，用英文逗号分隔
     */
    projectIds: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<EnterpriseDeployKey> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/deploy_keys',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'title': title,
        'key': key,
        'project_ids': projectIds,
      },
    });
  }

  /**
   * 查看公钥部署的仓库
   * 查看公钥部署的仓库
   * @returns Project 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDeployKeysDeployKeyIdProjects({
    deployKeyId,
    enterpriseId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 部署公钥 id
     */
    deployKeyId: number,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Project> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/deploy_keys/{deploy_key_id}/projects',
      path: {
        'deploy_key_id': deployKeyId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 部署公钥添加仓库
   * 部署公钥添加仓库
   * @returns Project 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDeployKeysDeployKeyIdProjects({
    deployKeyId,
    projectIds,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 部署公钥 id
     */
    deployKeyId: number,
    /**
     * 添加仓库 id，英文逗号分隔
     */
    projectIds: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Project> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/deploy_keys/{deploy_key_id}/projects',
      path: {
        'deploy_key_id': deployKeyId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'project_ids': projectIds,
      },
    });
  }

  /**
   * 部署公钥移除仓库
   * 部署公钥移除仓库
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdDeployKeysDeployKeyIdProjects({
    deployKeyId,
    projectIds,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 部署公钥 id
     */
    deployKeyId: number,
    /**
     * 添加仓库 id，英文逗号分隔
     */
    projectIds: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/deploy_keys/{deploy_key_id}/projects',
      path: {
        'deploy_key_id': deployKeyId,
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'project_ids': projectIds,
      },
    });
  }

  /**
   * 修改部署公钥
   * 修改部署公钥
   * @returns EnterpriseDeployKey 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDeployKeysDeployKeyId({
    deployKeyId,
    title,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 部署公钥 id
     */
    deployKeyId: number,
    /**
     * 标题
     */
    title: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<EnterpriseDeployKey> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/deploy_keys/{deploy_key_id}',
      path: {
        'deploy_key_id': deployKeyId,
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'title': title,
      },
    });
  }

}
