/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EnterpriseRole } from '../models/EnterpriseRole';
import type { EnterpriseRoleDetail } from '../models/EnterpriseRoleDetail';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class EnterpriseRolesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取企业角色列表
   * 获取企业角色列表
   * @returns EnterpriseRole 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdEnterpriseRoles({
    enterpriseId,
    accessToken,
    sort,
    direction,
    scope,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (created_at: 创建时间 updated_at: 更新时间)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 筛选项 (system: 系统默认类型; custom: 自定义类型; admin: 管理员类型; can_operate: 非企业拥有者角色，can_invite: 能添加成员角色)
     */
    scope?: string,
    /**
     * 角色名称搜索字符串
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<EnterpriseRole>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/enterprise_roles',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'scope': scope,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新增企业角色
   * 新增企业角色
   * @returns EnterpriseRoleDetail 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdEnterpriseRoles({
    name,
    enterpriseId,
    accessToken,
    description,
    isDefault,
    rule,
  }: {
    /**
     * 角色名称
     */
    name: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 角色的描述
     */
    description?: string,
    /**
     * 是否设置为企业默认角色
     */
    isDefault?: boolean,
    /**
     *           参数名含义：
     * week_report: 周报
     * issue: 任务
     * program: 项目
     * project: 仓库
     * project_group: 仓库组
     * doc: 文档
     * test_plan: 测试计划
     * test_repository: 测试用例库
     * giteego_pipeline: 流水线
     * statistic: 统计
     * admin: 管理权限
     * 格式如下（具体可对照着 web 端的权限设置）：
     * "rule": {"week_report":{"general":{"read":true,"create":true,"update_history":false},"global":{"read":true}},
     * "issue":{"general":{"read":true,"create":true,"modify_attr":true,"modify_main":true,"destroy":true},"global":{"read":true,"create":true,"modify_attr":true,"modify_main":true,"destroy":true}},
     * "program":{"general":{"read":true,"create":true,"destroy":true,"setting":true},"global":{"read":true,"create":true,"destroy":true,"setting":true}},
     * "project":{"general":{"read":true,"create":true,"destroy":true,"pull":true,"push":true,"merge_pr":true,"setting":true},"global":{"read":true,"create":true,"destroy":true,"pull":true,"push":true,"open_source":true,"read_internal_source":true,"merge_pr":true,"setting":true}},
     * "project_group":{"general":{"read":true,"create":true,"destroy":true,"setting":true},"global":{"read":true,"create":true,"destroy":true,"setting":true}},
     * "doc":{"general":{"read":true,"create":true,"update":true,"destroy":true,"setting":true},"global":{"read":true,"create":true,"update":true,"destroy":true,"setting":true}},
     * "member":{"read":true},"statistic":{"read":true},
     * "admin":{"member":{"add_member":true,"destroy":true,"setting":true,"create_group":true,"manage_group":true,"change_role":false},
     * "log":{"read":true},
     * "emergency_log":{"read":true},
     * "git_lfs":{"read":true},
     * "order":{"read":true,"setting":true},
     * "hook":{"read":true,"setting":true},
     * "key":{"read":true,"setting":true},
     * "issue_type_status":{"read":true,"setting":true},
     * "label_manage":{"read":true,"setting":true},
     * "info":{"read":true,"setting":true},
     * "role":{"read":true,"setting":false},
     * "emergency":{"read":true,"setting":true},
     * "security":{"read":true,"setting":true},
     * "notice":{"read":true,"create":true},
     * "message":{"read":true,"create":true},"is_admin":true}}}
     *
     */
    rule?: string,
  }): CancelablePromise<Array<EnterpriseRoleDetail>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/enterprise_roles',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'description': description,
        'is_default': isDefault,
        'rule': rule,
      },
    });
  }

  /**
   * 导出企业角色
   * 导出企业角色
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdEnterpriseRolesExportRolesPermission({
    enterpriseId,
    accessToken,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/enterprise_roles/export_roles_permission',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取企业角色详情
   * 获取企业角色详情
   * @returns EnterpriseRoleDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdEnterpriseRolesEnterpriseRoleId({
    enterpriseId,
    enterpriseRoleId,
    accessToken,
  }: {
    enterpriseId: number,
    enterpriseRoleId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<EnterpriseRoleDetail>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/enterprise_roles/{enterprise_role_id}',
      path: {
        'enterprise_id': enterpriseId,
        'enterprise_role_id': enterpriseRoleId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新企业角色
   * 更新企业角色
   * @returns EnterpriseRoleDetail 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdEnterpriseRolesEnterpriseRoleId({
    enterpriseId,
    enterpriseRoleId,
    accessToken,
    name,
    description,
    isDefault,
    rule,
  }: {
    enterpriseId: number,
    enterpriseRoleId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 角色名称
     */
    name?: string,
    /**
     * 角色的描述
     */
    description?: string,
    /**
     * 是否设置为企业默认角色
     */
    isDefault?: boolean,
    /**
     *           参数名含义：
     * week_report: 周报
     * issue: 任务
     * program: 项目
     * project: 仓库
     * project_group: 仓库组
     * doc: 文档
     * test_plan: 测试计划
     * test_repository: 测试用例库
     * giteego_pipeline: 流水线
     * statistic: 统计
     * admin: 管理权限
     * 格式如下（具体可对照着 web 端的权限设置）：
     * "rule": {"week_report":{"general":{"read":true,"create":true,"update_history":false},"global":{"read":true}},
     * "issue":{"general":{"read":true,"create":true,"modify_attr":true,"modify_main":true,"destroy":true},"global":{"read":true,"create":true,"modify_attr":true,"modify_main":true,"destroy":true}},
     * "program":{"general":{"read":true,"create":true,"destroy":true,"setting":true},"global":{"read":true,"create":true,"destroy":true,"setting":true}},
     * "project":{"general":{"read":true,"create":true,"destroy":true,"pull":true,"push":true,"merge_pr":true,"setting":true},"global":{"read":true,"create":true,"destroy":true,"pull":true,"push":true,"open_source":true,"read_internal_source":true,"merge_pr":true,"setting":true}},
     * "project_group":{"general":{"read":true,"create":true,"destroy":true,"setting":true},"global":{"read":true,"create":true,"destroy":true,"setting":true}},
     * "doc":{"general":{"read":true,"create":true,"update":true,"destroy":true,"setting":true},"global":{"read":true,"create":true,"update":true,"destroy":true,"setting":true}},
     * "member":{"read":true},"statistic":{"read":true},
     * "admin":{"member":{"add_member":true,"destroy":true,"setting":true,"create_group":true,"manage_group":true,"change_role":false},
     * "log":{"read":true},
     * "emergency_log":{"read":true},
     * "git_lfs":{"read":true},
     * "order":{"read":true,"setting":true},
     * "hook":{"read":true,"setting":true},
     * "key":{"read":true,"setting":true},
     * "issue_type_status":{"read":true,"setting":true},
     * "label_manage":{"read":true,"setting":true},
     * "info":{"read":true,"setting":true},
     * "role":{"read":true,"setting":false},
     * "emergency":{"read":true,"setting":true},
     * "security":{"read":true,"setting":true},
     * "notice":{"read":true,"create":true},
     * "message":{"read":true,"create":true},"is_admin":true}}}
     *
     */
    rule?: string,
  }): CancelablePromise<Array<EnterpriseRoleDetail>> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/enterprise_roles/{enterprise_role_id}',
      path: {
        'enterprise_id': enterpriseId,
        'enterprise_role_id': enterpriseRoleId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'description': description,
        'is_default': isDefault,
        'rule': rule,
      },
    });
  }

  /**
   * 删除角色
   * 删除角色
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdEnterpriseRolesEnterpriseRoleId({
    enterpriseId,
    enterpriseRoleId,
    accessToken,
  }: {
    enterpriseId: number,
    enterpriseRoleId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/enterprise_roles/{enterprise_role_id}',
      path: {
        'enterprise_id': enterpriseId,
        'enterprise_role_id': enterpriseRoleId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
