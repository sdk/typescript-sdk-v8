/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CherryPick } from '../models/CherryPick';
import type { CommitList } from '../models/CommitList';
import type { DiffPosition } from '../models/DiffPosition';
import type { Issue } from '../models/Issue';
import type { IssueReaction } from '../models/IssueReaction';
import type { PrOperateLog } from '../models/PrOperateLog';
import type { PullRequest } from '../models/PullRequest';
import type { PullRequestAuth } from '../models/PullRequestAuth';
import type { PullRequestDetail } from '../models/PullRequestDetail';
import type { PullRequestFiles } from '../models/PullRequestFiles';
import type { PullRequestNote } from '../models/PullRequestNote';
import type { PullRequestNoteTree } from '../models/PullRequestNoteTree';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class PullRequestsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 创建 PR
   * 创建 PR
   * @returns PullRequestDetail 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdPullRequests({
    enterpriseId,
    projectId,
    sourceRepo,
    sourceBranch,
    targetBranch,
    title,
    body,
    assigneeId,
    prAssignNum,
    testerId,
    prTestNum,
    milestoneId,
    priority,
    pruneBranch,
    closeRelatedIssue,
    accessToken,
    qt,
    draft,
    squash,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 源仓库 id
     */
    sourceRepo: string,
    /**
     * 源分支名称
     */
    sourceBranch: string,
    /**
     * 目标分支名称
     */
    targetBranch: string,
    /**
     * 标题
     */
    title: string,
    /**
     * 内容
     */
    body: string,
    /**
     * 审查者。可多选，英文逗号分隔
     */
    assigneeId: string,
    /**
     * 审查人员数量
     */
    prAssignNum: string,
    /**
     * 测试人员。可多选，英文逗号分隔
     */
    testerId: string,
    /**
     * 测试人员数量
     */
    prTestNum: string,
    /**
     * 关联的里程碑
     */
    milestoneId: string,
    /**
     * 优先级 0~4
     */
    priority: string,
    /**
     * 是否需要在合并 PR 后删除提交分支。0: 否 1: 是
     */
    pruneBranch: 0 | 1,
    /**
     * 是否需要在合并 PR 后关闭关联的任务。0: 否 1: 是
     */
    closeRelatedIssue: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 是否指定为草稿：草稿 - true, 非草稿 - false，缺省时为非草稿
     */
    draft?: boolean,
    /**
     * 接受 Pull Request 时使用扁平化（squash）合并
     */
    squash?: boolean,
  }): CancelablePromise<Array<PullRequestDetail>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'source_repo': sourceRepo,
        'source_branch': sourceBranch,
        'target_branch': targetBranch,
        'title': title,
        'body': body,
        'assignee_id': assigneeId,
        'pr_assign_num': prAssignNum,
        'tester_id': testerId,
        'pr_test_num': prTestNum,
        'milestone_id': milestoneId,
        'priority': priority,
        'prune_branch': pruneBranch,
        'close_related_issue': closeRelatedIssue,
        'draft': draft,
        'squash': squash,
      },
    });
  }

  /**
   * 获取分支对比的 Commit 列表信息
   * 获取分支对比的 Commit 列表信息
   * @returns CommitList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsNewCommits({
    enterpriseId,
    projectId,
    sourceNamespace,
    sourceBranch,
    targetNamespace,
    targetBranch,
    accessToken,
    qt,
    page = 1,
    perPage,
    prevId,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 源仓库所属空间地址
     */
    sourceNamespace: string,
    /**
     * 源仓库的分支名称
     */
    sourceBranch: string,
    /**
     * 目标仓库所属空间地址
     */
    targetNamespace: string,
    /**
     * 目标仓库的分支名称
     */
    targetBranch: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码 (已弃用)
     */
    page?: number,
    /**
     * 每页的数量，最大为 100(已弃用)
     */
    perPage?: number,
    /**
     * 滚动列表的最后一条记录的 id
     */
    prevId?: string,
  }): CancelablePromise<Array<CommitList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/new/commits',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'source_namespace': sourceNamespace,
        'source_branch': sourceBranch,
        'target_namespace': targetNamespace,
        'target_branch': targetBranch,
        'page': page,
        'per_page': perPage,
        'prev_id': prevId,
      },
    });
  }

  /**
   * 获取分支对比的文件改动列表
   * 获取分支对比的文件改动列表
   * @returns CommitList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsNewFiles({
    enterpriseId,
    projectId,
    sourceNamespace,
    sourceBranch,
    targetNamespace,
    targetBranch,
    accessToken,
    qt,
    w,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 源仓库所属空间地址
     */
    sourceNamespace: string,
    /**
     * 源仓��的分支名称
     */
    sourceBranch: string,
    /**
     * 目标仓库所属空间地址
     */
    targetNamespace: string,
    /**
     * 目标仓库的分支名称
     */
    targetBranch: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 是否隐藏空白和换行，1：隐藏，0：不隐藏，默认不隐藏
     */
    w?: number,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<CommitList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/new/files',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'source_namespace': sourceNamespace,
        'source_branch': sourceBranch,
        'target_namespace': targetNamespace,
        'target_branch': targetBranch,
        'w': w,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 创建 Cherry Pick
   * 创建 Cherry Pick
   * @returns CherryPick 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdCherryPick({
    enterpriseId,
    projectId,
    targetBranchName,
    targetProjectId,
    pullRequestId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 目标分支名称
     */
    targetBranchName: string,
    /**
     * 目标仓库 id
     */
    targetProjectId: number,
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<CherryPick> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/cherry_pick',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'target_branch_name': targetBranchName,
        'target_project_id': targetProjectId,
      },
    });
  }

  /**
   * 获取 Pull Request 详情
   * 获取 Pull Request 详情
   * @returns PullRequestDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestId({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<PullRequestDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 更新 Pull Request
   * 更新 Pull Request
   * @returns PullRequestDetail 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdPullRequestsPullRequestId({
    enterpriseId,
    projectId,
    pullRequestId,
    reviewersUserId,
    reviewersScore,
    accessToken,
    qt,
    prQt,
    milestoneId,
    relatedIssueId,
    title,
    body,
    targetBranch,
    priority,
    stateEvent,
    labelIds,
    closeRelatedIssue,
    pruneBranch,
    assigneeId,
    prAssignNum,
    testerId,
    prTestNum,
    checkState,
    testState,
    draft,
    squash,
    reviewScore,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户 ID
     */
    reviewersUserId: Array<number>,
    /**
     * 审查者代表的的分数 reviewer: 1, maintainer: 2
     */
    reviewersScore: 2 | 1 | 0,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * PR id 类型
     */
    prQt?: 'id' | 'iid',
    /**
     * 关联的里程碑 id
     */
    milestoneId?: number,
    /**
     * 关联的任务 id。如有多个，用英文逗号分隔。eg: 1,2,3
     */
    relatedIssueId?: string,
    /**
     * PR 标题
     */
    title?: string,
    /**
     * PR 正文内容
     */
    body?: string,
    /**
     * 目标仓库的分支名称
     */
    targetBranch?: string,
    /**
     * 优先级 (0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重)
     */
    priority?: number,
    /**
     * 关闭 PR
     */
    stateEvent?: 'close',
    /**
     * 关联的标签 id。如有多个，用英文逗号分隔。eg: 1,2,3
     */
    labelIds?: string,
    /**
     * 是否需要在合并 PR 后关闭关联的任务。0: 否 1: 是
     */
    closeRelatedIssue?: number,
    /**
     * 是否需要在合并 PR 后删除提交分支。0: 否 1: 是
     */
    pruneBranch?: 0 | 1,
    /**
     * 审查人员的用户 id。如有多个，用英文逗号分隔。eg: 1,2,3
     */
    assigneeId?: string,
    /**
     * 至少需要{pr_assign_num}名审查人员审查通过后可合并
     */
    prAssignNum?: number,
    /**
     * 测试人员的用户 id。如有多个，用英文逗号分隔。eg: 1,2,3
     */
    testerId?: string,
    /**
     * 至少需要{pr_assign_num}名测试人员测试通过后可合并
     */
    prTestNum?: number,
    /**
     * 审查状态。0: 非必须审查 1: 必须审查
     */
    checkState?: 0 | 1,
    /**
     * 测试状态。0: 非必须测试 1: 必须测试
     */
    testState?: 0 | 1,
    /**
     * 是否指定为草稿：草稿 - true, 非草稿 - false，缺省时为非草稿
     */
    draft?: boolean,
    /**
     * 接受 Pull Request 时使用扁平化（squash）合并
     */
    squash?: boolean,
    /**
     * 分数模式下最少通过分数
     */
    reviewScore?: number,
  }): CancelablePromise<PullRequestDetail> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'pr_qt': prQt,
        'milestone_id': milestoneId,
        'related_issue_id': relatedIssueId,
        'title': title,
        'body': body,
        'target_branch': targetBranch,
        'priority': priority,
        'state_event': stateEvent,
        'label_ids': labelIds,
        'close_related_issue': closeRelatedIssue,
        'prune_branch': pruneBranch,
        'assignee_id': assigneeId,
        'pr_assign_num': prAssignNum,
        'tester_id': testerId,
        'pr_test_num': prTestNum,
        'check_state': checkState,
        'test_state': testState,
        'draft': draft,
        'squash': squash,
        'reviewers[user_id]': reviewersUserId,
        'reviewers[score]': reviewersScore,
        'review_score': reviewScore,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 审查 PR
   * 审查 PR
   * @returns any 审查 PR
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdAssign({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    forceAccept = 0,
    action = 'accept',
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 是否强制审核通过 (默认否)
     */
    forceAccept?: 0 | 1,
    /**
     * 操作类型 (默认通过)
     */
    action?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/assign',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'force_accept': forceAccept,
        'action': action,
      },
    });
  }

  /**
   * PR 测试通过
   * PR 测试通过
   * @returns any PR 测试通过
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdTest({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    forceAccept = 0,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 是否强制测试通过 (默认否)
     */
    forceAccept?: 0 | 1,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/test',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'force_accept': forceAccept,
      },
    });
  }

  /**
   * PR 标记与取消草稿
   * PR 标记与取消草稿
   * @returns any PR 标记与取消草稿
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdDraft({
    enterpriseId,
    projectId,
    pullRequestId,
    draft,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 是否指定为草稿：指定草稿-true, 取消草稿-false
     */
    draft: boolean,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/draft',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'draft': draft,
      },
    });
  }

  /**
   * 合并 PR
   * 合并 PR
   * @returns any 合并 PR
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdMerge({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    mergeMethod = 'merge',
    title,
    description,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 可选。合并 PR 的方��，merge（合并所有提交）和 squash（扁平化分支合并）和 rebase（变基并合并）。默认为 merge。
     */
    mergeMethod?: 'merge' | 'squash' | 'rebase',
    /**
     * 可选。合并标题，默认为 PR 的标题
     */
    title?: string,
    /**
     * 可选。合并描述，默认为 "Merge pull request !{pr_id} from {author}/{source_branch}"，与页面显示的默认一致。
     */
    description?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/merge',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'merge_method': mergeMethod,
        'title': title,
        'description': description,
      },
    });
  }

  /**
   * 获取某 Pull Request 的所有 Commit 信息
   * 获取某 Pull Request 的所有 Commit 信息
   * @returns CommitList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdCommits({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    page = 1,
    perPage,
    prevId,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码 (已弃用)
     */
    page?: number,
    /**
     * 每页的数量，最大为 100(已弃用)
     */
    perPage?: number,
    /**
     * 滚动列表的最后一条记录的 id
     */
    prevId?: string,
  }): CancelablePromise<Array<CommitList>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/commits',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
        'prev_id': prevId,
      },
    });
  }

  /**
   * Pull Request Commit 文件列表。最多显示 200 条 diff
   * Pull Request Commit 文件列表。最多显示 200 条 diff
   * @returns PullRequestFiles 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdFiles({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    page = 1,
    perPage,
    headSha,
    baseSha,
    disablePaginate,
    w,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * head sha
     */
    headSha?: string,
    /**
     * base sha
     */
    baseSha?: string,
    /**
     * 是否禁用分页
     */
    disablePaginate?: boolean,
    /**
     * 是否隐藏空白和换行，1：隐藏，0：不隐藏，默认不隐藏
     */
    w?: number,
  }): CancelablePromise<Array<PullRequestFiles>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/files',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
        'head_sha': headSha,
        'base_sha': baseSha,
        'disable_paginate': disablePaginate,
        'w': w,
      },
    });
  }

  /**
   * 获取 Commit 下的 diffs
   * 获取 Commit 下的 diffs
   * @returns PullRequestFiles 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdCommitsCommitIdFiles({
    enterpriseId,
    projectId,
    pullRequestId,
    commitId,
    accessToken,
    qt,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * Commit id
     */
    commitId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<PullRequestFiles>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/commits/{commit_id}/files',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
        'commit_id': commitId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取 Pull Request 评论列表
   * 获取 Pull Request 评论列表
   * @returns PullRequestNote 获取 Pull Request 评论列表
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdNotes({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    commitId,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * commit sha
     */
    commitId?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<PullRequestNote> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/notes',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'commit_id': commitId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 评论 Pull Request
   * 评论 Pull Request
   * @returns PullRequestNote 评论 Pull Request
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdNotes({
    enterpriseId,
    projectId,
    pullRequestId,
    body,
    accessToken,
    qt,
    lineCode,
    diffPositionId,
    replyId,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 默认 PR id，当 pr_qt 参数为 iid 时，此处是仓库的 PR 编号
     */
    pullRequestId: number,
    /**
     * 评论内容，代码建议用```suggestion ``` 包围
     */
    body: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 代码行标记值
     */
    lineCode?: string,
    /**
     * 代码评论组 id，同位置已有评论时传递
     */
    diffPositionId?: number,
    /**
     * 回复的上级评论 id
     */
    replyId?: number,
  }): CancelablePromise<PullRequestNote> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/notes',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'body': body,
        'line_code': lineCode,
        'diff_position_id': diffPositionId,
        'reply_id': replyId,
      },
    });
  }

  /**
   * 获取 Pull Request 评论引用的代码片段
   * 获取 Pull Request 评论引用的代码片段
   * @returns DiffPosition 获取 Pull Request 评论引用的代码片段
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdDiffPositionContext({
    enterpriseId,
    projectId,
    ids,
    pullRequestId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * diff_position_id，使用英文逗号分隔
     */
    ids: string,
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Array<DiffPosition>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/diff_position_context',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'ids': ids,
      },
    });
  }

  /**
   * 获取 Pull Request 或相关提交的已解决未解决评论列表
   * 获取 Pull Request 或相关提交的已解决未解决评论列表
   * @returns PullRequestNote 获取 Pull Request 或相关提交的已解决未解决评论列表
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdNotesGroup({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    commitIds,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * commit sha 提交列表，多个用 [,] 连接
     */
    commitIds?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<PullRequestNote> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/notes/group',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'commit_ids': commitIds,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取 Pull Request 或相关提交的评论列表树
   * 获取 Pull Request 或相关提交的评论列表树
   * @returns PullRequestNoteTree 获取 Pull Request 或相关提交的评论列表树
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdNotesTrees({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    headSha,
    baseSha,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 默认 PR id，当 pr_qt 参数为 iid 时，此处是仓库的 PR 编号
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * head sha
     */
    headSha?: string,
    /**
     * base sha
     */
    baseSha?: string,
  }): CancelablePromise<PullRequestNoteTree> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/notes/trees',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'head_sha': headSha,
        'base_sha': baseSha,
      },
    });
  }

  /**
   * 获取 Pull Request 操作日志
   * 获取 Pull Request 操作日志
   * @returns PrOperateLog 获取 Pull Request 操作日志
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdOperateLogs({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 排序字段 (created_at: 创建时间 updated_at: 更新时间)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<PrOperateLog> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/operate_logs',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取授权用户对 PR 的权限
   * 获取授权用户对 PR 的权限
   * @returns PullRequestAuth 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdAuths({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<PullRequestAuth> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/auths',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 获取 PullRequest 中差异较大的文件内容
   * 获取 PullRequest 中差异较大的文件内容
   * @returns any 获取 PullRequest 中差异较大的文件内容
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdDiffForPath({
    enterpriseId,
    projectId,
    pullRequestId,
    fileIdentifier,
    newPath,
    oldPath,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * git 文件标识符
     */
    fileIdentifier: string,
    /**
     * 旧路径
     */
    newPath: string,
    /**
     * 新路径
     */
    oldPath: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/diff_for_path',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'file_identifier': fileIdentifier,
        'new_path': newPath,
        'old_path': oldPath,
      },
    });
  }

  /**
   * 获取 PullRequest 的 GiteeScan 报告
   * 获取 PullRequest 的 GiteeScan 报告
   * @returns any 获取 PullRequest 的 GiteeScan 报告
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdScanReports({
    enterpriseId,
    projectId,
    pullRequestId,
    taskId,
    type,
    accessToken,
    qt,
    bugLevel,
    bugType,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * GiteeScan 扫描任务 id
     */
    taskId: number,
    /**
     * 扫描类型，bug：缺陷扫描、style：规范扫描、cve：依赖项漏洞扫描
     */
    type: 'bug' | 'style' | 'cve',
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 缺陷等级，高危：1、中危：2
     */
    bugLevel?: string,
    /**
     * 缺陷类型，BUG：0、CodeSmell：1、Security：2
     */
    bugType?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/scan_reports',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'task_id': taskId,
        'type': type,
        'bug_level': bugLevel,
        'bug_type': bugType,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 应用代码建议到 PR
   * 应用代码建议到 PR
   * @returns any 应用代码建议到 PR
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdApplySuggestions({
    enterpriseId,
    projectId,
    suggestionsNoteId,
    suggestionsNoteTimestamp,
    suggestionsDiffPositionTimestamp,
    pullRequestId,
    accessToken,
    qt,
    message,
    suggestionsContent,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * Note 的 id
     */
    suggestionsNoteId: Array<number>,
    /**
     * Note 的 updated_at 的秒时间戳
     */
    suggestionsNoteTimestamp: Array<number>,
    /**
     * Diff_position 的 updated_at 的秒时间戳
     */
    suggestionsDiffPositionTimestamp: Array<number>,
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 应用建议操作的提交信息
     */
    message?: string,
    /**
     * 代码建议的内容（如果没有，则视为删除）
     */
    suggestionsContent?: Array<string>,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/apply_suggestions',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'message': message,
        'suggestions[note_id]': suggestionsNoteId,
        'suggestions[note_timestamp]': suggestionsNoteTimestamp,
        'suggestions[diff_position_timestamp]': suggestionsDiffPositionTimestamp,
        'suggestions[content]': suggestionsContent,
      },
    });
  }

  /**
   * 新增 PR 表态
   * 新增 PR 表态
   * @returns IssueReaction 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdReactions({
    enterpriseId,
    projectId,
    pullRequestId,
    text,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 表情
     */
    text: '+1' | '-1' | 'smile' | 'tada' | 'confused' | 'heart' | 'rocket' | 'eyes',
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<IssueReaction> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/reactions',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'text': text,
      },
    });
  }

  /**
   * 取消 PR 表态
   * 取消 PR 表态
   * @returns IssueReaction 返回格式
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdReactionsCurrentUserReactionId({
    enterpriseId,
    projectId,
    pullRequestId,
    currentUserReactionId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * PR id
     */
    pullRequestId: number,
    /**
     * 表态 id
     */
    currentUserReactionId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<IssueReaction> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/reactions/{current_user_reaction_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
        'current_user_reaction_id': currentUserReactionId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * PR 关联任务任务列表
   * PR 关联任务任务列表
   * @returns Issue PR 关联任务任务列表
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdLinkIssues({
    enterpriseId,
    projectId,
    pullRequestId,
    accessToken,
    qt,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Issue> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/link_issues',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * PR 添加 关联任务
   * PR 添加 关联任务
   * @returns Issue PR 添加 关联任务
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdLinkIssues({
    enterpriseId,
    projectId,
    linkIssueId,
    pullRequestId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 任务 id
     */
    linkIssueId: number,
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Issue> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/link_issues',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'pull_request_id': pullRequestId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'link_issue_id': linkIssueId,
      },
    });
  }

  /**
   * PR 取消 关联任务
   * PR 取消 关联任务
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdLinkIssuesLinkIssueId({
    enterpriseId,
    projectId,
    linkIssueId,
    pullRequestId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 任务 id
     */
    linkIssueId: number,
    pullRequestId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/link_issues/{link_issue_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'link_issue_id': linkIssueId,
        'pull_request_id': pullRequestId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 获取企业下的 Pull Request 列表
   * 获取企业下的 Pull Request 列表
   * @returns PullRequest 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdPullRequests({
    enterpriseId,
    accessToken,
    state,
    scope,
    authorId,
    assigneeId,
    testerId,
    search,
    sort,
    direction,
    groupId,
    milestoneId,
    labels,
    labelIds,
    canBeMerged,
    projectId,
    needStateCount,
    publicOrInternalOpenOnly,
    targetBranch,
    sourceBranch,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * PR 状态
     */
    state?: 'opened' | 'closed' | 'merged',
    /**
     * 范围筛选。指派我的：assigned_or_test，我创建或指派给我的：related_to_me，
     * 我参与仓库的 PR: participate_in，草稿 PR: draft，create：我发起的，assign：我评审的，test：我测试的
     */
    scope?: 'assigned_or_test' | 'related_to_me' | 'participate_in' | 'draft' | 'create' | 'assign' | 'test',
    /**
     * 筛选 PR 创建者
     */
    authorId?: string,
    /**
     * 筛选 PR 审查者
     */
    assigneeId?: string,
    /**
     * 筛选 PR 测试人员
     */
    testerId?: string,
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 排序字段 (created_at、closed_at、priority、updated_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 团队 id
     */
    groupId?: number,
    /**
     * 里程碑 id
     */
    milestoneId?: number,
    /**
     * 标签名称。多个标签逗号 (,) 隔开
     */
    labels?: string,
    /**
     * 标签 ID，多个标签逗号 (,) 隔开
     */
    labelIds?: string,
    /**
     * 是否可合并
     */
    canBeMerged?: 0 | 1,
    /**
     * 仓库 id
     */
    projectId?: number,
    /**
     * 是否需要状态统计数
     */
    needStateCount?: 0 | 1,
    /**
     * 仅列出内部公开和外部公开的 PR
     */
    publicOrInternalOpenOnly?: 0 | 1,
    /**
     * 目标分支名
     */
    targetBranch?: string,
    /**
     * 源分支名
     */
    sourceBranch?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<PullRequest>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/pull_requests',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'state': state,
        'scope': scope,
        'author_id': authorId,
        'assignee_id': assigneeId,
        'tester_id': testerId,
        'search': search,
        'sort': sort,
        'direction': direction,
        'group_id': groupId,
        'milestone_id': milestoneId,
        'labels': labels,
        'label_ids': labelIds,
        'can_be_merged': canBeMerged,
        'project_id': projectId,
        'need_state_count': needStateCount,
        'public_or_internal_open_only': publicOrInternalOpenOnly,
        'target_branch': targetBranch,
        'source_branch': sourceBranch,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取企业下用户 Pull Request 相关数量数据
   * 获取企业下用户 Pull Request 相关数量数据
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdPullRequestsPullRequestStats({
    enterpriseId,
    accessToken,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/pull_requests/pull_request_stats',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
