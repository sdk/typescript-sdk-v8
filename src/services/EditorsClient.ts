/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class EditorsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * markdown 转 html
   * markdown 转 html
   * @returns any 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdEditorsMd2Html({
    content,
    enterpriseId,
  }: {
    /**
     * markdown 内容
     */
    content: string,
    enterpriseId: number,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/editors/md2html',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'content': content,
      },
    });
  }

}
