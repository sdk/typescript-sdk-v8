/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EnterpriseProgramsList } from '../models/EnterpriseProgramsList';
import type { EnterpriseProjectsList } from '../models/EnterpriseProjectsList';
import type { OsProjectsList } from '../models/OsProjectsList';
import type { UserStatistic } from '../models/UserStatistic';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class StatisticsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取成员的统计信息
   * 获取成员的统计信息
   * @returns UserStatistic 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdStatisticsUserStatistic({
    enterpriseId,
    userId,
    startDate,
    endDate,
    accessToken,
    projectId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 查找指定用户的数据。（可多选，用逗号分割）
     */
    userId: string,
    /**
     * 统计的起始时间。(格式：yyyy-mm-dd)
     */
    startDate: string,
    /**
     * 统计的结束时间。(格式：yyyy-mm-dd)
     */
    endDate: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 仓库 id
     */
    projectId?: number,
  }): CancelablePromise<Array<UserStatistic>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/statistics/user_statistic',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'user_id': userId,
        'project_id': projectId,
        'start_date': startDate,
        'end_date': endDate,
      },
    });
  }

  /**
   * 获取企业项目的统计信息
   * 获取企业项目的统计信息
   * @returns EnterpriseProgramsList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdStatisticsEnterpriseProgramList({
    enterpriseId,
    accessToken,
    programId,
    startDate,
    endDate,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
  }): CancelablePromise<EnterpriseProgramsList> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/statistics/enterprise_program_list',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
        'start_date': startDate,
        'end_date': endDate,
      },
    });
  }

  /**
   * 获取企业仓库的统计信息
   * 获取企业仓库的统计信息
   * @returns EnterpriseProjectsList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdStatisticsEnterpriseProjectList({
    enterpriseId,
    accessToken,
    projectId,
    startDate,
    endDate,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 仓库 id
     */
    projectId?: number,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
  }): CancelablePromise<EnterpriseProjectsList> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/statistics/enterprise_project_list',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'project_id': projectId,
        'start_date': startDate,
        'end_date': endDate,
      },
    });
  }

  /**
   * 获取企业开源仓库详情列表
   * 获取企业开源仓库详情列表
   * @returns OsProjectsList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdStatisticsEnterpriseOsProjectList({
    enterpriseId,
    accessToken,
    projectId,
    startDate,
    endDate,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 仓库 id
     */
    projectId?: number,
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
  }): CancelablePromise<OsProjectsList> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/statistics/enterprise_os_project_list',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'project_id': projectId,
        'start_date': startDate,
        'end_date': endDate,
      },
    });
  }

  /**
   * 获取当前用户的统计信息
   * 获取当前用户的统计信息
   * @returns any 获取当前用户的统计信息
   * @throws ApiError
   */
  public getEnterpriseIdDashboardStatisticsUserDashboard({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/dashboard_statistics/user_dashboard',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取企业概览数据
   * 获取企业概览数据
   * @returns any 获取企业概览数据
   * @throws ApiError
   */
  public getEnterpriseIdDashboardStatisticsOverview({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/dashboard_statistics/overview',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
