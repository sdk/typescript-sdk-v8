/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EnvVariable } from '../models/EnvVariable';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ProjectEnvClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 环境变量列表
   * 环境变量列表
   * @returns EnvVariable 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdEnvVariables({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    query,
    readOnly,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 搜索字符串
     */
    query?: string,
    /**
     * 1 为只读，其他情况为全部
     */
    readOnly?: number,
    /**
     * 排序字段 (created_at: 创建时间 updated_at: 更新时间 name: 名称)
     */
    sort?: 'created_at' | 'updated_at' | 'name',
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: 'asc' | 'desc',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<EnvVariable> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/env_variables',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'query': query,
        'read_only': readOnly,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新建环境变量
   * 新建环境变量
   * @returns EnvVariable 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdEnvVariables({
    enterpriseId,
    projectId,
    name,
    value,
    accessToken,
    qt,
    readOnly,
    remark,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 变量名称
     */
    name: string,
    /**
     * 变量值
     */
    value: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 是否只读，1 为 true,0 为 false
     */
    readOnly?: number,
    /**
     * 备注
     */
    remark?: string,
  }): CancelablePromise<EnvVariable> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/env_variables',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'name': name,
        'value': value,
        'read_only': readOnly,
        'remark': remark,
      },
    });
  }

  /**
   * 修改环境变量
   * 修改环境变量
   * @returns EnvVariable 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdEnvVariablesEnvVariableId({
    enterpriseId,
    projectId,
    envVariableId,
    accessToken,
    qt,
    name,
    value,
    readOnly,
    remark,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 变量 id
     */
    envVariableId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 变量名称
     */
    name?: string,
    /**
     * 变量值
     */
    value?: string,
    /**
     * 是否只读，1 为 true,0 为 false
     */
    readOnly?: number,
    /**
     * 备注
     */
    remark?: string,
  }): CancelablePromise<EnvVariable> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/env_variables/{env_variable_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'env_variable_id': envVariableId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'name': name,
        'value': value,
        'read_only': readOnly,
        'remark': remark,
      },
    });
  }

  /**
   * 删除环境变量
   * 删除环境变量
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdEnvVariablesEnvVariableId({
    enterpriseId,
    projectId,
    envVariableId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 变量 id
     */
    envVariableId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/env_variables/{env_variable_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'env_variable_id': envVariableId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

}
