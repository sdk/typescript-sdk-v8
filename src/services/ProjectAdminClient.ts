/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EnterpriseDeployKey } from '../models/EnterpriseDeployKey';
import type { PrAssigner } from '../models/PrAssigner';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ProjectAdminClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 仓库归档 (状态管理)
   * 仓库归档 (状态管理)
   * @returns any 仓库归档 (状态管理)
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdStatus({
    enterpriseId,
    projectId,
    status,
    accessToken,
    qt,
    password,
    validateType,
    smsCaptcha,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 仓库状态，0：开始，1：暂停，2：关闭
     */
    status: 0 | 1 | 2,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 用户密码
     */
    password?: string,
    /**
     * 验证方式
     */
    validateType?: string,
    /**
     * 短信验证码
     */
    smsCaptcha?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/status',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'status': status,
        'password': password,
        'validate_type': validateType,
        'sms_captcha': smsCaptcha,
      },
    });
  }

  /**
   * 初始化 README
   * 初始化 README
   * @returns any 初始化 README
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdInitialiazeReadme({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/initialiaze_readme',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 查看代码审查设置
   * 查看代码审查设置
   * @returns PrAssigner 查看代码审查设置
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPrAssigner({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<PrAssigner> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pr_assigner',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 指派审查、测试人员
   * 指派审查、测试人员
   * @returns PrAssigner 指派审查、测试人员
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdPrAssigner({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    assignerIds,
    prAssignNum,
    testerIds,
    prTestNum,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 审查人员 id，英文逗号分隔
     */
    assignerIds?: string,
    /**
     * 可合并的审查人员门槛数
     */
    prAssignNum?: number,
    /**
     * 测试人员 id，英文逗号分隔
     */
    testerIds?: string,
    /**
     * 可合并的测试人员门槛数
     */
    prTestNum?: number,
  }): CancelablePromise<PrAssigner> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/pr_assigner',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'assigner_ids': assignerIds,
        'pr_assign_num': prAssignNum,
        'tester_ids': testerIds,
        'pr_test_num': prTestNum,
      },
    });
  }

  /**
   * 添加部署公钥
   * 添加部署公钥
   * @returns EnterpriseDeployKey 添加部署公钥
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdDeployKeys({
    enterpriseId,
    projectId,
    title,
    key,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 标题
     */
    title: string,
    /**
     * 公钥
     */
    key: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<EnterpriseDeployKey> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/deploy_keys',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'title': title,
        'key': key,
      },
    });
  }

  /**
   * 已启用公钥
   * 已启用公钥
   * @returns EnterpriseDeployKey 已启用公钥
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdDeployKeys({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<EnterpriseDeployKey> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/deploy_keys',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 可部署公钥
   * 可部署公钥
   * @returns EnterpriseDeployKey 可部署公钥
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdAvailabelKeys({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<EnterpriseDeployKey> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/availabel_keys',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 公钥详情
   * 公钥详情
   * @returns EnterpriseDeployKey 公钥详情
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdDeployKeysDeployKeyId({
    enterpriseId,
    projectId,
    deployKeyId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 公钥 id
     */
    deployKeyId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<EnterpriseDeployKey> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/deploy_keys/{deploy_key_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'deploy_key_id': deployKeyId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 启用/停用公钥
   * 启用/停用公钥
   * @returns any 启用/停用公钥
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdDeployKeysDeployKeyId({
    enterpriseId,
    projectId,
    deployKeyId,
    enabled,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 公钥 id
     */
    deployKeyId: number,
    /**
     * 0:停用，1：启用
     */
    enabled: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/deploy_keys/{deploy_key_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'deploy_key_id': deployKeyId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'enabled': enabled,
      },
    });
  }

}
