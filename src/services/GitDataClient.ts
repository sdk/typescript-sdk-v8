/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Event } from '../models/Event';
import type { Issue } from '../models/Issue';
import type { Program } from '../models/Program';
import type { Project } from '../models/Project';
import type { ProjectBaseSetting } from '../models/ProjectBaseSetting';
import type { ProjectContributor } from '../models/ProjectContributor';
import type { ProjectDetail } from '../models/ProjectDetail';
import type { ProjectOverview } from '../models/ProjectOverview';
import type { ProjectSummary } from '../models/ProjectSummary';
import type { PullRequest } from '../models/PullRequest';
import type { Release } from '../models/Release';
import type { ResultResponse } from '../models/ResultResponse';
import type { UserProjectList } from '../models/UserProjectList';
import type { UsersProjects } from '../models/UsersProjects';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class GitDataClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取授权用户参与的仓库列表
   * 获取授权用户参与的仓库列表
   * @returns ProjectDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjects({
    enterpriseId,
    accessToken,
    scope,
    search,
    type,
    status,
    creatorId,
    parentId,
    forkFilter,
    outsourced,
    groupId,
    sort,
    direction,
    namespaceScope,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 范围筛选
     */
    scope?: 'private' | 'public' | 'internal-open' | 'not-belong-any-program' | 'all',
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库，personal_namespace: 企业下个人空间地址下的仓库
     */
    type?: 'joined' | 'created' | 'star' | 'template' | 'personal_namespace',
    /**
     * 状态：0: 开始，1: 暂停，2: 关闭
     */
    status?: 0 | 1 | 2,
    /**
     * 负责人
     */
    creatorId?: number,
    /**
     * form_from 仓库 id
     */
    parentId?: number,
    /**
     * 非 fork 的 (not_fork), 只看 fork 的 (only_fork)
     */
    forkFilter?: 'not_fork' | 'only_fork',
    /**
     * 是否外包：0：内部，1：外包
     */
    outsourced?: 0 | 1,
    /**
     * 团队 id
     */
    groupId?: number,
    /**
     * 排序字段 (created_at、last_push_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 归属搜索的 scope，配合 group_id 使用，belongs_to: 搜索归属包含下级，only_this: 搜索归属仅包含当前层级
     */
    namespaceScope?: 'belongs_to' | 'only_this',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<ProjectDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'scope': scope,
        'search': search,
        'type': type,
        'status': status,
        'creator_id': creatorId,
        'parent_id': parentId,
        'fork_filter': forkFilter,
        'outsourced': outsourced,
        'group_id': groupId,
        'sort': sort,
        'direction': direction,
        'namespace_scope': namespaceScope,
        'page': page,
        'per_page': perPage,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 新建仓库
   * 新建仓库
   * @returns ProjectDetail 新建仓库
   * @throws ApiError
   */
  public postEnterpriseIdProjects({
    projectName,
    projectNamespacePath,
    projectPath,
    enterpriseId,
    accessToken,
    projectTemplateId,
    projectDescription,
    projectPublic,
    projectOutsourced,
    projectProgramIds,
    projectMemberIds,
    projectGroupIds,
    projectImportUrl,
    normalRefs,
    importProgramUsers,
    readme,
    issueTemplate,
    pullRequestTemplate,
    userSyncCode,
    passwordSyncCode,
    language,
    ignore = 'no',
    license = 'no',
    model,
    customBranchesProd,
    customBranchesDev,
    customBranchesFeat,
    customBranchesRel,
    customBranchesBugfix,
    customBranchesTag,
  }: {
    /**
     * 仓库名称
     */
    projectName: string,
    /**
     * 归属路径
     */
    projectNamespacePath: string,
    /**
     * 仓库路径
     */
    projectPath: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 模板仓库 id
     */
    projectTemplateId?: number,
    /**
     * 仓库介绍
     */
    projectDescription?: string,
    /**
     * 是否开源
     */
    projectPublic?: 0 | 1 | 2,
    /**
     * 类型，0：内部，1：外包
     */
    projectOutsourced?: 1 | 0,
    /**
     * 关联项目列表，英文逗号分隔
     */
    projectProgramIds?: string,
    /**
     * 关联成员列表，英文逗号分隔
     */
    projectMemberIds?: string,
    /**
     * 授权团队列表，英文逗号分隔
     */
    projectGroupIds?: string,
    /**
     * 导入已有仓库路径
     */
    projectImportUrl?: string,
    /**
     * 是否包含所有分支
     */
    normalRefs?: boolean,
    /**
     * 是否导入项目成员：0:否，1:是
     */
    importProgramUsers?: 0 | 1,
    /**
     * 是否初始化 readme: 0:否，1:是
     */
    readme?: 0 | 1,
    /**
     * 是否初始化 issue 模版：0:否，1:是
     */
    issueTemplate?: 0 | 1,
    /**
     * 是否初始化 PR 模版：0:否，1:是
     */
    pullRequestTemplate?: 0 | 1,
    /**
     * 仓库导入 - 账号
     */
    userSyncCode?: string,
    /**
     * 仓库导入 - 密码
     */
    passwordSyncCode?: string,
    /**
     * 语言 id
     */
    language?: number,
    /**
     * .gitignore
     */
    ignore?: string,
    /**
     * 开源许可证
     */
    license?: string,
    /**
     * 分支模型 id
     */
    model?: '1' | '2' | '3' | '4' | '5' | '6',
    /**
     * Production environment branch
     */
    customBranchesProd?: string,
    /**
     * Development branch
     */
    customBranchesDev?: string,
    /**
     * Function branch
     */
    customBranchesFeat?: string,
    /**
     * Release branch
     */
    customBranchesRel?: string,
    /**
     * Patch branch
     */
    customBranchesBugfix?: string,
    /**
     * Version tag branch
     */
    customBranchesTag?: string,
  }): CancelablePromise<ProjectDetail> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'project[name]': projectName,
        'project[namespace_path]': projectNamespacePath,
        'project[path]': projectPath,
        'project[template_id]': projectTemplateId,
        'project[description]': projectDescription,
        'project[public]': projectPublic,
        'project[outsourced]': projectOutsourced,
        'project[program_ids]': projectProgramIds,
        'project[member_ids]': projectMemberIds,
        'project[group_ids]': projectGroupIds,
        'project[import_url]': projectImportUrl,
        'normal_refs': normalRefs,
        'import_program_users': importProgramUsers,
        'readme': readme,
        'issue_template': issueTemplate,
        'pull_request_template': pullRequestTemplate,
        'user_sync_code': userSyncCode,
        'password_sync_code': passwordSyncCode,
        'language': language,
        'ignore': ignore,
        'license': license,
        'model': model,
        'custom_branches[prod]': customBranchesProd,
        'custom_branches[dev]': customBranchesDev,
        'custom_branches[feat]': customBranchesFeat,
        'custom_branches[rel]': customBranchesRel,
        'custom_branches[bugfix]': customBranchesBugfix,
        'custom_branches[tag]': customBranchesTag,
      },
    });
  }

  /**
   * 获取授权用户参与的仓库列表 (按层级获取)
   * 获取授权用户参与的仓库列表 (按层级获取)
   * @returns UserProjectList 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsAsTree({
    enterpriseId,
    accessToken,
    scope,
    search,
    type,
    status,
    creatorId,
    parentId,
    forkFilter,
    outsourced,
    groupId,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 范围筛选
     */
    scope?: 'private' | 'public' | 'internal-open' | 'not-belong-any-program' | 'all',
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库，personal_namespace: 企业下个人空间地址下的仓库
     */
    type?: 'joined' | 'created' | 'star' | 'template' | 'personal_namespace',
    /**
     * 状态：0: 开始，1: 暂停，2: 关闭
     */
    status?: 0 | 1 | 2,
    /**
     * 负责人
     */
    creatorId?: number,
    /**
     * form_from 仓库 id
     */
    parentId?: number,
    /**
     * 非 fork 的 (not_fork), 只看 fork 的 (only_fork)
     */
    forkFilter?: 'not_fork' | 'only_fork',
    /**
     * 是否外包：0：内部，1：外包
     */
    outsourced?: 0 | 1,
    /**
     * 团队 id
     */
    groupId?: number,
    /**
     * 排序字段 (created_at、last_push_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<UserProjectList> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/as_tree',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'scope': scope,
        'search': search,
        'type': type,
        'status': status,
        'creator_id': creatorId,
        'parent_id': parentId,
        'fork_filter': forkFilter,
        'outsourced': outsourced,
        'group_id': groupId,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 获取当前用户有权限提 pr 的仓库
   * 获取当前用户有权限提 pr 的仓库
   * @returns ProjectDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsForPullRequest({
    enterpriseId,
    accessToken,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 搜索字符串
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<ProjectDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/for_pull_request',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 获取仓库转入 - 转移码
   * 获取仓库转入 - 转移码
   * @returns any 获取仓库转入 - 转移码
   * @throws ApiError
   */
  public postEnterpriseIdProjectsTransferCode({
    enterpriseId,
    accessToken,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/transfer_code',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 新建仓库 - 仓库名/路径是否已经存在
   * 新建仓库 - 仓库名/路径是否已经存在
   * @returns any 新建仓库 - 仓库名/路径是否已经存在
   * @throws ApiError
   */
  public postEnterpriseIdProjectsCheckProjectName({
    namespacePath,
    enterpriseId,
    accessToken,
    name,
    path,
  }: {
    /**
     * 归属路径
     */
    namespacePath: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 名称。二选一检验某一项
     */
    name?: string,
    /**
     * 路径。二选一检验某一项
     */
    path?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/check_project_name',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'path': path,
        'namespace_path': namespacePath,
      },
    });
  }

  /**
   * 新建仓库 - 导入仓库参数是否有效
   * 新建仓库 - 导入仓库参数是否有效
   * @returns ResultResponse 新建仓库 - 导入仓库参数是否有效
   * @throws ApiError
   */
  public postEnterpriseIdProjectsCheckProjectCanImport({
    importUrl,
    enterpriseId,
    accessToken,
    userSyncCode,
    passwordSyncCode,
  }: {
    /**
     * 待导入仓库克隆路径
     */
    importUrl: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 仓库导入 - 账号（私有仓库需要）
     */
    userSyncCode?: string,
    /**
     * 仓库导入 - 密码（私有仓库需要）
     */
    passwordSyncCode?: string,
  }): CancelablePromise<ResultResponse> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/check_project_can_import',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'import_url': importUrl,
        'user_sync_code': userSyncCode,
        'password_sync_code': passwordSyncCode,
      },
    });
  }

  /**
   * 查看发行版列表
   * 查看发行版列表
   * @returns Release 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdReleases({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Release> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/releases',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新建发行版
   * 新建发行版
   * @returns Release 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdReleases({
    enterpriseId,
    projectId,
    releaseTagVersion,
    releaseTitle,
    accessToken,
    qt,
    releaseRef = 'master',
    releaseDescription,
    releaseReleaseType,
    attachIds,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 发行版版本
     */
    releaseTagVersion: string,
    /**
     * 发行版标题
     */
    releaseTitle: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 发行版所属分支
     */
    releaseRef?: string,
    /**
     * 发行版描述
     */
    releaseDescription?: string,
    /**
     * 发行版类型，0：发行版、1：预发行版
     */
    releaseReleaseType?: string,
    /**
     * 附件 id 列表，英文逗号分隔
     */
    attachIds?: string,
  }): CancelablePromise<Release> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/releases',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'release[tag_version]': releaseTagVersion,
        'release[title]': releaseTitle,
        'release[ref]': releaseRef,
        'release[description]': releaseDescription,
        'release[release_type]': releaseReleaseType,
        'attach_ids': attachIds,
      },
    });
  }

  /**
   * 查看发行版详情
   * 查看发行版详情
   * @returns Release 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdReleasesTagVersion({
    enterpriseId,
    projectId,
    tagVersion,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 发行版版本
     */
    tagVersion: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Release> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/releases/{tag_version}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'tag_version': tagVersion,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 编辑发行版
   * 编辑发行版
   * @returns Release 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdReleasesReleaseId({
    enterpriseId,
    projectId,
    releaseId,
    releaseTagVersion,
    releaseTitle,
    accessToken,
    qt,
    releaseRef = 'master',
    releaseDescription,
    releaseReleaseType,
    attachIds,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 发行版 id
     */
    releaseId: number,
    /**
     * 发行版版本
     */
    releaseTagVersion: string,
    /**
     * 发行版标题
     */
    releaseTitle: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 发行版所属分支
     */
    releaseRef?: string,
    /**
     * 发行版描述
     */
    releaseDescription?: string,
    /**
     * 发行版类型，0：发行版、1：预发行版
     */
    releaseReleaseType?: string,
    /**
     * 附件 id 列表，英文逗号分隔
     */
    attachIds?: string,
  }): CancelablePromise<Release> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/releases/{release_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'release_id': releaseId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'release[tag_version]': releaseTagVersion,
        'release[title]': releaseTitle,
        'release[ref]': releaseRef,
        'release[description]': releaseDescription,
        'release[release_type]': releaseReleaseType,
        'attach_ids': attachIds,
      },
    });
  }

  /**
   * 删除发行版
   * 删除发行版
   * @returns Release 返回格式
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdReleasesReleaseId({
    enterpriseId,
    projectId,
    releaseId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 发行版 id
     */
    releaseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Release> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/releases/{release_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'release_id': releaseId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 检查发行版是否存在
   * 检查发行版是否存在
   * @returns any 检查发行版是否存在
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCheckReleases({
    enterpriseId,
    projectId,
    tagVersion,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 发行版本
     */
    tagVersion: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/check_releases',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'tag_version': tagVersion,
      },
    });
  }

  /**
   * 获取可创建 Pull Request 的仓库
   * 获取可创建 Pull Request 的仓库
   * @returns Project 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdCanPull({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Project> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/can_pull',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 获取仓库的 Pull Request 列表
   * 获取仓库的 Pull Request 列表
   * @returns PullRequest 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPullRequests({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    state,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * PR 状态
     */
    state?: 'opened' | 'closed' | 'merged',
    /**
     * 排序字段 (created_at、updated_at)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<PullRequest> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/pull_requests',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'state': state,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 获取仓库的概览数据
   * 获取仓库的概览数据
   * @returns ProjectSummary 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdSummary({
    enterpriseId,
    projectId,
    ref,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 分支/标签名
     */
    ref: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<ProjectSummary> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/summary',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'ref': ref,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 获取仓库的成员列表
   * 获取仓库的成员列表
   * @returns UsersProjects 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdUsers({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 搜索字符串
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<UsersProjects> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/users',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 获取仓库的操作权限
   * 获取仓库的操作权限
   * @returns UsersProjects 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdOperateAuths({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<UsersProjects> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/operate_auths',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 获取仓库的关联项目列表
   * 获取仓库的关联项目列表
   * @returns Program 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdPrograms({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    scope,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * not_joined
     */
    scope?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Program> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/programs',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'scope': scope,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取仓库的任务列���
   * 获取仓库的任务列表
   * @returns Issue 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdIssues({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    programId,
    milestoneId,
    state,
    onlyRelatedMe,
    assigneeId,
    authorId,
    collaboratorIds,
    createdAt,
    finishedAt,
    planStartedAt,
    deadline,
    search,
    filterChild,
    issueStateIds,
    issueTypeId,
    labelIds,
    priority,
    scrumSprintIds,
    scrumVersionIds,
    kanbanIds,
    kanbanColumnIds,
    sort,
    direction,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 项目 id（可多选，用英文逗号分隔）
     */
    programId?: string,
    /**
     * 里程碑 id（可多选，用英文逗号分隔）
     */
    milestoneId?: string,
    /**
     * 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中：progressing）
     */
    state?: string,
    /**
     * 是否仅列出与授权用户相关的任务（0: 否 1: 是）
     */
    onlyRelatedMe?: string,
    /**
     * 负责人 id
     */
    assigneeId?: string,
    /**
     * 创建者 id
     */
    authorId?: string,
    /**
     * 协作者。(,分隔的 id 字符串)
     */
    collaboratorIds?: string,
    /**
     * 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）<yyyymmddTHH:MM:SS+08:00，（大于指定日期）>yyyymmddTHH:MM:SS+08:00
     */
    createdAt?: string,
    /**
     * 任务完成日期，格式同上
     */
    finishedAt?: string,
    /**
     * 计划开始时间，(格式：yyyy-mm-dd)
     */
    planStartedAt?: string,
    /**
     * 任务截止日期，(格式：yyyy-mm-dd)
     */
    deadline?: string,
    /**
     * 搜索参数
     */
    search?: string,
    /**
     * 是否过滤子任务 (0: 否，1: 是)
     */
    filterChild?: string,
    /**
     * 任务状态 id，多选，用英文逗号分隔。
     */
    issueStateIds?: string,
    /**
     * 任务类型
     */
    issueTypeId?: string,
    /**
     * 标签 id（可多选，用英文逗号分隔）
     */
    labelIds?: string,
    /**
     * 优先级（可多选，用英文逗号分隔）
     */
    priority?: string,
    /**
     * 迭代 id(可多选，用英文逗号分隔)
     */
    scrumSprintIds?: string,
    /**
     * 版本 id(可多选，用英文逗号分隔)
     */
    scrumVersionIds?: string,
    /**
     * 看板 id(可多选，用英文逗号分隔)
     */
    kanbanIds?: string,
    /**
     * 看板栏 id(可多选，用英文逗号分隔)
     */
    kanbanColumnIds?: string,
    /**
     * 排序字段 (created_at、updated_at、deadline、priority)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<Issue>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/issues',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'program_id': programId,
        'milestone_id': milestoneId,
        'state': state,
        'only_related_me': onlyRelatedMe,
        'assignee_id': assigneeId,
        'author_id': authorId,
        'collaborator_ids': collaboratorIds,
        'created_at': createdAt,
        'finished_at': finishedAt,
        'plan_started_at': planStartedAt,
        'deadline': deadline,
        'search': search,
        'filter_child': filterChild,
        'issue_state_ids': issueStateIds,
        'issue_type_id': issueTypeId,
        'label_ids': labelIds,
        'priority': priority,
        'scrum_sprint_ids': scrumSprintIds,
        'scrum_version_ids': scrumVersionIds,
        'kanban_ids': kanbanIds,
        'kanban_column_ids': kanbanColumnIds,
        'sort': sort,
        'direction': direction,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取仓库贡献者列表
   * 获取仓库贡献者列表
   * @returns ProjectContributor 获取仓库贡献者列表
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdContributors({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    ref,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 分支，不传则取仓库的默认分支
     */
    ref?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ProjectContributor>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/contributors',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'ref': ref,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取仓库基本设置
   * 获取仓库基本设置
   * @returns ProjectBaseSetting 获取仓库基本设置
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdSettings({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<ProjectBaseSetting> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/settings',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 更新仓库设置
   * 更新仓库设置
   * @returns ProjectBaseSetting 更新仓库设置
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectId({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    name,
    path,
    description,
    homepage,
    langId,
    defaultBranch,
    outsourced,
    creatorId,
    canComment,
    issueComment,
    issuesEnabled,
    securityHoleEnabled,
    forkEnabled,
    onlineEditEnabled,
    pullRequestsEnabled,
    wikiEnabled,
    lightweightPrEnabled,
    prMasterOnly,
    forbidForcePush,
    importUrl,
    forbidForceSync,
    svnEnabled,
    canReadonly,
    programAddIds,
    programRemoveIds,
    giteeGoEnabled,
    programPipelineEnabled,
    templateEnabled,
    mergeEnabled,
    squashEnabled,
    rebaseEnabled,
    defaultMergeMethod,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 仓库名称
     */
    name?: string,
    /**
     * 仓库路径
     */
    path?: string,
    /**
     * 仓库介绍
     */
    description?: string,
    /**
     * 主页
     */
    homepage?: string,
    /**
     * 语言
     */
    langId?: number,
    /**
     * 默认分支
     */
    defaultBranch?: string,
    /**
     * 类型，0：内部，1：外包
     */
    outsourced?: number,
    /**
     * 仓库负责人
     */
    creatorId?: number,
    /**
     * 允许用户对仓库进行评论
     */
    canComment?: number,
    /**
     * 允许用户对"关闭"状态的 Issues 进行评论
     */
    issueComment?: number,
    /**
     * 轻量级的 issue 跟踪系统
     */
    issuesEnabled?: number,
    /**
     * 允许用户创建涉及敏感信息的 Issue，提交后不公开此 Issue（可见范围：仓库成员、企业成员）
     */
    securityHoleEnabled?: number,
    /**
     * 是否允许仓库被 Fork
     */
    forkEnabled?: number,
    /**
     * 是否允许仓库文件在线编辑
     */
    onlineEditEnabled?: number,
    /**
     * 接受 pull request，协作开发
     */
    pullRequestsEnabled?: number,
    /**
     * 可以编写文档
     */
    wikiEnabled?: number,
    /**
     * 接受轻量级 Pull Request
     */
    lightweightPrEnabled?: number,
    /**
     * 开启的 Pull Request，仅管理员、审查者、测试者可见
     */
    prMasterOnly?: number,
    /**
     * 禁止强制推送
     */
    forbidForcePush?: number,
    /**
     * 仓库远程地址
     */
    importUrl?: string,
    /**
     * 禁止仓库同步
     */
    forbidForceSync?: number,
    /**
     * 使用 SVN 管理您的仓库
     */
    svnEnabled?: number,
    /**
     * 开启文件/文件夹只读功能
     */
    canReadonly?: number,
    /**
     * 待添加的关联项目列表，英文逗号分隔
     */
    programAddIds?: string,
    /**
     * 待移除的关联项目列表，英文逗号分隔
     */
    programRemoveIds?: string,
    /**
     * GiteeGo 启停状态，1 表示启用，0 表示停用
     */
    giteeGoEnabled?: number,
    /**
     * 是否支持项目流水线，1 表示支持，0 表示不支持
     */
    programPipelineEnabled?: number,
    /**
     * 仓库模板启停状态
     */
    templateEnabled?: boolean,
    /**
     * 是否开启 merge 合并方式，默认为开启
     */
    mergeEnabled?: boolean,
    /**
     * 是否开启 squash 合并方式，默认为开启
     */
    squashEnabled?: boolean,
    /**
     * 是否开启 rebase 合并方式，默认为开启
     */
    rebaseEnabled?: boolean,
    /**
     * 选择默认合并 Pull Request 的方式，分别为 merge squash rebase
     */
    defaultMergeMethod?: 'merge' | 'squash' | 'rebase',
  }): CancelablePromise<ProjectBaseSetting> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'name': name,
        'path': path,
        'description': description,
        'homepage': homepage,
        'lang_id': langId,
        'default_branch': defaultBranch,
        'outsourced': outsourced,
        'creator_id': creatorId,
        'can_comment': canComment,
        'issue_comment': issueComment,
        'issues_enabled': issuesEnabled,
        'security_hole_enabled': securityHoleEnabled,
        'fork_enabled': forkEnabled,
        'online_edit_enabled': onlineEditEnabled,
        'pull_requests_enabled': pullRequestsEnabled,
        'wiki_enabled': wikiEnabled,
        'lightweight_pr_enabled': lightweightPrEnabled,
        'pr_master_only': prMasterOnly,
        'forbid_force_push': forbidForcePush,
        'import_url': importUrl,
        'forbid_force_sync': forbidForceSync,
        'svn_enabled': svnEnabled,
        'can_readonly': canReadonly,
        'program_add_ids': programAddIds,
        'program_remove_ids': programRemoveIds,
        'gitee_go_enabled': giteeGoEnabled,
        'program_pipeline_enabled': programPipelineEnabled,
        'template_enabled': templateEnabled,
        'merge_enabled': mergeEnabled,
        'squash_enabled': squashEnabled,
        'rebase_enabled': rebaseEnabled,
        'default_merge_method': defaultMergeMethod,
      },
    });
  }

  /**
   * 仓库概览信息
   * 仓库概览信息
   * @returns ProjectOverview 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectId({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<ProjectOverview> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 获取仓库动态
   * 获取仓库动态
   * @returns Event 获取仓库动态
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdEvents({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    startDate,
    endDate,
    page = 1,
    perPage,
    prevId,
    limit,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 查询的起始时间。(格式：yyyy-mm-dd)
     */
    startDate?: string,
    /**
     * 查询的结束时间。(格式：yyyy-mm-dd)
     */
    endDate?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 上一次动态列表中最小动态 ID (返回列表不包含该 ID 记录)
     */
    prevId?: number,
    /**
     * 每次获取动态的条数
     */
    limit?: number,
  }): CancelablePromise<Event> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/events',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'start_date': startDate,
        'end_date': endDate,
        'page': page,
        'per_page': perPage,
        'prev_id': prevId,
        'limit': limit,
      },
    });
  }

}
