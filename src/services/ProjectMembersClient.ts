/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Member } from '../models/Member';
import type { MemberApplication } from '../models/MemberApplication';
import type { ProjectMember } from '../models/ProjectMember';
import type { ProjectMemberAdd } from '../models/ProjectMemberAdd';
import type { ProjectMemberOverview } from '../models/ProjectMemberOverview';
import type { ProjectRole } from '../models/ProjectRole';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ProjectMembersClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 仓库成员
   * 仓库成员
   * @returns ProjectMember 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdMembers({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    accessLevel,
    scope,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * reporter:报告者，viewer:观察者，developer:开发者，master:管理员
     */
    accessLevel?: 'reporter' | 'viewer' | 'developer' | 'master',
    /**
     * not_in:获取不在本仓库的企业成员
     */
    scope?: 'not_in',
    /**
     * 成员搜索，邮箱、username、name、remark
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<ProjectMember> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/members',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'access_level': accessLevel,
        'scope': scope,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 添加仓库成员
   * 添加仓库成员
   * @returns ProjectMemberAdd 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdMembers({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    users,
    groups,
    accessLevel,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 要添加的成员信息，例如 [{"id":"13", "access":"30", "name":"真喜洋洋 (xiyangyang)", "username":"xiyangyang"}]
     */
    users?: string,
    /**
     * 授权团队的 id，多个 ID 通过英文逗号分隔
     */
    groups?: string,
    /**
     * 授权团队的成员在仓库的权限
     */
    accessLevel?: 10 | 15 | 25 | 30 | 40,
  }): CancelablePromise<ProjectMemberAdd> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/members',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'users': users,
        'groups': groups,
        'access_level': accessLevel,
      },
    });
  }

  /**
   * 获取仓库成员和仓库团队成员
   * 获取仓库成员和仓库团队成员
   * @returns ProjectMember 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdMembersWithTeamMembers({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    scope,
    search,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * not_in:获取不在本仓库的企业成员
     */
    scope?: 'not_in',
    /**
     * 成员搜索，邮箱、username、name、remark
     */
    search?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<ProjectMember> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/members/with_team_members',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'scope': scope,
        'search': search,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 删除仓库成员申请
   * 删除仓库成员申请
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdMembersApplyApplyId({
    enterpriseId,
    projectId,
    applyId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 成员申请 id
     */
    applyId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/members/apply/{apply_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'apply_id': applyId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 修改仓库成员权限
   * 修改仓库成员权限
   * @returns Member 修改仓库成员权限
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdMembersMemberId({
    enterpriseId,
    projectId,
    memberId,
    projectAccess,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 成员 id
     */
    memberId: number,
    /**
     * 仓库角色级别，报告者:15，观察者:25，开发者:30，管理员:40，负责人:100
     */
    projectAccess: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<Member> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/members/{member_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'member_id': memberId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'project_access': projectAccess,
      },
    });
  }

  /**
   * 移除仓库成员
   * 移除仓库成员
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdMembersMemberId({
    enterpriseId,
    projectId,
    memberId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 成员 id
     */
    memberId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/members/{member_id}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'member_id': memberId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 获取仓库角色
   * 获取仓库角色
   * @returns ProjectRole 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdMembersRoles({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<ProjectRole> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/members/roles',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 仓库成员申请列表
   * 仓库成员申请列表
   * @returns MemberApplication 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdMembersApplyList({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    query,
    status,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 搜索字符串：申请人姓名或个人空间地址
     */
    query?: string,
    /**
     * 状态 [approved:已同意，pending:待审核，refused:已拒绝，overdue:已过期，invite_pass:已接受邀请]
     */
    status?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<MemberApplication> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/members/apply_list',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'query': query,
        'status': status,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 成员概览
   * 成员概览
   * @returns ProjectMemberOverview 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdMembersOverview({
    enterpriseId,
    projectId,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<ProjectMemberOverview> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/members/overview',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 获取仓库授权的团队列表
   * 获取仓库授权的团队列表
   * @returns any 获取仓库授权的团队列表
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdAuthGroups({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    page = 1,
    perPage,
    accessLevel,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 仓库角色等级
     */
    accessLevel?: 10 | 15 | 25 | 30 | 40,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/auth_groups',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'page': page,
        'per_page': perPage,
        'access_level': accessLevel,
      },
    });
  }

  /**
   * 调整团队授权
   * 调整团队授权
   * @returns any 调整团队授权
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdAuthGroups({
    enterpriseId,
    projectId,
    groupIds,
    accessLevel,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 被授权团队的 id，多个 id 用英文逗号分隔
     */
    groupIds: string,
    /**
     * 授权团队的成员在仓库的权限
     */
    accessLevel: 10 | 15 | 25 | 30 | 40,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/auth_groups',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'group_ids': groupIds,
        'access_level': accessLevel,
      },
    });
  }

  /**
   * 撤销团队授权
   * 撤销团队授权
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdAuthGroups({
    enterpriseId,
    projectId,
    groupIds,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 被授权团队的 id，多个 id 用英文逗号分隔
     */
    groupIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/auth_groups',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'group_ids': groupIds,
      },
    });
  }

}
