/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DocDirectory } from '../models/DocDirectory';
import type { DocNode } from '../models/DocNode';
import type { DocNodeDetail } from '../models/DocNodeDetail';
import type { DocNodeLevel } from '../models/DocNodeLevel';
import type { DocRecent } from '../models/DocRecent';
import type { FileVersion } from '../models/FileVersion';
import type { ProgramDirectory } from '../models/ProgramDirectory';
import type { ScrumDocNode } from '../models/ScrumDocNode';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class DocNodesClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 新建文档
   * 新建文档
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesCreate({
    enterpriseId,
    name,
    accessToken,
    parentId,
    programId,
    authType = 'inherit',
    scrumSprintId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文档名称
     */
    name: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 所属文件夹的 doc_node 的 id，默认为 0
     */
    parentId?: number,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 权限; 继承：inherit; 私有：private; 只读 read_only; 读写：read_write
     */
    authType?: string,
    /**
     * 迭代 id
     */
    scrumSprintId?: number,
  }): CancelablePromise<Array<DocNode>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/create',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'parent_id': parentId,
        'program_id': programId,
        'auth_type': authType,
        'scrum_sprint_id': scrumSprintId,
      },
    });
  }

  /**
   * 新建 Wiki
   * 新建 Wiki
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesCreateWiki({
    enterpriseId,
    projectId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 仓库 id
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<DocNode>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/create_wiki',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'project_id': projectId,
      },
    });
  }

  /**
   * 检测附件是否重名
   * 检测附件是否重名
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesCheckAttachFileName({
    enterpriseId,
    name,
    parentId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 附件名称
     */
    name: string,
    /**
     * 层级 id
     */
    parentId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/check_attach_file_name',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'name': name,
        'parent_id': parentId,
      },
    });
  }

  /**
   * 上传附件（覆盖）
   * 上传附件（覆盖）
   * @returns any 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesCoverAttachFile({
    enterpriseId,
    file,
    parentId,
    fileId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 上传的文件
     */
    file: Blob,
    /**
     * 上传的层级
     */
    parentId: number,
    /**
     * 覆盖的文件节点
     */
    fileId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/cover_attach_file',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'file': file,
        'parent_id': parentId,
        'file_id': fileId,
      },
    });
  }

  /**
   * 上传附件
   * 上传附件
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesAttachFile({
    enterpriseId,
    file,
    accessToken,
    programId,
    parentId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 上传的文件
     */
    file: Blob,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 父级目录
     */
    parentId?: number,
  }): CancelablePromise<Array<DocNode>> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/attach_file',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'file': file,
        'program_id': programId,
        'parent_id': parentId,
      },
    });
  }

  /**
   * 查看文件节点详情
   * 查看文件节点详情
   * @returns DocNodeDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesDocNodeId({
    enterpriseId,
    docNodeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<DocNodeDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 彻底删除文件节点
   * 彻底删除文件节点
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdDocNodesDocNodeId({
    enterpriseId,
    docNodeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新文件节点
   * 更新文件节点
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesDocNodeId({
    enterpriseId,
    docNodeId,
    accessToken,
    name,
    password,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * doc_node 节点 id
     */
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 文件夹名称
     */
    name?: string,
    /**
     * 设置访问密码（此字段填入空串可取消密码）
     */
    password?: string,
  }): CancelablePromise<DocNode> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'password': password,
      },
    });
  }

  /**
   * 备份文档
   * 备份文档
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesDocNodeIdBackup({
    enterpriseId,
    name,
    docNodeId,
    accessToken,
    parentId,
    programId,
    authType = 'inherit',
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文档名称
     */
    name: string,
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 所属文件夹的 doc_node 的 id，默认为 0
     */
    parentId?: number,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 权限; 继承：inherit; 私有：private; 只读 read_only; 读写：read_write
     */
    authType?: string,
  }): CancelablePromise<DocNode> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/backup',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'parent_id': parentId,
        'program_id': programId,
        'auth_type': authType,
      },
    });
  }

  /**
   * 获取文件节点的权限
   * 获取文件节点的权限
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesDocNodeIdOperateAuths({
    enterpriseId,
    docNodeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/operate_auths',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取文件节点列表（平铺）
   * 获取文件节点列表（平铺）
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesTile({
    enterpriseId,
    accessToken,
    parentId,
    creatorId,
    programId,
    scope,
    fileType,
    sort,
    direction = 'desc',
    scrumSprintId,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 父级 id（默认：0）
     */
    parentId?: number,
    /**
     * 创建者 id
     */
    creatorId?: number,
    /**
     * 项目 id(默认：0)
     */
    programId?: number,
    /**
     * 筛选项，Wiki、AttachFile（附件）、create_by_me
     */
    scope?: string,
    /**
     * 文件类型（WikiInfo,Wiki,AttachFile），支持多选，用逗号分割
     */
    fileType?: string,
    /**
     * 排序字段 (name size updated_at created_at creator_id public)
     */
    sort?: string,
    /**
     * 可选。升序/降序
     */
    direction?: 'asc' | 'desc',
    /**
     * 迭代 ID
     */
    scrumSprintId?: number,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<DocNode>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/tile',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'parent_id': parentId,
        'creator_id': creatorId,
        'program_id': programId,
        'scope': scope,
        'file_type': fileType,
        'sort': sort,
        'direction': direction,
        'scrum_sprint_id': scrumSprintId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取文件节点列表（层级）
   * 获取文件节点列表（层级）
   * @returns DocNodeLevel 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesLevel({
    enterpriseId,
    accessToken,
    parentId,
    programId,
    scope,
    fileType,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 父级 id（默认：0）
     */
    parentId: number,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 可筛选类型：directory, recycle
     */
    scope?: string,
    /**
     * 文件类型（WikiInfo,Wiki,DocDirectory,AttachFile），支持多选，用逗号分割
     */
    fileType?: string,
  }): CancelablePromise<Array<DocNodeLevel>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/level',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'parent_id': parentId,
        'program_id': programId,
        'scope': scope,
        'file_type': fileType,
      },
    });
  }

  /**
   * 获取文件夹
   * 获取文件夹
   * @returns DocDirectory 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesDirectories({
    enterpriseId,
    accessToken,
    programId,
    scrumSprintId,
    sprintDocId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 id
     */
    programId?: number,
    /**
     * 迭代 ID
     */
    scrumSprintId?: number,
    /**
     * 迭代默认生成的文件夹 ID
     */
    sprintDocId?: number,
  }): CancelablePromise<Array<DocDirectory>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/directories',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
        'scrum_sprint_id': scrumSprintId,
        'sprint_doc_id': sprintDocId,
      },
    });
  }

  /**
   * 获取项目类型文件夹
   * 获取项目类型文件夹
   * @returns ProgramDirectory 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesProgramsDirectories({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<ProgramDirectory>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/programs_directories',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取最近编辑的文件
   * 获取最近编辑的文件
   * @returns DocRecent 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesRecent({
    enterpriseId,
    accessToken,
    sort,
    direction = 'desc',
    scrumSprintId,
    fileType,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (name size updated_at created_at creator_id public)
     */
    sort?: string,
    /**
     * 可选。升序/降序
     */
    direction?: 'asc' | 'desc',
    /**
     * 迭代 ID
     */
    scrumSprintId?: number,
    /**
     * 文件类型（WikiInfo,Wiki,AttachFile），支持多选，用逗号分割
     */
    fileType?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<DocRecent>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/recent',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'scrum_sprint_id': scrumSprintId,
        'file_type': fileType,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取最近编辑的文档
   * 获取最近编辑的文档
   * @returns ScrumDocNode 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesRecentDoc({
    enterpriseId,
    accessToken,
    programId,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 ID
     */
    programId?: number,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<ScrumDocNode>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/recent_doc',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取授权用户收藏的文件节点（仅顶层）
   * 获取授权用户收藏的文件节点（仅顶层）
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesCollection({
    enterpriseId,
    accessToken,
    programId,
    sort,
    fileType,
    direction = 'desc',
    scrumSprintId,
    wikiInfoOnly,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 项目 id（默认：0）
     */
    programId?: number,
    /**
     * 排序字段 (name size updated_at created_at creator_id public)
     */
    sort?: string,
    /**
     * 文件类型（WikiInfo,Wiki,DocDirectory,AttachFile），支持多选，用逗号分割
     */
    fileType?: string,
    /**
     * 可选。升序/降序
     */
    direction?: 'asc' | 'desc',
    /**
     * 迭代 ID
     */
    scrumSprintId?: number,
    /**
     * 是否仅展示文档
     */
    wikiInfoOnly?: boolean,
  }): CancelablePromise<Array<DocNode>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/collection',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
        'sort': sort,
        'file_type': fileType,
        'direction': direction,
        'scrum_sprint_id': scrumSprintId,
        'wiki_info_only': wikiInfoOnly,
      },
    });
  }

  /**
   * 获取与文档有关的项目
   * 获取与文档有关的项目
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesPrograms({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Array<DocNode>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/programs',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 收藏/取消收藏文件节点
   * 收藏/取消收藏文件节点
   * @returns any 收藏/取消收藏文件节点
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesDocNodeIdCollection({
    enterpriseId,
    docNodeId,
    operateType,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * doc_node 节点 id
     */
    docNodeId: number,
    /**
     * 收藏：set; 取消收藏：unset
     */
    operateType: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/collection',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      formData: {
        'access_token': accessToken,
        'operate_type': operateType,
      },
    });
  }

  /**
   * 批量收藏/取消收藏文件节点
   * 批量收藏/取消收藏文件节点
   * @returns any 批量收藏/取消收藏文件节点
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesBatchCollection({
    enterpriseId,
    docNodeIds,
    operateType,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文件 id，使用，，隔开
     */
    docNodeIds: string,
    /**
     * 收藏：set; 取消收藏：unset
     */
    operateType: 'set' | 'unset',
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/batch_collection',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'doc_node_ids': docNodeIds,
        'operate_type': operateType,
      },
    });
  }

  /**
   * 获取回收站的内容列表
   * 获取回收站的内容列表
   * @returns any 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesRecycle({
    enterpriseId,
    accessToken,
    parentId,
    scrumSprintId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 父级 id（默认：0）
     */
    parentId?: number,
    /**
     * 迭代 ID
     */
    scrumSprintId?: number,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/recycle',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'parent_id': parentId,
        'scrum_sprint_id': scrumSprintId,
      },
    });
  }

  /**
   * 移动到回收站
   * 移动到回收站
   * @returns any 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesDocNodeIdRecycle({
    enterpriseId,
    docNodeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/recycle',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      formData: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 批量移除到回收站
   * 批量移除到回收站
   * @returns any 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesBatchRecycle({
    enterpriseId,
    docNodeIds,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文件 id，使用，，隔开
     */
    docNodeIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/batch_recycle',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'doc_node_ids': docNodeIds,
      },
    });
  }

  /**
   * 批量彻底删除文件节点
   * 批量彻底删除文件节点
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdDocNodesBatchDelete({
    enterpriseId,
    docNodeIds,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文件 id，使用，，隔开
     */
    docNodeIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/doc_nodes/batch_delete',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'doc_node_ids': docNodeIds,
      },
    });
  }

  /**
   * 创建文件夹
   * 创建文件夹
   * @returns any 创建文件夹
   * @throws ApiError
   */
  public postEnterpriseIdDocNodesDirectory({
    enterpriseId,
    name,
    accessToken,
    parentId,
    programId,
    authType = 'inherit',
    scrumSprintId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文件夹名称
     */
    name: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 父级 id（默认：0）
     */
    parentId?: number,
    /**
     * 归属的项目 id
     */
    programId?: number,
    /**
     * 权限; 继承：inherit; 私有：private; 只读 read_only; 读写：read_write
     */
    authType?: string,
    /**
     * 迭代 ID
     */
    scrumSprintId?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/doc_nodes/directory',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'parent_id': parentId,
        'program_id': programId,
        'auth_type': authType,
        'scrum_sprint_id': scrumSprintId,
      },
    });
  }

  /**
   * 置顶节点
   * 置顶节点
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesDocNodeIdIsTop({
    enterpriseId,
    docNodeId,
    value,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * doc_node 节点 id
     */
    docNodeId: number,
    /**
     * 是否置顶（置顶：1，取消置顶：0）
     */
    value: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<DocNode> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/is_top',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      formData: {
        'access_token': accessToken,
        'value': value,
      },
    });
  }

  /**
   * 移动文件节点
   * 移动文件节点
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesDocNodeIdMove({
    enterpriseId,
    docNodeId,
    parentId,
    accessToken,
    programId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * doc_node 节点 id
     */
    docNodeId: number,
    /**
     * 挂载到文件下（此处填入上级的 doc_node 节点的 id; 若移动到根目录，parent_id: 0）
     */
    parentId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 挂载到某项目下（此处填入项目 id）
     */
    programId?: number,
  }): CancelablePromise<DocNode> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/move',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      formData: {
        'access_token': accessToken,
        'parent_id': parentId,
        'program_id': programId,
      },
    });
  }

  /**
   * 批量移动文件节点
   * 批量移动文件节点
   * @returns any 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesBatchMove({
    enterpriseId,
    docNodeIds,
    parentId,
    accessToken,
    programId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 文件 id，使用，，隔开
     */
    docNodeIds: string,
    /**
     * 挂载到文件下（此处填入上级的 doc_node 节点的 id; 若移动到根目录，parent_id: 0）
     */
    parentId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 挂载到某项目下（此处填入项目 id）
     */
    programId?: number,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/batch_move',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'doc_node_ids': docNodeIds,
        'parent_id': parentId,
        'program_id': programId,
      },
    });
  }

  /**
   * 查看文件节点的权限
   * 查看文件节点的权限
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesDocNodeIdAuth({
    enterpriseId,
    docNodeId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * doc_node 节点 id
     */
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<DocNode> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/auth',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 更新文件节点的权限
   * 更新文件节点的权限
   * @returns DocNode 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesDocNodeIdAuth({
    enterpriseId,
    docNodeId,
    accessToken,
    guessAccess,
    enterpriseAccess,
    accessTargetType,
    accessTargetId,
    accessAccessLevel,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * doc_node 节点 id
     */
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 访客的访问权限; (无权限：0; 只读：1;)
     */
    guessAccess?: number,
    /**
     * 企业成员的访问权限 (无权限：0; 只读：1; 读写：2)
     */
    enterpriseAccess?: number,
    /**
     * 关联类型 (target_type: program|group|user)
     */
    accessTargetType?: Array<string>,
    /**
     * 关联类型的 id(如 target_type = program，则此处应为对应 program 的 id)
     */
    accessTargetId?: Array<number>,
    /**
     * 访问属性（无权限：0; 只读：1; 读写：2）
     */
    accessAccessLevel?: Array<number>,
  }): CancelablePromise<DocNode> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/auth',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      formData: {
        'access_token': accessToken,
        'guess_access': guessAccess,
        'enterprise_access': enterpriseAccess,
        'access[target_type]': accessTargetType,
        'access[target_id]': accessTargetId,
        'access[access_level]': accessAccessLevel,
      },
    });
  }

  /**
   * 批量更新文件节点的权限
   * 批量更新文件节点的权限
   * @returns any 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdDocNodesBatchAuth({
    enterpriseId,
    docNodeIds,
    accessToken,
    guessAccess,
    enterpriseAccess,
    accessTargetType,
    accessTargetId,
    accessAccessLevel,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * doc_node 节点 id，使用，，隔开
     */
    docNodeIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 访客的访问权限; (无权限：0; 只读：1;)
     */
    guessAccess?: number,
    /**
     * 企业成员的访问权限 (无权限：0; 只读：1; 读写：2)
     */
    enterpriseAccess?: number,
    /**
     * 关联类型 (target_type: program|group|user)
     */
    accessTargetType?: Array<string>,
    /**
     * 关联类型的 id(如 target_type = program，则此处应为对应 program 的 id)
     */
    accessTargetId?: Array<number>,
    /**
     * 访问属性（无权限：0; 只读：1; 读写：2）
     */
    accessAccessLevel?: Array<number>,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/doc_nodes/batch_auth',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'doc_node_ids': docNodeIds,
        'guess_access': guessAccess,
        'enterprise_access': enterpriseAccess,
        'access[target_type]': accessTargetType,
        'access[target_id]': accessTargetId,
        'access[access_level]': accessAccessLevel,
      },
    });
  }

  /**
   * 查看历史版本
   * 查看历史版本
   * @returns FileVersion 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdDocNodesDocNodeIdFileVersions({
    enterpriseId,
    docNodeId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 节点 id
     */
    docNodeId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<FileVersion>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/file_versions',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 删除历史版本
   * 删除历史版本
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdDocNodesDocNodeIdFileVersions({
    enterpriseId,
    docNodeId,
    attachFileIds,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 节点 id
     */
    docNodeId: number,
    /**
     * 要删除的文件 id（多个用逗号分隔）
     */
    attachFileIds: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/doc_nodes/{doc_node_id}/file_versions',
      path: {
        'enterprise_id': enterpriseId,
        'doc_node_id': docNodeId,
      },
      query: {
        'access_token': accessToken,
        'attach_file_ids': attachFileIds,
      },
    });
  }

}
