/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Label } from '../models/Label';
import type { LabelDetail } from '../models/LabelDetail';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class LabelsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取标签列表
   * 获取标签列表
   * @returns LabelDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdLabels({
    enterpriseId,
    accessToken,
    sort,
    direction,
    search,
    labelIds,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 排序字段 (created_at、updated_at, serial)
     */
    sort?: string,
    /**
     * 排序方向 (asc: 升序 desc: 倒序)
     */
    direction?: string,
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 根据 id 返回标签列表
     */
    labelIds?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Array<LabelDetail>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/labels',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'sort': sort,
        'direction': direction,
        'search': search,
        'label_ids': labelIds,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新增标签
   * 新增标签
   * @returns LabelDetail 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdLabels({
    enterpriseId,
    name,
    color,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 标签名称
     */
    name: string,
    /**
     * 标签颜色
     */
    color: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<LabelDetail> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/labels',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'color': color,
      },
    });
  }

  /**
   * 更新标签排序
   * 更新标签排序
   * @returns any 更新标签排序
   * @throws ApiError
   */
  public putEnterpriseIdLabelsChangeSerial({
    labelIds,
    enterpriseId,
    accessToken,
  }: {
    /**
     * 标签 id(预期顺序), 逗号隔开
     */
    labelIds: string,
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/labels/change_serial',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'label_ids': labelIds,
      },
    });
  }

  /**
   * 更新标签
   * 更新标签
   * @returns Label 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdLabelsLabelId({
    enterpriseId,
    labelId,
    name,
    color,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 标签 id
     */
    labelId: number,
    /**
     * 标签名称
     */
    name: string,
    /**
     * 标签颜色
     */
    color: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<Label> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/labels/{label_id}',
      path: {
        'enterprise_id': enterpriseId,
        'label_id': labelId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'color': color,
      },
    });
  }

  /**
   * 删除标签
   * 删除标签
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdLabelsLabelId({
    enterpriseId,
    labelId,
    accessToken,
  }: {
    enterpriseId: number,
    labelId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/labels/{label_id}',
      path: {
        'enterprise_id': enterpriseId,
        'label_id': labelId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
