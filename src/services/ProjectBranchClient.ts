/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Branch } from '../models/Branch';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ProjectBranchClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取仓库的分支列表
   * 获取仓库的分支列表
   * @returns Branch 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdProjectsProjectIdBranches({
    enterpriseId,
    projectId,
    accessToken,
    qt,
    search,
    sort = 'updated_desc',
    mode = 'all',
    creatorId,
    needModeCount,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 搜索关键字
     */
    search?: string,
    /**
     * 排序
     */
    sort?: 'updated_desc' | 'updated_asc' | 'name_asc' | 'name_desc',
    /**
     * 类型 (all: 全部分支，active: 活跃分支，stale: 非活跃分支，deleted: 已删除分支)
     */
    mode?: 'all' | 'active' | 'stale' | 'deleted' | 'created_by_me',
    /**
     * 分支创建者
     */
    creatorId?: number,
    /**
     * 是否需要按照类型分组统计数量
     */
    needModeCount?: boolean,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Branch> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/projects/{project_id}/branches',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
        'search': search,
        'sort': sort,
        'mode': mode,
        'creator_id': creatorId,
        'need_mode_count': needModeCount,
        'page': page,
        'per_page': perPage,
      },
      errors: {
        404: `没有相关数据`,
      },
    });
  }

  /**
   * 添加/恢复分支
   * 添加/恢复分支
   * @returns Branch 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdProjectsProjectIdBranches({
    enterpriseId,
    projectId,
    branchName,
    refs,
    accessToken,
    qt,
    action = 'new',
    type = 0,
    remark,
    newName,
    refProjectId,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 分支名称
     */
    branchName: string,
    /**
     * 起点
     */
    refs: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 动作：new(新建)，recovery(恢复)
     */
    action?: 'new' | 'recovery',
    /**
     * 分支状态，常规分支:0，保护分支:1，只读分支:2
     */
    type?: 0 | 1 | 2,
    /**
     * 分支备注
     */
    remark?: string,
    /**
     * 在恢复分支时新的分支名称
     */
    newName?: string,
    /**
     * 新建仓库时起点所属仓库
     */
    refProjectId?: number,
  }): CancelablePromise<Branch> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/projects/{project_id}/branches',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'branch_name': branchName,
        'refs': refs,
        'action': action,
        'type': type,
        'remark': remark,
        'new_name': newName,
        'ref_project_id': refProjectId,
      },
    });
  }

  /**
   * 设为默认分支
   * 设为默认分支
   * @returns any 设为默认分支
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdBranchesNameSetDefault({
    enterpriseId,
    projectId,
    name,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 分支名
     */
    name: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/branches/{name}/set_default',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'name': name,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 编辑分支
   * 编辑分支
   * @returns Branch 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdProjectsProjectIdBranchesName({
    enterpriseId,
    projectId,
    name,
    accessToken,
    qt,
    type,
    remark,
    newName,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 分支名
     */
    name: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
    /**
     * 分支状态，常规分支:0，保护分支:1，只读分支:2
     */
    type?: number,
    /**
     * 分支备注
     */
    remark?: string,
    /**
     * 新的分支名称
     */
    newName?: string,
  }): CancelablePromise<Branch> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/projects/{project_id}/branches/{name}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'name': name,
      },
      formData: {
        'access_token': accessToken,
        'qt': qt,
        'type': type,
        'remark': remark,
        'new_name': newName,
      },
    });
  }

  /**
   * 删除分支
   * 删除分支
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdBranchesName({
    enterpriseId,
    projectId,
    name,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 分支名
     */
    name: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/branches/{name}',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
        'name': name,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
    });
  }

  /**
   * 批量删除分支
   * 批量删除分支
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdProjectsProjectIdBranchesBatchDestroy({
    enterpriseId,
    projectId,
    names,
    accessToken,
    qt,
  }: {
    /**
     * 企业 id
     */
    enterpriseId: number,
    /**
     * 仓库 id 或 path
     */
    projectId: string,
    /**
     * 分支名数组
     */
    names: Array<string>,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * path 类型（查询参数为 path）, 空则表示查询参数为 id
     */
    qt?: 'path',
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/projects/{project_id}/branches/batch_destroy',
      path: {
        'enterprise_id': enterpriseId,
        'project_id': projectId,
      },
      query: {
        'access_token': accessToken,
        'qt': qt,
      },
      formData: {
        'names': names,
      },
    });
  }

}
