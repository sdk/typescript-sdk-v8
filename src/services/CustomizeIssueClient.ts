/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { IssueField } from '../models/IssueField';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class CustomizeIssueClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 获取管理界面的字段列表
   * 获取管理界面的字段列表
   * @returns IssueField 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdCustomizeList({
    enterpriseId,
    accessToken,
    page = 1,
    perPage,
    sort,
    issueTypeId,
    issueTypeCategory,
    programId,
    type,
    search,
  }: {
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
    /**
     * 是否排序（被选的字段排后面）
     */
    sort?: 0 | 1,
    /**
     * 任务类型 id
     */
    issueTypeId?: number,
    /**
     * 任务类型属性
     */
    issueTypeCategory?: 'task' | 'bug' | 'requirement',
    /**
     * 项目 ID
     */
    programId?: number,
    /**
     * 获取字段列表来源（导出/企业管理）
     */
    type?: 'export',
    /**
     * 搜索关键字
     */
    search?: string,
  }): CancelablePromise<IssueField> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/customize/list',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
        'sort': sort,
        'issue_type_id': issueTypeId,
        'issue_type_category': issueTypeCategory,
        'program_id': programId,
        'type': type,
        'search': search,
      },
    });
  }

}
