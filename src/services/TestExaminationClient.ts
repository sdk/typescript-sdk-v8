/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class TestExaminationClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 删除测试评审
   * 删除测试评审
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdTestExaminationsTestExaminationId({
    enterpriseId,
    programId,
    testExaminationId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 项目 ID
     */
    programId: number,
    testExaminationId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/test_examinations/{test_examination_id}',
      path: {
        'enterprise_id': enterpriseId,
        'test_examination_id': testExaminationId,
      },
      query: {
        'access_token': accessToken,
        'program_id': programId,
      },
    });
  }

}
