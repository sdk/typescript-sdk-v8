/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Event } from '../models/Event';
import type { MemberWeekReport } from '../models/MemberWeekReport';
import type { MyWeekReport } from '../models/MyWeekReport';
import type { WeekReportDetail } from '../models/WeekReportDetail';
import type { WeekReportNote } from '../models/WeekReportNote';
import type { WeekReportPreview } from '../models/WeekReportPreview';
import type { WeekReportRelation } from '../models/WeekReportRelation';
import type { WeekReportTemplate } from '../models/WeekReportTemplate';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class WeekReportsClient {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * 成员周报列表
   * 成员周报列表
   * @returns MemberWeekReport 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsMemberReports({
    enterpriseId,
    accessToken,
    year = 2023,
    weekIndex,
    programId,
    groupId,
    memberIds,
    date,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 年份
     */
    year?: number,
    /**
     * 周报所属周
     */
    weekIndex?: number,
    /**
     * 项目 ID
     */
    programId?: number,
    /**
     * 团队 ID
     */
    groupId?: number,
    /**
     * 成员 ID，逗号分隔，如：1,2
     */
    memberIds?: string,
    /**
     * 周报日期 (格式：2019-03-25)
     */
    date?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<MemberWeekReport> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/member_reports',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'year': year,
        'week_index': weekIndex,
        'program_id': programId,
        'group_id': groupId,
        'member_ids': memberIds,
        'date': date,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 获取授权用户的周报列表
   * 获取授权用户的周报列表
   * @returns MyWeekReport 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsMyReports({
    enterpriseId,
    accessToken,
    year = 2023,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 年份
     */
    year?: number,
  }): CancelablePromise<MyWeekReport> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/my_reports',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'year': year,
      },
    });
  }

  /**
   * 获取未提交周报的用户列表
   * 获取未提交周报的用户列表
   * @returns MyWeekReport 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsNotSubmitUsers({
    enterpriseId,
    accessToken,
    year = 2023,
    weekIndex,
    programId,
    groupId,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 年份
     */
    year?: number,
    /**
     * 当前周数（默认当前周）
     */
    weekIndex?: number,
    /**
     * 项目的 id
     */
    programId?: number,
    /**
     * 团队的 id
     */
    groupId?: number,
  }): CancelablePromise<MyWeekReport> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/not_submit_users',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'year': year,
        'week_index': weekIndex,
        'program_id': programId,
        'group_id': groupId,
      },
    });
  }

  /**
   * 获取某人某年某周的周报详情
   * 获取某人某年某周的周报详情
   * @returns WeekReportDetail 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsUsernameYearWeekIndex({
    enterpriseId,
    username,
    year,
    weekIndex,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户个性域名
     */
    username: string,
    /**
     * 周报所处年
     */
    year: number,
    /**
     * 周报所处周
     */
    weekIndex: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<WeekReportDetail> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/{username}/{year}/{week_index}',
      path: {
        'enterprise_id': enterpriseId,
        'username': username,
        'year': year,
        'week_index': weekIndex,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 新建/编辑周报
   * 新建/编辑周报
   * @returns WeekReportDetail 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdWeekReportsUsernameYearWeekIndex({
    enterpriseId,
    year,
    content,
    weekIndex,
    username,
    accessToken,
    pullRequestIds,
    issueIds,
    eventIds,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 周报所属年
     */
    year: number,
    /**
     * 周报内容
     */
    content: string,
    /**
     * 周报所属周
     */
    weekIndex: number,
    /**
     * 用户名 (username/login)
     */
    username: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 周报关联 PR
     */
    pullRequestIds?: Array<number>,
    /**
     * 周报关联 issue
     */
    issueIds?: Array<number>,
    /**
     * 关联动态
     */
    eventIds?: Array<number>,
  }): CancelablePromise<WeekReportDetail> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/week_reports/{username}/{year}/{week_index}',
      path: {
        'enterprise_id': enterpriseId,
        'year': year,
        'week_index': weekIndex,
        'username': username,
      },
      formData: {
        'access_token': accessToken,
        'content': content,
        'pull_request_ids': pullRequestIds,
        'issue_ids': issueIds,
        'event_ids': eventIds,
      },
    });
  }

  /**
   * 获取周报可关联 issue 和 pull request
   * 获取周报可关联 issue 和 pull request
   * @returns WeekReportRelation 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsRelations({
    enterpriseId,
    year,
    weekIndex,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 周报所属年
     */
    year: number,
    /**
     * 周报所属周
     */
    weekIndex: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<WeekReportRelation> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/relations',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'year': year,
        'week_index': weekIndex,
      },
    });
  }

  /**
   * 周报可关联动态列表
   * 周报可关联动态列表
   * @returns Event 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsRelateEvents({
    enterpriseId,
    year,
    weekIndex,
    accessToken,
    eventIds,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 周报所属年
     */
    year: number,
    /**
     * 周报所属周
     */
    weekIndex: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 一关联的动态 ids，用逗号分隔，如：1,2,3
     */
    eventIds?: number,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<Event> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/relate_events',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'year': year,
        'week_index': weekIndex,
        'event_ids': eventIds,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 预览周报
   * 预览周报
   * @returns WeekReportPreview 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdWeekReportsPreviewData({
    enterpriseId,
    content,
    accessToken,
    eventIds,
    issueIds,
    pullRequestIds,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 需要预览的周报内容
     */
    content: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 关联动态 ids，以逗号分隔，如：3241,2324
     */
    eventIds?: string,
    /**
     * 关联 issue ids，以逗号分隔，如：3241,2324
     */
    issueIds?: string,
    /**
     * 关联 pull request ids，以逗号分隔，如：3241,2324
     */
    pullRequestIds?: string,
  }): CancelablePromise<WeekReportPreview> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/week_reports/preview_data',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'content': content,
        'event_ids': eventIds,
        'issue_ids': issueIds,
        'pull_request_ids': pullRequestIds,
      },
    });
  }

  /**
   * 获取企业存在周报的年份
   * 获取企业存在周报的年份
   * @returns any 获取企业存在周报的年份
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsYears({
    enterpriseId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/years',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 获取企业某年存在周报的周
   * 获取企业某年存在周报的周
   * @returns any 获取企业某年存在周报的周
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsYearWeeks({
    enterpriseId,
    year,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 周报所属年
     */
    year: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<any> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/{year}/weeks',
      path: {
        'enterprise_id': enterpriseId,
        'year': year,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 周报模版列表
   * 周报模版列表
   * @returns WeekReportTemplate 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsTemplates({
    enterpriseId,
    accessToken,
    page = 1,
    perPage,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 当前的页码
     */
    page?: number,
    /**
     * 每页的数量，最大为 100
     */
    perPage?: number,
  }): CancelablePromise<WeekReportTemplate> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/templates',
      path: {
        'enterprise_id': enterpriseId,
      },
      query: {
        'access_token': accessToken,
        'page': page,
        'per_page': perPage,
      },
    });
  }

  /**
   * 新增周报模版
   * 新增周报模版
   * @returns WeekReportTemplate 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdWeekReportsTemplates({
    enterpriseId,
    name,
    content,
    accessToken,
    isDefault,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 模版名称
     */
    name: string,
    /**
     * 模版内容
     */
    content: string,
    /**
     * 用户授权��
     */
    accessToken?: string,
    /**
     * 是否设为默认模版。0(否) 或 1(是)，默认 0(否)
     */
    isDefault?: number,
  }): CancelablePromise<WeekReportTemplate> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/week_reports/templates',
      path: {
        'enterprise_id': enterpriseId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'content': content,
        'is_default': isDefault,
      },
    });
  }

  /**
   * 查看周报模版详情
   * 查看周报模版详情
   * @returns WeekReportTemplate 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsTemplatesTemplateId({
    enterpriseId,
    templateId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 模版 ID
     */
    templateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<WeekReportTemplate> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/templates/{template_id}',
      path: {
        'enterprise_id': enterpriseId,
        'template_id': templateId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 编辑周报模版
   * 编辑周报模版
   * @returns WeekReportTemplate 返回格式
   * @throws ApiError
   */
  public putEnterpriseIdWeekReportsTemplatesTemplateId({
    enterpriseId,
    templateId,
    accessToken,
    name,
    content,
    isDefault,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 模版 ID
     */
    templateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
    /**
     * 模版名称
     */
    name?: string,
    /**
     * 模版内容
     */
    content?: string,
    /**
     * 是否设为默认模版。0(否) 或 1(是)，默认 0(否)
     */
    isDefault?: number,
  }): CancelablePromise<WeekReportTemplate> {
    return this.httpRequest.request({
      method: 'PUT',
      url: '/{enterprise_id}/week_reports/templates/{template_id}',
      path: {
        'enterprise_id': enterpriseId,
        'template_id': templateId,
      },
      formData: {
        'access_token': accessToken,
        'name': name,
        'content': content,
        'is_default': isDefault,
      },
    });
  }

  /**
   * 删除周报模版
   * 删除周报模版
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdWeekReportsTemplatesTemplateId({
    enterpriseId,
    templateId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 模版 ID
     */
    templateId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/week_reports/templates/{template_id}',
      path: {
        'enterprise_id': enterpriseId,
        'template_id': templateId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 某个周报评论列表
   * 某个周报评论列表
   * @returns WeekReportNote 返回格式
   * @throws ApiError
   */
  public getEnterpriseIdWeekReportsReportIdComments({
    enterpriseId,
    reportId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 周报的 ID
     */
    reportId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<WeekReportNote> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/{enterprise_id}/week_reports/{report_id}/comments',
      path: {
        'enterprise_id': enterpriseId,
        'report_id': reportId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

  /**
   * 评论周���
   * 评论周报
   * @returns WeekReportNote 返回格式
   * @throws ApiError
   */
  public postEnterpriseIdWeekReportsReportIdComment({
    enterpriseId,
    reportId,
    body,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 周报的 ID
     */
    reportId: number,
    /**
     * 评论的内容
     */
    body: string,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<WeekReportNote> {
    return this.httpRequest.request({
      method: 'POST',
      url: '/{enterprise_id}/week_reports/{report_id}/comment',
      path: {
        'enterprise_id': enterpriseId,
        'report_id': reportId,
      },
      formData: {
        'access_token': accessToken,
        'body': body,
      },
    });
  }

  /**
   * 删除周报某个评论
   * 删除周报某个评论
   * @returns void
   * @throws ApiError
   */
  public deleteEnterpriseIdWeekReportsReportIdCommentCommentId({
    enterpriseId,
    reportId,
    commentId,
    accessToken,
  }: {
    /**
     * 企业 id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
     */
    enterpriseId: number,
    /**
     * 周报的 ID
     */
    reportId: number,
    /**
     * 评论 ID
     */
    commentId: number,
    /**
     * 用户授权码
     */
    accessToken?: string,
  }): CancelablePromise<void> {
    return this.httpRequest.request({
      method: 'DELETE',
      url: '/{enterprise_id}/week_reports/{report_id}/comment/{comment_id}',
      path: {
        'enterprise_id': enterpriseId,
        'report_id': reportId,
        'comment_id': commentId,
      },
      query: {
        'access_token': accessToken,
      },
    });
  }

}
