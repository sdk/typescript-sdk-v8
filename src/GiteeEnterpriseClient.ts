/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BaseHttpRequest } from './core/BaseHttpRequest';
import type { OpenAPIConfig } from './core/OpenAPI';
import { AxiosHttpRequest } from './core/AxiosHttpRequest';

import { AdminLogsStatisticClient } from './services/AdminLogsStatisticClient';
import { CustomizeIssueClient } from './services/CustomizeIssueClient';
import { DeployKeyClient } from './services/DeployKeyClient';
import { DocNodesClient } from './services/DocNodesClient';
import { DocsClient } from './services/DocsClient';
import { EditorsClient } from './services/EditorsClient';
import { EnterpriseRolesClient } from './services/EnterpriseRolesClient';
import { EnterprisesClient } from './services/EnterprisesClient';
import { GitDataClient } from './services/GitDataClient';
import { GroupsClient } from './services/GroupsClient';
import { IssuesClient } from './services/IssuesClient';
import { IssueStatesClient } from './services/IssueStatesClient';
import { IssueTypesClient } from './services/IssueTypesClient';
import { KooderClient } from './services/KooderClient';
import { LabelsClient } from './services/LabelsClient';
import { MemberApplicationsClient } from './services/MemberApplicationsClient';
import { MembersClient } from './services/MembersClient';
import { MilestonesClient } from './services/MilestonesClient';
import { ProgramsClient } from './services/ProgramsClient';
import { ProjectAdminClient } from './services/ProjectAdminClient';
import { ProjectBranchClient } from './services/ProjectBranchClient';
import { ProjectCommitsClient } from './services/ProjectCommitsClient';
import { ProjectEnvClient } from './services/ProjectEnvClient';
import { ProjectMembersClient } from './services/ProjectMembersClient';
import { ProjectTagsClient } from './services/ProjectTagsClient';
import { PullRequestsClient } from './services/PullRequestsClient';
import { ReactionClient } from './services/ReactionClient';
import { StatisticsClient } from './services/StatisticsClient';
import { TestCasesClient } from './services/TestCasesClient';
import { TestExaminationClient } from './services/TestExaminationClient';
import { TestPlanClient } from './services/TestPlanClient';
import { UsersClient } from './services/UsersClient';
import { WeekReportsClient } from './services/WeekReportsClient';
import { WorkflowClient } from './services/WorkflowClient';
import { WorkloadsClient } from './services/WorkloadsClient';

type HttpRequestConstructor = new (config: OpenAPIConfig) => BaseHttpRequest;

export class GiteeEnterpriseClient {

  public readonly adminLogsStatistic: AdminLogsStatisticClient;
  public readonly customizeIssue: CustomizeIssueClient;
  public readonly deployKey: DeployKeyClient;
  public readonly docNodes: DocNodesClient;
  public readonly docs: DocsClient;
  public readonly editors: EditorsClient;
  public readonly enterpriseRoles: EnterpriseRolesClient;
  public readonly enterprises: EnterprisesClient;
  public readonly gitData: GitDataClient;
  public readonly groups: GroupsClient;
  public readonly issues: IssuesClient;
  public readonly issueStates: IssueStatesClient;
  public readonly issueTypes: IssueTypesClient;
  public readonly kooder: KooderClient;
  public readonly labels: LabelsClient;
  public readonly memberApplications: MemberApplicationsClient;
  public readonly members: MembersClient;
  public readonly milestones: MilestonesClient;
  public readonly programs: ProgramsClient;
  public readonly projectAdmin: ProjectAdminClient;
  public readonly projectBranch: ProjectBranchClient;
  public readonly projectCommits: ProjectCommitsClient;
  public readonly projectEnv: ProjectEnvClient;
  public readonly projectMembers: ProjectMembersClient;
  public readonly projectTags: ProjectTagsClient;
  public readonly pullRequests: PullRequestsClient;
  public readonly reaction: ReactionClient;
  public readonly statistics: StatisticsClient;
  public readonly testCases: TestCasesClient;
  public readonly testExamination: TestExaminationClient;
  public readonly testPlan: TestPlanClient;
  public readonly users: UsersClient;
  public readonly weekReports: WeekReportsClient;
  public readonly workflow: WorkflowClient;
  public readonly workloads: WorkloadsClient;

  public readonly request: BaseHttpRequest;

  constructor(config?: Partial<OpenAPIConfig>, HttpRequest: HttpRequestConstructor = AxiosHttpRequest) {
    this.request = new HttpRequest({
      BASE: config?.BASE ?? 'http://api.gitee.com/enterprises',
      VERSION: config?.VERSION ?? '0.1.335',
      WITH_CREDENTIALS: config?.WITH_CREDENTIALS ?? false,
      CREDENTIALS: config?.CREDENTIALS ?? 'include',
      TOKEN: config?.TOKEN,
      USERNAME: config?.USERNAME,
      PASSWORD: config?.PASSWORD,
      HEADERS: config?.HEADERS,
      ENCODE_PATH: config?.ENCODE_PATH,
    });

    this.adminLogsStatistic = new AdminLogsStatisticClient(this.request);
    this.customizeIssue = new CustomizeIssueClient(this.request);
    this.deployKey = new DeployKeyClient(this.request);
    this.docNodes = new DocNodesClient(this.request);
    this.docs = new DocsClient(this.request);
    this.editors = new EditorsClient(this.request);
    this.enterpriseRoles = new EnterpriseRolesClient(this.request);
    this.enterprises = new EnterprisesClient(this.request);
    this.gitData = new GitDataClient(this.request);
    this.groups = new GroupsClient(this.request);
    this.issues = new IssuesClient(this.request);
    this.issueStates = new IssueStatesClient(this.request);
    this.issueTypes = new IssueTypesClient(this.request);
    this.kooder = new KooderClient(this.request);
    this.labels = new LabelsClient(this.request);
    this.memberApplications = new MemberApplicationsClient(this.request);
    this.members = new MembersClient(this.request);
    this.milestones = new MilestonesClient(this.request);
    this.programs = new ProgramsClient(this.request);
    this.projectAdmin = new ProjectAdminClient(this.request);
    this.projectBranch = new ProjectBranchClient(this.request);
    this.projectCommits = new ProjectCommitsClient(this.request);
    this.projectEnv = new ProjectEnvClient(this.request);
    this.projectMembers = new ProjectMembersClient(this.request);
    this.projectTags = new ProjectTagsClient(this.request);
    this.pullRequests = new PullRequestsClient(this.request);
    this.reaction = new ReactionClient(this.request);
    this.statistics = new StatisticsClient(this.request);
    this.testCases = new TestCasesClient(this.request);
    this.testExamination = new TestExaminationClient(this.request);
    this.testPlan = new TestPlanClient(this.request);
    this.users = new UsersClient(this.request);
    this.weekReports = new WeekReportsClient(this.request);
    this.workflow = new WorkflowClient(this.request);
    this.workloads = new WorkloadsClient(this.request);
  }
}

