/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 修改项目组件
 */
export type ProgramComponent = {
  /**
   * 项目组件标识符
   */
  ident?: string;
  /**
   * 是否开启
   */
  enabled?: boolean;
  /**
   * 位置值
   */
  position?: number;
  /**
   * 是否允许显示 (角色控制)
   */
  visible?: boolean;
  /**
   * 权限控制条件，0 为属于，1 为不属于 (未设置时为 0，roles 为 nil)
   */
  condition?: number;
  /**
   * 角色列表
   */
  roles?: any[];
};

