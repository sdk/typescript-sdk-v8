/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取任务的操作日志��表
 */
export type IssueOperateLog = {
  id?: number;
  /**
   * 图标
   */
  icon?: string;
  /**
   * 内容
   */
  content?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 操作者
   */
  user?: UserWithRemark;
  /**
   * 改变前的值
   */
  before_change?: string;
  /**
   * 改变后的值
   */
  after_change?: string;
  /**
   * 日志通过对象类型
   */
  through_type?: string;
  /**
   * 日志通过对象类型
   */
  through?: Object;
};

