/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取授权用户对 PR 的权限
 */
export type PullRequestAuth = {
  /**
   * 是否可查看此 PR
   */
  read?: boolean;
  /**
   * 是否可评论此 PR
   */
  create_note?: boolean;
  /**
   * 是否可更新 PR 的信息
   */
  update?: boolean;
  /**
   * PR 当前的状态是否处于可合并
   */
  merge_status_check?: boolean;
  /**
   * 授权用户是否可以让此 PR 审查通过
   */
  can_check?: boolean;
  /**
   * 授权用户是否可以让此 PR 测试通过
   */
  can_test?: boolean;
  /**
   * 授权用户是否有权限可以合并此 PR
   */
  can_merge?: boolean;
  /**
   * 授权用户是否可把 PR 的审查状态强制设置为已通过
   */
  force_checked?: boolean;
  /**
   * 授权用户是否可把 PR 的测试状态强制设置为已通过
   */
  force_tested?: boolean;
};

