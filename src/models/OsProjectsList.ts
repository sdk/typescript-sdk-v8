/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Array } from './Array';
import type { Integer } from './Integer';
import type { OsProject } from './OsProject';
import type { Timestamp } from './Timestamp';

/**
 * 获取企业开源仓库详情列表
 */
export type OsProjectsList = {
  /**
   * 完成任务数
   */
  close_issue_count?: Array;
  /**
   * 代码行数
   */
  code_line_count?: Array;
  /**
   * 提交数
   */
  commit_count?: Array;
  /**
   * 创建任务数
   */
  create_issue_count?: Array;
  /**
   * 创建 PR 数
   */
  create_pr_count?: Array;
  /**
   * 日期列表
   */
  date_list?: Array;
  /**
   * fork 数量
   */
  fork_count?: Array;
  /**
   * 合并 PR 数
   */
  merge_pr_count?: Array;
  /**
   * 仓库 id 列表
   */
  project_ids?: Array;
  projects?: OsProject;
  /**
   * star 数
   */
  stars_count?: Array;
  /**
   * 起始日期
   */
  start_date?: Timestamp;
  /**
   * 终止日期
   */
  end_date?: Timestamp;
};

