/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Label } from './Label';
import type { PrAssign } from './PrAssign';
import type { Project } from './Project';
import type { Timestamp } from './Timestamp';
import type { User } from './User';

/**
 * 获取企业下的 Pull Request 列表
 */
export type PullRequest = {
  /**
   * PR 的 id
   */
  id?: number;
  /**
   * 仓库内唯一的 PR id 标识符
   */
  iid?: number;
  /**
   * PR 的标题
   */
  title?: string;
  /**
   * 仓库 id
   */
  project_id?: number;
  /**
   * PR 的状态 (opened: 开启; reopened: 关闭后重开; closed: 关闭; merged: 已合并;)
   */
  state?: string;
  /**
   * PR 草稿状态：草稿 - true, 非草稿 - false
   */
  draft?: boolean;
  /**
   * PR 的审查状态 (0: 不需要审查; 1: ���审查; 2: 审查已全部通过;)
   */
  check_state?: number;
  /**
   * PR 的测试状态 (0: 不需要测试; 1: 待测试; 2: 测试已全部通过;)
   */
  test_state?: number;
  /**
   * PR 的优先级。
   */
  priority?: string;
  /**
   * PR 优先级标签名称
   */
  priority_human?: string;
  /**
   * 是否轻量级 PR
   */
  lightweight?: boolean;
  /**
   * PR 的创建时间
   */
  created_at?: Timestamp;
  /**
   * PR 的更新时间
   */
  updated_at?: Timestamp;
  /**
   * PR 的合并时间
   */
  merged_at?: Timestamp;
  /**
   * PR 的标签列表
   */
  labels?: Label;
  /**
   * PR 创建者
   */
  author?: User;
  /**
   * 是否存在冲突
   */
  conflict?: boolean;
  /**
   * 所属仓库
   */
  project?: Project;
  /**
   * 源分支
   */
  source_branch?: any;
  /**
   * 目标分支
   */
  target_branch?: any;
  /**
   * 是否可合并
   */
  can_merge?: string;
  /**
   * 审查人员
   */
  assignees?: PrAssign;
  /**
   * 最少审查人数
   */
  pr_assign_num?: number;
  /**
   * 测试人员
   */
  testers?: PrAssign;
  /**
   * 合并 PR 后关闭关联的任务
   */
  close_related_issue?: number;
  /**
   * 合并 PR 后删除关联分支
   */
  prune_branch?: number;
  /**
   * 最少测试人数
   */
  pr_test_num?: number;
  /**
   * 最后一次 gitee scan 扫描结果
   */
  latest_scan_task?: any;
  /**
   * 所属仓库 GiteeGo 服务是否可用
   */
  gitee_go_enabled?: boolean;
  /**
   * scan 任务状态：0 等待中、1 执行中、2 已取消、3 成功、4 失败、5 构建中、6 超时
   */
  scan_status?: number;
  /**
   * scan 返回门禁相关的描述
   */
  scan_status_desc?: number;
  /**
   * 质量门禁标志：1 通过 2 不通过
   */
  scan_flag?: number;
  /**
   * 评审策略
   */
  review_strategy?: string;
  /**
   * pr 可以合入的最少审查得分
   */
  review_score?: number;
  /**
   * pr 当前审查得分
   */
  current_review_score?: number;
};

