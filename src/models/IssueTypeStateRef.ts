/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 获取当前任务可流转的下一状态
 */
export type IssueTypeStateRef = {
  /**
   * 任务状态 ID
   */
  id?: number;
  /**
   * 任务状态的名称
   */
  title?: string;
  /**
   * 任务状态的属性
   */
  state?: string;
  /**
   * 任务状态的颜色
   */
  color?: string;
  /**
   * 任务状态的 Icon
   */
  icon?: string;
  /**
   * 任务状态创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务状态更新时间
   */
  updated_at?: Timestamp;
};

