/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

export type EnterpriseProject = {
  /**
   * 创建日期
   */
  date?: Timestamp;
  /**
   * 仓库 id
   */
  id?: number;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 仓库公开与否
   */
  public?: number;
};

