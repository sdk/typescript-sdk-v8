/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EnterpriseRoleBase = {
  /**
   * 角色 id
   */
  id?: number;
  /**
   * 角色名称
   */
  name?: string;
  /**
   * 角色备注信息
   */
  description?: string;
  /**
   * 是否系统默认角色
   */
  is_system_default?: boolean;
  /**
   * 角色类型标识符
   */
  ident?: string;
  /**
   * 是否企业默认角色
   */
  is_default?: boolean;
};

