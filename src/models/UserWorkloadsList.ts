/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserWithRemark } from './UserWithRemark';

/**
 * 查询成员工时明细
 */
export type UserWorkloadsList = {
  /**
   * 用户
   */
  user?: UserWithRemark;
  /**
   * 工作项 ID
   */
  issue_id?: number;
  /**
   * 工作项 ident
   */
  issue_ident?: string;
  /**
   * 工作项类型属性
   */
  issue_type_category?: string;
  /**
   * 工作项类型 ID
   */
  issue_type_id?: number;
  /**
   * 工作项类型名称
   */
  issue_type_name?: string;
  /**
   * 工作项标题
   */
  issue_title?: string;
  /**
   * 工作项创建时间
   */
  issue_created_at?: string;
  /**
   * 工作项完成时间
   */
  issue_finished_at?: string;
  /**
   * 工时 ID
   */
  workload_id?: number;
  /**
   * 工作项预计工时
   */
  issue_estimated_duration?: number;
  /**
   * 登记工时
   */
  workload_registered_duration?: number;
  /**
   * 登记日期
   */
  workload_registered_date?: string;
  /**
   * 登记内容
   */
  workload_registered_description?: string;
  /**
   * 项目 ID
   */
  program_id?: number;
  /**
   * 项目名称
   */
  program_name?: string;
  /**
   * 迭代 ID
   */
  scrum_sprint_id?: number;
  /**
   * 迭代名称
   */
  scrum_sprint_name?: string;
  /**
   * 版本 ID
   */
  scrum_version_id?: number;
  /**
   * 版本号
   */
  scrum_version_number?: string;
};

