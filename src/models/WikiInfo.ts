/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DocNode } from './DocNode';
import type { Project } from './Project';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

export type WikiInfo = {
  /**
   * 文档 id
   */
  id?: number;
  /**
   * 文档名称
   */
  name?: string;
  /**
   * 文档路径
   */
  path?: string;
  /**
   * 文档创建者
   */
  creator?: UserWithRemark;
  /**
   * 文档归属的仓库
   */
  project?: Project;
  /**
   * 文档节点
   */
  doc_node?: DocNode;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

