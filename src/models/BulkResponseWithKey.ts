/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 批量处理申请记录
 */
export type BulkResponseWithKey = {
  /**
   * 消息数组模版 ['key', 'message']
   */
  fields?: any[];
  /**
   * 错误消息数组
   */
  errors?: any[];
  /**
   * 成功消息数组
   */
  successes?: any[];
};

