/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProgramExtraField = {
  /**
   * 自定义字段 id
   */
  program_field_id?: number;
  /**
   * 自定义字段的值
   */
  value?: string;
};

