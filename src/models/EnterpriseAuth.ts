/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取授权用户在企业拥有的权限
 */
export type EnterpriseAuth = {
  /**
   * 是否可查看成员列表
   */
  read_member?: boolean;
  /**
   * 是否可添加企业成员
   */
  add_member?: boolean;
  /**
   * 是否可查看授权用户参与的团队列表
   */
  read_group?: boolean;
  /**
   * 是否可新增团队
   */
  add_group?: boolean;
  /**
   * 是否可查看授权用户参与的任务列表
   */
  read_issue?: boolean;
  /**
   * 是否可创建任务
   */
  create_issue?: boolean;
  /**
   * 是否可见用例设置
   */
  read_program_config?: boolean;
  /**
   * 是否可配置用例设置
   */
  setting_program_config?: boolean;
  /**
   * 是否可查看授权用户参与的项目列表
   */
  read_program?: boolean;
  /**
   * 是否可创建项目
   */
  create_program?: boolean;
  /**
   * 是否可查看授权用户参与的仓库列表
   */
  read_project?: boolean;
  /**
   * 是否可读取周报
   */
  read_week_report?: boolean;
  /**
   * 是否可读取任务类型
   */
  read_issue_type?: boolean;
  /**
   * 是否可管理任务类型
   */
  setting_issue_type?: boolean;
  /**
   * 是否可读取标签
   */
  read_label?: boolean;
  /**
   * 是否可管理标签
   */
  setting_label?: boolean;
  /**
   * 是否可管理企业
   */
  admin_enterprise?: boolean;
};

