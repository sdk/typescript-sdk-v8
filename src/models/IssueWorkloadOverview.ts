/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Number } from './Number';

export type IssueWorkloadOverview = {
  /**
   * 预预估工时
   */
  estimated_duration?: Number;
  /**
   * 剩余工时
   */
  left_duration?: Number;
  /**
   * 登记工时
   */
  registered_duration?: Number;
};

