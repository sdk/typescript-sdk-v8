/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Namespace = {
  /**
   * 空间 ID
   */
  id?: number;
  /**
   * 空间类型 (Enterprise: 企业; Group: 组织; ProjectGroup: 仓库组; 其它情况：个人)
   */
  type?: string;
  /**
   * 空间名称
   */
  name?: string;
  /**
   * 空间路径
   */
  path?: string;
  /**
   * 完整空���名称
   */
  complete_name?: string;
  /**
   * 完整空间路径
   */
  complete_path?: string;
  /**
   * 描述
   */
  description?: string;
};

