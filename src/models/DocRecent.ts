/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';
import type { WikiInfo } from './WikiInfo';

/**
 * 获取最近编辑的文件
 */
export type DocRecent = {
  /**
   * 文件 id
   */
  id?: number;
  /**
   * 文件名称
   */
  name?: string;
  /**
   * 父级 id
   */
  parent_id?: number;
  /**
   * 最后编辑者
   */
  editor?: UserWithRemark;
  /**
   * 正在编辑的用户
   */
  editing_user?: UserWithRemark;
  /**
   * 创建者
   */
  creator?: UserWithRemark;
  /**
   * 文件归属的文档信息
   */
  wiki_info?: WikiInfo;
  /**
   * 最后更新时间
   */
  last_edit_time?: Timestamp;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

