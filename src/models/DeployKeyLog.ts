/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DeployKey } from './DeployKey';
import type { Project } from './Project';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 企业部署公钥管理日志
 */
export type DeployKeyLog = {
  /**
   * ID
   */
  id?: number;
  /**
   * 用户 ID
   */
  user_id?: number;
  /**
   * 操作用户
   */
  user?: UserWithRemark;
  /**
   * ip
   */
  ip?: string;
  /**
   * 目标操作对象 ID
   */
  target_id?: number;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 操作对象名称
   */
  title?: string;
  /**
   * 操作对象类型
   */
  type?: string;
  /**
   * 操作
   */
  operating?: string;
  /**
   * 操作对象：仓库
   */
  target_project?: Project;
  /**
   * 仓库名称
   */
  target_project_name?: string;
  /**
   * 公钥名称
   */
  target_deploy_key_title?: string;
  /**
   * 操作对象：部署公钥
   */
  target_deploy_key?: DeployKey;
};

