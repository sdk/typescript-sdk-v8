/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取仓库角色
 */
export type ProjectRole = {
  /**
   * 角色
   */
  role?: string;
  /**
   * 角色名称
   */
  access?: string;
  /**
   * 角色等级
   */
  role_level?: string;
};

