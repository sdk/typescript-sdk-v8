/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 查看历史版本
 */
export type FileVersion = {
  /**
   * 版本 id
   */
  id?: string;
  /**
   * 提交信息
   */
  message?: string;
  /**
   * 提交日期
   */
  author?: UserWithRemark;
  /**
   * 提交日期
   */
  created_at?: Timestamp;
};

