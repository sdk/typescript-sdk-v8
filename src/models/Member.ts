/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Array } from './Array';
import type { EnterpriseRoleBase } from './EnterpriseRoleBase';
import type { String } from './String';
import type { Timestamp } from './Timestamp';
import type { User } from './User';

/**
 * 修改仓库成员权限
 */
export type Member = {
  /**
   * 成员 id
   */
  id?: number;
  /**
   * 成员个性地址
   */
  username?: string;
  /**
   * 成员名称
   */
  name?: string;
  /**
   * 成员在企业的备注姓名
   */
  remark?: string;
  /**
   * 成员备注或名称拼音
   */
  pinyin?: string;
  /**
   * 职位
   */
  occupation?: string;
  /**
   * 是否被锁定
   */
  is_block?: boolean;
  /**
   * 成员被锁定的原因
   */
  block_message?: string;
  /**
   * 手机号码
   */
  phone?: string;
  /**
   * 邮箱
   */
  email?: string;
  /**
   * 用户的基础信息
   */
  user?: User;
  /**
   * 成员在企业的角色
   */
  enterprise_role?: EnterpriseRoleBase;
  /**
   * 加入企业的时间
   */
  created_at?: Timestamp;
  /**
   * 成员新版问卷反馈时间
   */
  feedback_time?: Timestamp;
  /**
   * 成员是否填写过
   */
  is_feedback?: boolean;
  /**
   * 是否完成引导
   */
  is_guided?: boolean;
  /**
   * 是否关闭了新手引导
   */
  user_guide_closed?: boolean;
  /**
   * 新手引导的步骤指示
   */
  user_guide_steps_finished?: Array;
};

