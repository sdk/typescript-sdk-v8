/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Event } from './Event';
import type { IssueMain } from './IssueMain';
import type { PullRequest } from './PullRequest';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 新建/编辑周报
 */
export type WeekReportDetail = {
  /**
   * 周报的 id
   */
  id?: number;
  /**
   * 周报所属年
   */
  year?: number;
  /**
   * 周报所属月份
   */
  month?: number;
  /**
   * 处于本年的第几周
   */
  week_index?: number;
  /**
   * 起始日期
   */
  begin_day?: Timestamp;
  /**
   * 结束���期
   */
  end_day?: Timestamp;
  /**
   * 创建日期
   */
  created_at?: Timestamp;
  /**
   * 更新日期
   */
  updated_at?: Timestamp;
  /**
   * 周报所属用户
   */
  user?: UserWithRemark;
  /**
   * 周报内容
   */
  content?: string;
  /**
   * 周报内容
   */
  content_html?: string;
  /**
   * 关联 issues
   */
  issues?: IssueMain;
  /**
   * 关联 pull request
   */
  pull_requests?: PullRequest;
  /**
   * 关联动态
   */
  events?: Event;
};

