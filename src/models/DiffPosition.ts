/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取 Pull Request 评论引用的代码片段
 */
export type DiffPosition = {
  /**
   * 评论引用的代码块 id
   */
  diff_position_id?: number;
  /**
   * diff 统计
   */
  statistic?: string;
  /**
   * diff head
   */
  head?: string;
  /**
   * diff 内容
   */
  content?: string;
  /**
   * 解决人
   */
  resolved_user?: string;
  /**
   * 解决人
   */
  author?: string;
  /**
   * 评论解决状态
   */
  resolved_state?: boolean;
  /**
   * 最近变动时间
   */
  updated_at?: string;
};

