/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取当前企业的配额信息
 */
export type EnterpriseQuota = {
  /**
   * 当前企业的团队总数
   */
  groups_count?: number;
  /**
   * 当前企业的成员总数（不包含观察者）
   */
  members_count?: number;
  /**
   * 当前企业的观察者总数
   */
  viewers_count?: number;
  /**
   * 当前企业的仓库总数
   */
  projects_count?: number;
  /**
   * 当前企业的项目总数
   */
  programs_count?: number;
  /**
   * 在企业内能创建的仓库数的总限制
   */
  project_quota?: number;
  /**
   * 当前企业套餐的仓库容量大小配额。单位：G
   */
  space_quota?: number;
  /**
   * 当前企业套餐的成员数配额
   */
  member_quota?: number;
  /**
   * 当前企业套餐的观察者数配额
   */
  viewer_quota?: number;
  /**
   * 当前企业套餐的附件配额。单位：G
   */
  attach_file_quota?: number;
  /**
   * 当前企业套餐的 LFS 套餐配额。单位：G
   */
  lfs_quota?: number;
  /**
   * 企业附件已占用的容量。单位：兆
   */
  attach_file_used_space_quota?: number;
  /**
   * 企业 LFS 已使用的容量。单位：兆
   */
  lfs_space_used_quota?: number;
  /**
   * 企业仓库已占用的容量。单位：兆
   */
  project_used_space_quota?: number;
  /**
   * 当前企业套餐的单文件大小配额。单位：兆
   */
  single_file_quota?: number;
  /**
   * 当前企业单仓库容量。单位：兆
   */
  single_project_quota?: number;
  /**
   * 知识库上传单文件大小配额。单位：兆
   */
  doc_single_file_quota?: number;
};

