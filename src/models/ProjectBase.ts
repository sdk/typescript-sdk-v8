/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

export type ProjectBase = {
  /**
   * 仓库 ID
   */
  id?: number;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 仓库路径
   */
  path?: string;
  /**
   * namespace/path
   */
  path_with_namespace?: string;
  /**
   * 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
   */
  public?: number;
  /**
   * 仓库创建时间
   */
  created_at?: Timestamp;
  /**
   * 仓库更新时间
   */
  updated_at?: Timestamp;
  /**
   * 是否为 GVP 仓库
   */
  is_gvp?: boolean;
  /**
   * 是否允许 fork
   */
  fork_enabled?: boolean;
  /**
   * namespace_name/path
   */
  name_with_namespace?: string;
};

