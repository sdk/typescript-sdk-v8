/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 获取仓库的操作权限
 */
export type UsersProjects = {
  /**
   * 用户 id
   */
  id?: number;
  /**
   * 用户个性地址
   */
  username?: string;
  /**
   * 用户名称
   */
  name?: string;
  /**
   * 用户头像
   */
  avatar_url?: string;
  /**
   * 账号是否可用 (active: 可用 blocked: 已被系统屏蔽)
   */
  state?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 最近活跃时间
   */
  updated_at?: Timestamp;
  /**
   * 用户代码风格
   */
  user_color?: number;
  /**
   * 用户来源
   */
  authen_way?: string;
  /**
   * 邮箱
   */
  email?: string;
  /**
   * 是否需要重置密码
   */
  need_change_password?: boolean;
  /**
   * 用户对仓库的权限 (报告者:15; 观察者:25; 开发者:30; 管理员：40)
   */
  access_level?: number;
  /**
   * 仓库角色名称
   */
  access_name?: string;
  /**
   * 加入仓库的时间
   */
  join_at?: Timestamp;
  /**
   * 用户在企业的备注名
   */
  remark?: string;
  /**
   * 成员备注或名称拼音
   */
  pinyin?: string;
};

