/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CommitSignature = {
  /**
   * 是否验证
   */
  is_verified?: boolean;
  /**
   * 状态文案
   */
  title?: string;
  /**
   * GPG key ID
   */
  gpg_key_primary_keyid?: string;
};

