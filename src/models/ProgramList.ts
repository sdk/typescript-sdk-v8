/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ProgramComponent } from './ProgramComponent';
import type { ProgramExtraField } from './ProgramExtraField';
import type { Timestamp } from './Timestamp';
import type { User } from './User';

/**
 * 获取用户最近浏览的项目集合
 */
export type ProgramList = {
  /**
   * 项目 id
   */
  id?: number;
  /**
   * 项目编号
   */
  ident?: string;
  /**
   * 项目名称
   */
  name?: string;
  /**
   * 项目描述
   */
  description?: string;
  /**
   * 项目状态（0:开始 1:暂停 2:关闭）
   */
  status?: number;
  /**
   * 是否外包项目
   */
  outsourced?: boolean;
  /**
   * 项目类型（内部、外包）
   */
  type?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 颜色
   */
  color?: string;
  /**
   * 项目类型
   */
  category?: string;
  /**
   * 项目自定义字段值
   */
  program_extra_fields?: ProgramExtraField;
  /**
   * 负责人
   */
  assignee?: User;
  /**
   * 是否置顶项��
   */
  is_topped?: boolean;
  /**
   * 项目切换类型状态
   */
  change_category_status?: string;
  /**
   * 工作项模式
   */
  issue_module?: string;
  /**
   * 项目组件列表
   */
  components?: Array<ProgramComponent>;
  /**
   * 成员数
   */
  users_count?: number;
  /**
   * 仓库数
   */
  projects_count?: number;
  /**
   * 任务总数
   */
  issues_count?: number;
  /**
   * 已关闭的任务总数
   */
  closed_issues_count?: number;
  /**
   * 是否被当前用户收藏
   */
  star_status?: boolean;
};

