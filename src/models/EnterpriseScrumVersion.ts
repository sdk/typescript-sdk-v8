/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ScrumStage } from './ScrumStage';

export type EnterpriseScrumVersion = {
  /**
   * 版本 ID
   */
  id?: number;
  /**
   * 企业 ID
   */
  enterprise_id?: number;
  /**
   * 项目 ID
   */
  program_id?: number;
  /**
   * 版本号
   */
  number?: string;
  /**
   * 计划发版时间
   */
  plan_released_at?: string;
  /**
   * 实际发版时间
   */
  released_at?: string;
  /**
   * 标题
   */
  title?: string;
  /**
   * 描述
   */
  description?: string;
  /**
   * 状态
   */
  state?: string;
  /**
   * 当前阶段
   */
  current_stage?: ScrumStage;
};

