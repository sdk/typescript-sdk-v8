/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 添加成员到企业 - 邮件邀请
 */
export type EmailInvitation = {
  /**
   * 邮箱
   */
  email?: string;
  /**
   * 邀请状态，true: 成功，false: 失败
   */
  status?: string;
  /**
   * 失败原因
   */
  message?: string;
};

