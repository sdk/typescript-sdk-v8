/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 指派审查、测试人员
 */
export type PrAssigner = {
  /**
   * 审查人员 id 列表
   */
  assigners?: string;
  /**
   * 测试人员 id 列表
   */
  testers?: string;
  /**
   * 可合并的审查人员门槛数
   */
  pr_assign_num?: number;
  /**
   * 可合并的测试人员门槛数
   */
  pr_test_num?: number;
};

