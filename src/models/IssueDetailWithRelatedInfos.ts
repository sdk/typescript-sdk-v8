/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EnterpriseScrumVersion } from './EnterpriseScrumVersion';
import type { Extra } from './Extra';
import type { Issue } from './Issue';
import type { IssueState } from './IssueState';
import type { IssueType } from './IssueType';
import type { Label } from './Label';
import type { Milestone } from './Milestone';
import type { Number } from './Number';
import type { Object } from './Object';
import type { ProgramWithComponents } from './ProgramWithComponents';
import type { Project } from './Project';
import type { ScrumSprint } from './ScrumSprint';
import type { Timestamp } from './Timestamp';
import type { User } from './User';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 更新任务的关联关系
 */
export type IssueDetailWithRelatedInfos = {
  /**
   * 任务 ID
   */
  id?: number;
  /**
   * 根结点 ID
   */
  root_id?: number;
  /**
   * 父任务 ID
   */
  parent_id?: number;
  /**
   * 关联项目 ID
   */
  project_id?: number;
  /**
   * 任务全局唯一标识符
   */
  ident?: string;
  /**
   * 任务标题
   */
  title?: string;
  /**
   * 任务状态 id
   */
  issue_state_id?: number;
  /**
   * 项目 id
   */
  program_id?: number;
  /**
   * 任务状态标识符：open, progressing, closed, rejected
   */
  state?: string;
  /**
   * 评论数量
   */
  comments_count?: number;
  /**
   * 优先级标识符
   */
  priority?: number;
  /**
   * 关联的分支名
   */
  branch?: string;
  /**
   * 优先级中文名称
   */
  priority_human?: string;
  /**
   * 任务负责人
   */
  assignee?: User;
  /**
   * 预计工时。（单位：分钟）
   */
  duration?: number;
  /**
   * 任务创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务更新时间
   */
  updated_at?: Timestamp;
  /**
   * 任务协作者
   */
  collaborators?: Array<UserWithRemark>;
  /**
   * 任务创建者
   */
  author?: UserWithRemark;
  /**
   * 关联的里程碑
   */
  milestone?: Milestone;
  /**
   * 任务状态
   */
  issue_state?: IssueState;
  /**
   * 任务类型
   */
  issue_type?: IssueType;
  /**
   * 任务关联的标签
   */
  labels?: Label;
  /**
   * 任务自定义字段值
   */
  issue_extra?: Extra;
  /**
   * 计划开始时间
   */
  plan_started_at?: Timestamp;
  /**
   * 计划完成时间
   */
  deadline?: Timestamp;
  /**
   * 实际完成时间
   */
  finished_at?: Timestamp;
  /**
   * 实际开始时间
   */
  started_at?: Timestamp;
  /**
   * 是否是私有 Issue
   */
  security_hole?: boolean;
  /**
   * 当前用户是否收藏过此任务
   */
  is_star?: boolean;
  /**
   * 所属看板
   */
  kanban_info?: Object;
  /**
   * 关联迭代
   */
  scrum_sprint?: ScrumSprint;
  /**
   * 任务内容 (markdown 格式)
   */
  description?: string;
  /**
   * 任务内容 (html 格式)
   */
  description_html?: string;
  /**
   * 工作项 JSON 格式内容
   */
  description_json?: string;
  /**
   * 工作项描述文本类型
   */
  description_type?: string;
  /**
   * PC 的任务详情链接
   */
  issue_url?: string;
  /**
   * 关联的项目
   */
  program?: ProgramWithComponents;
  /**
   * 关联的仓库
   */
  project?: Project;
  /**
   * 父级任务
   */
  parent?: Issue;
  /**
   * 操作日志的数量
   */
  operate_logs_count?: number;
  /**
   * 表态
   */
  reactions?: any;
  /**
   * issue 附件 id 列表
   */
  attach_file_ids?: number;
  /**
   * 私有 issue 操作者
   */
  security_hole_operater?: boolean;
  /**
   * 关联版本
   */
  scrum_version?: EnterpriseScrumVersion;
  /**
   * 收藏了该任务的用户列表
   */
  starred_users?: User;
  /**
   * 是否有异步执行的触发器
   */
  trigger_execute_info?: boolean;
  /**
   * 预估工时 (单位小时)
   */
  estimated_duration?: Number;
  /**
   * 登记工时 (单位小时)
   */
  registered_duration?: Number;
  /**
   * 剩余工时 (单位小时)
   */
  left_duration?: Number;
  /**
   * 子工作项是否符合层级关系
   */
  is_inherit_satisfying?: boolean;
  /**
   * 版本号集合
   */
  version?: any[];
  /**
   * 子任务
   */
  children?: Issue;
  /**
   * 子任务数量
   */
  children_count?: number;
  /**
   * 关联关系
   */
  ref_type?: string;
  /**
   * 关联顺序
   */
  direction?: string;
  direction_cn?: string;
};

