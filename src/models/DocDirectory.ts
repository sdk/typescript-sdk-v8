/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Program } from './Program';
import type { Timestamp } from './Timestamp';

/**
 * 获取文件夹
 */
export type DocDirectory = {
  /**
   * id
   */
  id?: number;
  /**
   * 父层级的 id
   */
  parent_id?: number;
  /**
   * 名称
   */
  name?: string;
  /**
   * 权限值
   */
  public?: string;
  /**
   * 权限名称
   */
  public_name?: string;
  /**
   * 关联项目
   */
  program?: Program;
  /**
   * 关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile)
   */
  file_type?: string;
  /**
   * 关联类型的 id
   */
  file_id?: string;
  children?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

