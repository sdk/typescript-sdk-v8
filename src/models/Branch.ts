/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PullRequest } from './PullRequest';

/**
 * 编辑分支
 */
export type Branch = {
  /**
   * 分支名称
   */
  name?: string;
  /**
   * 分支类型
   */
  branch_type?: any;
  /**
   * 是否默认分支
   */
  is_default?: boolean;
  /**
   * 保护分支的操作规则
   */
  protection_rule?: any;
  /**
   * commit 信息
   */
  commit?: any;
  /**
   * 操作相关权限
   */
  operating?: any;
  /**
   * 向默认分支的 pr
   */
  pull_request?: PullRequest;
  /**
   * 分支备注
   */
  remark?: string;
  /**
   * 分支拥有者信息
   */
  owner?: any;
};

