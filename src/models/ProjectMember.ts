/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { TimeStamp } from './TimeStamp';

/**
 * 获取仓库成员和仓库团队成员
 */
export type ProjectMember = {
  /**
   * 用户 id
   */
  id?: number;
  /**
   * 用户名称
   */
  name?: string;
  /**
   * 用户昵称
   */
  nickname?: string;
  /**
   * 用户个性域名
   */
  username?: string;
  /**
   * 跳转类型
   */
  target?: string;
  /**
   * 用户头像链接
   */
  avatar_url?: string;
  /**
   * 用户邮箱
   */
  email?: string;
  /**
   * 是否是企业成员
   */
  is_enterprise_member?: boolean;
  /**
   * 是否是已离职成员
   */
  is_history_member?: boolean;
  /**
   * 用户企业角色名称
   */
  access_level?: string;
  /**
   * 用户企业角色 ident
   */
  access_level_ident?: string;
  /**
   * 是否被锁定
   */
  is_blocked?: boolean;
  /**
   * 是否是自己
   */
  is_myself?: boolean;
  /**
   * 用户在仓库中的级别
   */
  project_access?: string;
  /**
   * 用户在仓库中的级别名称
   */
  project_access_name?: string;
  /**
   * 是否是仓库创建者
   */
  project_creator?: boolean;
  /**
   * 是否是仓库拥有者
   */
  project_owner?: boolean;
  /**
   * 加入时间
   */
  join_at?: TimeStamp;
};

