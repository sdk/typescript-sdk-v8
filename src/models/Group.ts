/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 更新企业团队
 */
export type Group = {
  /**
   * 团队 id
   */
  id?: number;
  /**
   * 团队名称
   */
  name?: string;
  /**
   * 团队路径
   */
  path?: string;
  /**
   * 团队头像
   */
  avatar_url?: string;
  /**
   * 团队描述
   */
  description?: string;
  /**
   * deprecated.团队的类型值。0: 内部 1:公开 2:外包
   */
  group_type?: number;
  /**
   * 团队的类型名称
   */
  group_type_human_name?: string;
  /**
   * 团队的类型值。0: 内部 1:公开 2:外包
   */
  public?: number;
};

