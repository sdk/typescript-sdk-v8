/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 添加仓库成员
 */
export type ProjectMemberAdd = {
  /**
   * 成员 id
   */
  id?: number;
  /**
   * 邮箱
   */
  email?: string;
  /**
   * 成员名称
   */
  name?: string;
  /**
   * 成员命名空间
   */
  username?: string;
  /**
   * 返回信息
   */
  msg?: string;
  /**
   * 是否成功
   */
  success?: boolean;
  /**
   * 是否是注册用户
   */
  no_register?: boolean;
  /**
   * 角色名称
   */
  role_name?: string;
  /**
   * 角色级别
   */
  access?: string;
  /**
   * 角色状态
   */
  state?: string;
};

