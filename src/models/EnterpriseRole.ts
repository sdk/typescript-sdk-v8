/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 获取企业角色列表
 */
export type EnterpriseRole = {
  /**
   * 角色 id
   */
  id?: number;
  /**
   * 角色名称
   */
  name?: string;
  /**
   * 角色备注信息
   */
  description?: string;
  /**
   * 是否系统默认角色
   */
  is_system_default?: boolean;
  /**
   * 角色类型标识符
   */
  ident?: string;
  /**
   * 是否企业默认角色
   */
  is_default?: boolean;
  /**
   * 归属于该角色下的成员数量
   */
  member_count?: number;
  /**
   * 能否设置为默认角色
   */
  can_set_default?: boolean;
  /**
   * 角色创建时间
   */
  created_at?: Timestamp;
  /**
   * 角色更新时间
   */
  updated_at?: Timestamp;
};

