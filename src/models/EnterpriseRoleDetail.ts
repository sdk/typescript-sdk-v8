/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Doc } from './Doc';
import type { GiteegoPipeline } from './GiteegoPipeline';
import type { Issue } from './Issue';
import type { Member } from './Member';
import type { Program } from './Program';
import type { Project } from './Project';
import type { ProjectGroup } from './ProjectGroup';
import type { Statistic } from './Statistic';
import type { TestPlan } from './TestPlan';
import type { TestRepository } from './TestRepository';
import type { Timestamp } from './Timestamp';
import type { WeekReport } from './WeekReport';

/**
 * 更新企业角色
 */
export type EnterpriseRoleDetail = {
  /**
   * 角色 id
   */
  id?: number;
  /**
   * 角色名称
   */
  name?: string;
  /**
   * 角色备注信息
   */
  description?: string;
  /**
   * 是否系统默认角色
   */
  is_system_default?: boolean;
  /**
   * 角色类型标识符
   */
  ident?: string;
  /**
   * 是否企业默认角色
   */
  is_default?: boolean;
  /**
   * 周报的权限属性
   */
  week_report?: WeekReport;
  /**
   * 任务的权限属性
   */
  issue?: Issue;
  /**
   * 项目的权限属性
   */
  program?: Program;
  /**
   * 仓库的权限属���
   */
  project?: Project;
  /**
   * 仓库组的权限属性
   */
  project_group?: ProjectGroup;
  /**
   * 文档的权限属性
   */
  doc?: Doc;
  /**
   * 文档的权限属性
   */
  member?: Member;
  /**
   * 统计的权限属性
   */
  statistic?: Statistic;
  /**
   * 是否是企业管理员角色
   */
  is_admin?: boolean;
  /**
   * 测试用例计划权限属性
   */
  test_plan?: TestPlan;
  /**
   * 测试用例库权限属性
   */
  test_repository?: TestRepository;
  /**
   * Gitee Go 流水线
   */
  giteego_pipeline?: GiteegoPipeline;
  admin_rules?: string;
  /**
   * 能否访问企业
   */
  access?: any;
  /**
   * 角色创建时间
   */
  created_at?: Timestamp;
  /**
   * 角色更新时间
   */
  updated_at?: Timestamp;
};

