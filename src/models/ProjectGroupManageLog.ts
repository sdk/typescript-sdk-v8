/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ProjectGroup } from './ProjectGroup';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 仓库组管理日志
 */
export type ProjectGroupManageLog = {
  /**
   * ID
   */
  id?: number;
  /**
   * 用户 ID
   */
  user_id?: number;
  /**
   * 操作用户
   */
  user?: UserWithRemark;
  /**
   * ip
   */
  ip?: string;
  /**
   * 目标操作对象 ID
   */
  target_id?: number;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 操作原始记录
   */
  title?: string;
  /**
   * 目标仓库组，如果是 null，表示已删除
   */
  target_project_group?: ProjectGroup;
  /**
   * 操作
   */
  operating?: string;
};

