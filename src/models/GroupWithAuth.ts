/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { User } from './User';

/**
 * 获取成员加入的团队列表
 */
export type GroupWithAuth = {
  /**
   * 团队 id
   */
  id?: number;
  /**
   * 团队名称
   */
  name?: string;
  /**
   * 团队路径
   */
  path?: string;
  /**
   * 团队头像
   */
  avatar_url?: string;
  /**
   * 团队描述
   */
  description?: string;
  /**
   * deprecated.团队的类型值。0: 内部 1:公开 2:外包
   */
  group_type?: number;
  /**
   * 团队的类型名称
   */
  group_type_human_name?: string;
  /**
   * 团队的类型值。0: 内部 1:公开 2:外包
   */
  public?: number;
  /**
   * 创建者
   */
  creator?: User;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 能否编辑退出
   */
  can_quit?: boolean;
};

