/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取成员的统计信息
 */
export type UserStatistic = {
  /**
   * 唯一标示
   */
  id?: number;
  /**
   * 对应用户的 ID
   */
  user_id?: number;
  /**
   * 企业 ID
   */
  enterprise_id?: number;
  /**
   * 日期
   */
  date?: string;
  /**
   * 关闭的任务总数
   */
  close_issue_count?: number;
  /**
   * 提交的总数
   */
  commit_count?: number;
  /**
   * 创建的任务数
   */
  create_issue_count?: number;
  /**
   * 创建的 PR 数
   */
  create_pr_count?: number;
  /**
   * 合并的 PR 数
   */
  merge_pr_count?: number;
  /**
   * 新增代码行
   */
  add_code_line?: number;
};

