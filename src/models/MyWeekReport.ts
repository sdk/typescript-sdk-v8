/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 获取未提交周报的用户列表
 */
export type MyWeekReport = {
  /**
   * 周报的 id
   */
  id?: number;
  /**
   * 周报所属年
   */
  year?: number;
  /**
   * 周报所属月份
   */
  month?: number;
  /**
   * 处于本年的第几周
   */
  week_index?: number;
  /**
   * 起始日期
   */
  begin_day?: Timestamp;
  /**
   * 结束日期
   */
  end_day?: Timestamp;
  /**
   * 创建日期
   */
  created_at?: Timestamp;
  /**
   * 更新日期
   */
  updated_at?: Timestamp;
  /**
   * 周报内容
   */
  content?: string;
  /**
   * 周报 (html 格式)
   */
  content_html?: string;
};

