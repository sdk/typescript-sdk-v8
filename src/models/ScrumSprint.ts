/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

export type ScrumSprint = {
  /**
   * 迭代 ID
   */
  id?: number;
  /**
   * 迭代标题
   */
  title?: string;
  /**
   * 企业 ID
   */
  enterprise_id?: number;
  /**
   * 项目 ID
   */
  program_id?: number;
  /**
   * 迭代状态
   */
  state?: string;
  /**
   * 开始时间
   */
  started_at?: Timestamp;
  /**
   * 结束时间
   */
  finished_at?: Timestamp;
  /**
   * 实际结束时间
   */
  actual_finished_at?: Timestamp;
};

