/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

export type EnterpriseProgram = {
  /**
   * 所有者 id
   */
  assignee_id?: number;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 日期
   */
  date?: Timestamp;
  /**
   * 项目 id
   */
  id?: number;
  /**
   * 项目名称
   */
  name?: string;
  /**
   * 项目 path
   */
  path?: string;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

