/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IssueMain } from './IssueMain';
import type { PullRequest } from './PullRequest';

/**
 * 获取周报可关联 issue 和 pull request
 */
export type WeekReportRelation = {
  /**
   * 关联 Pull request
   */
  issues?: IssueMain;
  /**
   * 关联 Issues
   */
  pull_requests?: PullRequest;
};

