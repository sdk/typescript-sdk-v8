/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';
import type { Reaction } from './Reaction';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 修改任务下的评论
 */
export type IssueNote = {
  /**
   * 评论的 id
   */
  id?: number;
  /**
   * 评论类型
   */
  type?: string;
  /**
   * 评论发起人
   */
  author?: UserWithRemark;
  /**
   * 评论内容 (markdown 格式)
   */
  content?: string;
  /**
   * 评论内容 (html 格式)
   */
  content_html?: string;
  /**
   * 代码建议汇集信息{changed: boolean 代码是否不一致，raw: string 建议的 code}
   */
  suggestions?: any[];
  /**
   * 表态
   */
  reactions?: Reaction;
  /**
   * 评论编辑、删除权限
   */
  operating?: Object;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

