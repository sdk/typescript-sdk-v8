/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Extra } from './Extra';
import type { IssueState } from './IssueState';
import type { IssueType } from './IssueType';
import type { Label } from './Label';
import type { Milestone } from './Milestone';
import type { Object } from './Object';
import type { ScrumSprint } from './ScrumSprint';
import type { Timestamp } from './Timestamp';
import type { User } from './User';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取任务列表
 */
export type WithWorkload = {
  /**
   * 任务 ID
   */
  id?: number;
  /**
   * 根结点 ID
   */
  root_id?: number;
  /**
   * 父任务 ID
   */
  parent_id?: number;
  /**
   * 关联项目 ID
   */
  project_id?: number;
  /**
   * 任务全局唯一标识符
   */
  ident?: string;
  /**
   * 任务标题
   */
  title?: string;
  /**
   * 任务状态 id
   */
  issue_state_id?: number;
  /**
   * 项目 id
   */
  program_id?: number;
  /**
   * 任务状态标识符：open, progressing, closed, rejected
   */
  state?: string;
  /**
   * 评论数量
   */
  comments_count?: number;
  /**
   * 优先级标识符
   */
  priority?: number;
  /**
   * 关联的分支名
   */
  branch?: string;
  /**
   * 优先级中文名称
   */
  priority_human?: string;
  /**
   * 任务负责人
   */
  assignee?: User;
  /**
   * 预计工时。（单位：分钟）
   */
  duration?: number;
  /**
   * 任务创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务更新时间
   */
  updated_at?: Timestamp;
  /**
   * 任务协作者
   */
  collaborators?: Array<UserWithRemark>;
  /**
   * 任务创建者
   */
  author?: UserWithRemark;
  /**
   * 里程碑
   */
  milestone?: Milestone;
  /**
   * 任务状态
   */
  issue_state?: IssueState;
  /**
   * 任务类型
   */
  issue_type?: IssueType;
  /**
   * 任务关联的标签
   */
  labels?: Label;
  /**
   * 任务自定义字段值
   */
  issue_extra?: Extra;
  /**
   * 计划开始时间
   */
  plan_started_at?: Timestamp;
  /**
   * 计划完成时间
   */
  deadline?: Timestamp;
  /**
   * 实际完成时间
   */
  finished_at?: Timestamp;
  /**
   * 实际开始时间
   */
  started_at?: Timestamp;
  /**
   * 是否是私有 Issue
   */
  security_hole?: boolean;
  /**
   * 是否星标任务
   */
  is_star?: boolean;
  /**
   * 所属看板
   */
  kanban_info?: Object;
  /**
   * 关联迭代
   */
  scrum_sprint?: ScrumSprint;
  /**
   * 预计工时，单位小时
   */
  estimated_duration?: number;
  /**
   * 登记工时，单位小时
   */
  registered_duration?: number;
};

