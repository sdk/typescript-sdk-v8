/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Program } from './Program';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 项目管理日志
 */
export type ProgramManageLog = {
  /**
   * ID
   */
  id?: number;
  /**
   * 用户 ID
   */
  user_id?: number;
  /**
   * 操作用户
   */
  user?: UserWithRemark;
  /**
   * ip
   */
  ip?: string;
  /**
   * 目标操作对象 ID
   */
  target_id?: number;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 操作原始记录
   */
  title?: string;
  /**
   * 目标项目，如果是 null，表示已删除
   */
  target_program?: Program;
  /**
   * 操作
   */
  operating?: string;
};

