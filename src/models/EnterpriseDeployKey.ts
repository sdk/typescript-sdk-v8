/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Project } from './Project';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 修改部署公钥
 */
export type EnterpriseDeployKey = {
  /**
   * 部署公钥 id
   */
  id?: number;
  /**
   * 部署公钥 title
   */
  title?: string;
  /**
   * 部署公钥内容
   */
  key?: string;
  /**
   * sha256 指纹
   */
  sha256_fingerprint?: string;
  /**
   * 公钥的创建者
   */
  user?: UserWithRemark;
  /**
   * 已部署仓库数量
   */
  projects_count?: number;
  /**
   * 已部署的仓库
   */
  projects?: Project;
  /**
   * 是否是系统公钥
   */
  system?: boolean;
  /**
   * 是否是 bae 公钥
   */
  is_bae?: boolean;
  /**
   * 是否是 pages 公钥
   */
  is_pages?: boolean;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
};

