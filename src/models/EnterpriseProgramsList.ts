/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Array } from './Array';
import type { EnterpriseProgram } from './EnterpriseProgram';
import type { Integer } from './Integer';
import type { Timestamp } from './Timestamp';

/**
 * 获取企业项目的统计信息
 */
export type EnterpriseProgramsList = {
  /**
   * 实际完成任务数
   */
  close_issue_count?: Array;
  /**
   * 代码行数
   */
  code_line_count?: Array;
  /**
   * 提交数量
   */
  commit_count?: Array;
  /**
   * 时间列表
   */
  delay_issue_count?: Array;
  /**
   * 按期完成任务数
   */
  finish_issue_count?: Array;
  /**
   * 时间列表
   */
  date_list?: Array;
  /**
   * 项目 id
   */
  program_ids?: Array;
  /**
   * 项目列表
   */
  programs?: EnterpriseProgram;
  /**
   * 开始时间
   */
  start_date?: Timestamp;
  /**
   * 结束时间
   */
  end_date?: Timestamp;
};

