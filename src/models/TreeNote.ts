/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';
import type { Reaction } from './Reaction';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取任务类型的工作流列表
 */
export type TreeNote = {
  /**
   * 评论的 id
   */
  id?: number;
  /**
   * 评论类型
   */
  type?: string;
  /**
   * 评论发起人
   */
  author?: UserWithRemark;
  /**
   * 评论内容 (markdown 格式)
   */
  content?: string;
  /**
   * 评论内容 (html 格式)
   */
  content_html?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 祖先层级
   */
  ancestry?: string;
  /**
   * 是否最外层评论
   */
  head?: string;
  /**
   * 是否父级评论
   */
  parent?: string;
  /**
   * 评论编辑、删除权限
   */
  operating?: Object;
  /**
   * 表态
   */
  reactions?: Reaction;
};

