/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';

/**
 * 获取 Commit 下的 diffs
 */
export type PullRequestFiles = {
  /**
   * 文件评论数
   */
  comments_count?: number;
  /**
   * 文件已解决评论数
   */
  comments_resolved_count?: number;
  /**
   * Commit ID
   */
  sha?: string;
  /**
   * 文件路径 sha 值
   */
  file_path_sha?: string;
  /**
   * 文件名
   */
  filename?: string;
  /**
   * 文件改动类型。added: 新增 renamed: 重命名 deleted: 删除
   */
  status?: string;
  /**
   * 新增文件的行数
   */
  additions?: string;
  /**
   * 删除文件的行数
   */
  deletions?: string;
  patch?: string;
  /**
   * diff 统计
   */
  statistic?: string;
  /**
   * diff head
   */
  head?: string;
  /**
   * diff 内容
   */
  content?: string;
  /**
   * 已阅
   */
  viewed?: string;
  /**
   * code owner
   */
  code_owners?: Object;
};

