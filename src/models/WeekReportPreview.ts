/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Event } from './Event';
import type { IssueMain } from './IssueMain';
import type { PullRequest } from './PullRequest';

/**
 * 预览周报
 */
export type WeekReportPreview = {
  /**
   * 关联 Pull request
   */
  issues?: IssueMain;
  /**
   * 关联 Issues
   */
  pull_requests?: PullRequest;
  /**
   * 关联动态
   */
  events?: Event;
  /**
   * 内容
   */
  content?: string;
};

