/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Namespace } from './Namespace';
import type { Project } from './Project';
import type { ProjectCloneWaysBase } from './ProjectCloneWaysBase';
import type { ProjectTagBase } from './ProjectTagBase';

/**
 * 仓库概览信息
 */
export type ProjectOverview = {
  /**
   * 仓库 id
   */
  id?: number;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 仓库个性域名
   */
  path?: string;
  /**
   * 仓库 namespace_path/path
   */
  path_with_namespace?: string;
  /**
   * 仓库描述
   */
  description?: string;
  /**
   * 仓库许可证
   */
  license?: string;
  /**
   * 仓库语言
   */
  lang?: ProjectTagBase;
  /**
   * 仓库克隆方式
   */
  clone_ways?: ProjectCloneWaysBase;
  /**
   * 仓库内开启的 issue 数量
   */
  open_issue_count?: number;
  /**
   * 仓库内未完成的 issue 数量
   */
  not_finished_issue_count?: number;
  /**
   * 仓库内已完成的 issue 数量
   */
  close_issue_count?: number;
  /**
   * 仓库内 pull request 数量
   */
  total_pr_count?: number;
  /**
   * 仓库大小
   */
  repo_size?: number;
  /**
   * 仓库 wiki 数量
   */
  wiki_count?: number;
  /**
   * 仓库附件数量
   */
  attchment_count?: number;
  /**
   * 仓库成员数量
   */
  members_count?: number;
  /**
   * 刷新仓库
   */
  repo_size_refresh?: any;
  /**
   * 仓库挂载的空间
   */
  namespace?: Namespace;
  /**
   * 仓库默认分支
   */
  default_branch?: string;
  /**
   * 仓库主页地址
   */
  homepage?: string;
  /**
   * 仓库后台处理中
   */
  is_wait_fork?: boolean;
  template?: Project;
  /**
   * 是否可以执行 scan 扫描
   */
  can_exec_gitee_scan?: boolean;
};

