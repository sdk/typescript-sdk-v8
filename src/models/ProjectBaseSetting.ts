/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Program } from './Program';
import type { Project } from './Project';
import type { string } from './string';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 更新仓库设置
 */
export type ProjectBaseSetting = {
  /**
   * 仓库 ID
   */
  id?: number;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 仓库路径
   */
  path?: string;
  /**
   * namespace/path
   */
  path_with_namespace?: string;
  /**
   * 仓库开源属性，0:私有，1:开源，2:内部开源
   */
  public?: string;
  /**
   * 仓库创建时间
   */
  created_at?: Timestamp;
  /**
   * 仓库更新时间
   */
  updated_at?: Timestamp;
  /**
   * 是否为 GVP 仓库
   */
  is_gvp?: boolean;
  /**
   * 是否允许仓库被 Fork
   */
  fork_enabled?: boolean;
  /**
   * namespace_name/path
   */
  name_with_namespace?: string;
  /**
   * 仓库介绍
   */
  description?: string;
  /**
   * 仓库主页
   */
  homepage?: string;
  /**
   * 仓库语言
   */
  lang_id?: string;
  /**
   * 仓库状态 (已关闭/开发中/已完结/维护中)
   */
  status?: string;
  /**
   * 默认分支
   */
  default_branch?: string;
  /**
   * 仓库类型，0 内部，1 外包
   */
  outsourced?: boolean;
  /**
   * 仓库负责人
   */
  creator?: UserWithRemark;
  programs?: Program;
  /**
   * 允许用户对仓库进行评论
   */
  can_comment?: boolean;
  /**
   * 允许用户对"关闭"状态的 Issues 进行评论
   */
  issue_comment?: boolean;
  /**
   * 轻量级的 issue 跟踪系统
   */
  issues_enabled?: boolean;
  /**
   * 允许用户创建涉及敏感信息的 Issue，提交后不公开此 Issue（可见范围：仓库成员、企业成员）
   */
  security_hole_enabled?: boolean;
  /**
   * 是否允许仓库文件在线编辑
   */
  online_edit_enabled?: boolean;
  /**
   * 接受 pull request，协作开发
   */
  pull_requests_enabled?: boolean;
  /**
   * 可以编写文档
   */
  wiki_enabled?: boolean;
  /**
   * 接受轻量级 Pull Request（用户可以发起轻量级 Pull Request 而无需 Fork 仓库）
   */
  lightweight_pr_enabled?: boolean;
  /**
   * 开启的 Pull Request，仅管理员、审查者、测试者可见
   */
  pr_master_only?: boolean;
  /**
   * 禁止强制推送
   */
  forbid_force_push?: boolean;
  /**
   * 仓库远程地址 (用于强制同步)
   */
  import_url?: boolean;
  /**
   * 禁止仓库同步 (禁止从仓库远程地址或 Fork 的源仓库强制同步代码，禁止后将关闭同步按钮)
   */
  forbid_force_sync?: boolean;
  /**
   * 使用 SVN 管理您的仓库
   */
  svn_enabled?: boolean;
  /**
   * 开启文件/文件夹只读功能 (只读文件和 SVN 支持无法同时启用)
   */
  can_readonly?: boolean;
  /**
   * 企业是否禁用 SVN
   */
  enterprise_forbids_svn?: boolean;
  parent?: Project;
  /**
   * 是否为模板仓库
   */
  template_enabled?: boolean;
  /**
   * 仓库是启用了 GiteeGo
   */
  gitee_go_enabled?: boolean;
  /**
   * 是否支持项目级流水线
   */
  program_pipeline_enabled?: boolean;
  /**
   * 是否为空仓库
   */
  is_empty_repo?: boolean;
  /**
   * 仓库可见性
   */
  visible?: boolean;
  /**
   * 仓库审查状态
   */
  reviewing?: boolean;
  /**
   * 是否开启 merge 合并选项
   */
  merge_enabled?: boolean;
  /**
   * 是否开启 squash 合并选项
   */
  squash_enabled?: boolean;
  /**
   * 是否开启 rebase 合并选项
   */
  rebase_enabled?: boolean;
  /**
   * 选择默认合并 Pull Request 的方法，0 表示 merge（合并所有提交）、1 表示 squash（扁平化分支合并）和 2 表示 rebase（变基并合并）。默认为 0merge
   */
  default_merge_method?: string;
};

