/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { User } from './User';

export type IssueMain = {
  /**
   * 任务 ID
   */
  id?: number;
  /**
   * 根结点 ID
   */
  root_id?: number;
  /**
   * 任务全局唯一标识符
   */
  ident?: string;
  /**
   * 任务标题
   */
  title?: string;
  /**
   * 任务状态标识符：open, progressing, closed, rejected
   */
  state?: string;
  /**
   * 评论数量
   */
  comments_count?: number;
  /**
   * 优先级标识符
   */
  priority?: number;
  /**
   * 优先级中文名称
   */
  priority_human?: string;
  /**
   * 任务负责人
   */
  assignee?: User;
  /**
   * 预计工时。（单位：分钟）
   */
  duration?: number;
  /**
   * 任务创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务更新时间
   */
  updated_at?: Timestamp;
  /**
   * 任务完成时间
   */
  finished_at?: Timestamp;
  /**
   * 计划开始时间
   */
  plan_started_at?: Timestamp;
  /**
   * 截止时间
   */
  deadline?: Timestamp;
  /**
   * 是否过期
   */
  is_overdue?: boolean;
};

