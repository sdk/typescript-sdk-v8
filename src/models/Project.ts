/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 部署公钥添加仓库
 */
export type Project = {
  /**
   * 仓库 ID
   */
  id?: number;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 仓库路径
   */
  path?: string;
  /**
   * 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
   */
  public?: number;
  /**
   * 企业 id
   */
  enterprise_id?: number;
  /**
   * 仓库创建时间
   */
  created_at?: Timestamp;
  /**
   * 仓库更新时间
   */
  updated_at?: Timestamp;
};

