/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';

/**
 * 删除发行版
 */
export type Release = {
  /**
   * release
   */
  release?: Object;
  /**
   * tag
   */
  tag?: Object;
  /**
   * release 操作权限
   */
  operating?: Object;
};

