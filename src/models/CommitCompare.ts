/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CommitBase } from './CommitBase';
import type { Diff } from './Diff';

/**
 * 对比 commit
 */
export type CommitCompare = {
  /**
   * commits 数量超出限制
   */
  too_more_commits?: number;
  /**
   * commits 数量
   */
  commits_size?: number;
  /**
   * commits 数量上限
   */
  commits_limit_size?: number;
  /**
   * diff 差异文件数量
   */
  diffs_size?: string;
  /**
   * commits 列表
   */
  commits?: CommitBase;
  /**
   * diff 内容
   */
  diffs?: Diff;
};

