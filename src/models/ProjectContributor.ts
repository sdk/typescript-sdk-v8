/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取仓库贡献者列表
 */
export type ProjectContributor = {
  /**
   * 姓名
   */
  name?: string;
  /**
   * 贡献者名称
   */
  committer_name?: string;
  /**
   * 邮箱
   */
  email?: string;
  /**
   * 用户名
   */
  username?: string;
  /**
   * 是否为 Gitee 用户
   */
  is_gitee_user?: boolean;
  /**
   * 头像
   */
  image_path?: string;
  /**
   * commit 数
   */
  commits_count?: number;
  /**
   * 企业备注
   */
  enterprise_remark?: string;
};

