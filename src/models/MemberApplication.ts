/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 处理申请记录
 */
export type MemberApplication = {
  /**
   * 成员申请的 id
   */
  id?: number;
  /**
   * 成员申请时的名称
   */
  applicant_name?: string;
  /**
   * 申请时的名称
   */
  applicant?: UserWithRemark;
  /**
   * 邀请者信息
   */
  inviter?: UserWithRemark;
  /**
   * 申请时的备注
   */
  remark?: string;
  /**
   * 是否需要审核
   */
  need_check?: string;
  /**
   * 审核的状态。申请中：pending; 审核通过：approved; 已过期：overdue; 已拒绝：refused; 接收邀请链接：invite_pass
   */
  state?: string;
  /**
   * 操作者
   */
  operator?: string;
  /**
   * 角色 ID
   */
  role_id?: number;
  /**
   * 权限属性
   */
  access?: number;
  /**
   * 创建时间/申请时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

