/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DeployKey = {
  /**
   * ID
   */
  id?: number;
  /**
   * 标题
   */
  title?: string;
  /**
   * 创建时间
   */
  created_at?: string;
};

