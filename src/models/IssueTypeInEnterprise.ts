/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 获取企业任务类型列表
 */
export type IssueTypeInEnterprise = {
  /**
   * 任务类型 ID
   */
  id?: number;
  /**
   * 任务类型的名称
   */
  title?: string;
  /**
   * 任务类型模板
   */
  template?: string;
  /**
   * 唯一标识符
   */
  ident?: string;
  /**
   * 颜色
   */
  color?: string;
  /**
   * 是否系统默认类型
   */
  is_system?: boolean;
  /**
   * 任务类型创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务类型更新时间
   */
  updated_at?: Timestamp;
  /**
   * 类型属性
   */
  category?: string;
  /**
   * 任务类型描述
   */
  description?: string;
  /**
   * 使用的项目数量
   */
  programs_count?: number;
  /**
   * 任务类型的状态
   */
  state?: number;
  /**
   * 是否正在迁移
   */
  is_migrating?: boolean;
  /**
   * 是否有工作项正在使用
   */
  is_anyissue_using?: boolean;
};

