/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ScrumStage = {
  /**
   * 阶段 ID
   */
  id?: number;
  /**
   * 企业 ID
   */
  enterprise_id?: number;
  /**
   * 项目 ID
   */
  program_id?: number;
  /**
   * 版本 ID
   */
  scrum_version_id?: number;
  /**
   * 父阶段 ID
   */
  parent_id?: number;
  /**
   * 阶段名称
   */
  title?: string;
  /**
   * 排序值
   */
  sort?: number;
  /**
   * 状态
   */
  state?: string;
  /**
   * 完成时间
   */
  closed_at?: string;
  /**
   * 计划完成时间
   */
  plan_closed_at?: string;
};

