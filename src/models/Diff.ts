/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';

export type Diff = {
  /**
   * Commit ID
   */
  sha?: string;
  /**
   * 文件路径 sha 值
   */
  file_path_sha?: string;
  /**
   * 文件名
   */
  filename?: string;
  /**
   * 文件改动类型。added: 新增 renamed: 重命名 deleted: 删除
   */
  status?: string;
  /**
   * 新增代码行数
   */
  additions?: number;
  /**
   * 删除代码行数
   */
  deletions?: number;
  /**
   * diff 文件代码行数
   */
  diff_file_loc?: number;
  /**
   * diff 统计
   */
  statistic?: string;
  /**
   * diff head
   */
  head?: string;
  /**
   * diff 内容
   */
  content?: string;
  /**
   * diff 内容
   */
  patch?: Object;
};

