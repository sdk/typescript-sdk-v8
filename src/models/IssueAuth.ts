/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取授权用户对任务的权限
 */
export type IssueAuth = {
  /**
   * 是否可查看此任务
   */
  read?: boolean;
  /**
   * 是否可更新此任务的属性等状态
   */
  update?: boolean;
  /**
   * 是否可更新此任务的标题或内容
   */
  update_main?: boolean;
  /**
   * 是否可删除此任务
   */
  destroy?: boolean;
  /**
   * 是否可评论此任务
   */
  create_note?: boolean;
  /**
   * 是否可创建子任务，关联任务等
   */
  create_issue?: boolean;
  /**
   * 是否可查看测试用例
   */
  read_test_plan?: boolean;
  /**
   * 是否可更新测试用例
   */
  update_test_plan?: boolean;
};

