/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 新建仓库 - 导入仓库参数是否有效
 */
export type ResultResponse = {
  /**
   * private: 私有，success: 成功，duplicate: 仓库已经导入 (仓库信息：project)
   */
  result?: string;
  /**
   * 消息
   */
  message?: string;
};

