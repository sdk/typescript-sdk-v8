/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

export type Milestone = {
  /**
   * 里程碑 ID
   */
  id?: number;
  /**
   * 里程碑标题名称
   */
  title?: string;
  /**
   * 里程碑状态
   */
  state?: string;
  /**
   * 里程碑起始日期
   */
  start_date?: Timestamp;
  /**
   * 里程碑结束日期
   */
  due_date?: Timestamp;
  /**
   * 任务标签创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务标签更新时间
   */
  updated_at?: Timestamp;
  /**
   * 里程碑所属项目 ID
   */
  program_id?: number;
};

