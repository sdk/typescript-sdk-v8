/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Namespace } from './Namespace';
import type { Project } from './Project';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取项目下的仓库列表
 */
export type ProjectsList = {
  /**
   * 仓库 ID
   */
  id?: number;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 仓库路径
   */
  path?: string;
  /**
   * 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
   */
  public?: number;
  /**
   * 企业 id
   */
  enterprise_id?: number;
  /**
   * 仓库创建时间
   */
  created_at?: Timestamp;
  /**
   * 仓库更新时间
   */
  updated_at?: Timestamp;
  /**
   * 是否允许用户创建涉及敏感信息的任务
   */
  security_hole_enabled?: boolean;
  /**
   * 仓库挂载的空间
   */
  namespace?: Namespace;
  /**
   * 仓库创建者
   */
  creator?: UserWithRemark;
  /**
   * 个人空间地址以及仓库路径
   */
  path_with_namespace?: string;
  /**
   * 个人空间名称以及仓库名称
   */
  name_with_namespace?: string;
  /**
   * 仓库是否开了 scan 质量门禁
   */
  scan_check_run?: boolean;
  /**
   * 是否是 fork 仓库
   */
  is_fork?: boolean;
  /**
   * 父级仓库
   */
  parent_project?: Project;
  /**
   * 状态值
   */
  status?: number;
  /**
   * 状态中文名称
   */
  status_name?: string;
  /**
   * 是否外包
   */
  outsourced?: boolean;
  /**
   * 仓库大小
   */
  repo_size?: number;
  /**
   * 能否操作当前仓库
   */
  can_admin_project?: boolean;
  /**
   * 成员数
   */
  members_count?: number;
  /**
   * 最近 push
   */
  last_push_at?: Timestamp;
  /**
   * watches 数
   */
  watches_count?: number;
  /**
   * stars 数
   */
  stars_count?: number;
  /**
   * 被 fork 数
   */
  forked_count?: number;
  /**
   * 是否开启备份
   */
  enable_backup?: boolean;
  /**
   * 是否有备份
   */
  has_backups?: boolean;
  /**
   * 是否 vip
   */
  vip?: boolean;
  /**
   * 是否推荐
   */
  recomm?: boolean;
  /**
   * 模板仓库基本信息
   */
  template?: Project;
  /**
   * 是否为模板仓库
   */
  template_enabled?: boolean;
  /**
   * 仓库描述
   */
  description?: string;
  /**
   * 默认分支
   */
  get_default_branch?: string;
  /**
   * 发行版数
   */
  releases_count?: number;
  /**
   * PR 数
   */
  total_pr_count?: number;
  /**
   * 是否收藏
   */
  is_star?: boolean;
  /**
   * 使用此仓库作为模板的仓库总数
   */
  used_template_count?: number;
  /**
   * 是否允许被 Fork
   */
  fork_enabled?: boolean;
  /**
   * 是否接受 PR
   */
  pull_requests_enabled?: boolean;
  /**
   * 去除顶层命名空间名称的仓库全路径名称
   */
  name_without_top_namespace?: string;
};

