/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Array } from './Array';
import type { EnterpriseProject } from './EnterpriseProject';
import type { Integer } from './Integer';
import type { Timestamp } from './Timestamp';

/**
 * 获取企业仓库的统计信息
 */
export type EnterpriseProjectsList = {
  close_issue_count?: Array;
  /**
   * 代码行数
   */
  code_line_count?: Array;
  /**
   * 提交数量
   */
  commit_count?: Array;
  /**
   * 创建任务数
   */
  create_issue_count?: Array;
  /**
   * 创建 PR 数
   */
  create_pr_count?: Array;
  /**
   * 日期列表
   */
  date_list?: Array;
  /**
   * fork 数量
   */
  fork_count?: Array;
  /**
   * 合并 PR 数
   */
  merge_pr_count?: Array;
  /**
   * 仓库 id
   */
  project_ids?: Array;
  /**
   * 仓库列表
   */
  projects?: EnterpriseProject;
  /**
   * star 数量
   */
  stars_count?: Array;
  /**
   * 开始时间
   */
  start_date?: Timestamp;
  /**
   * 结束时间
   */
  end_date?: Timestamp;
};

