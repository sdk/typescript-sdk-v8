/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 变更团队成员角色
 */
export type GroupMember = {
  /**
   * 成员 ID
   */
  id?: number;
  /**
   * 用户 ID
   */
  user_id?: number;
  /**
   * 角色等级
   */
  access_level?: number;
};

