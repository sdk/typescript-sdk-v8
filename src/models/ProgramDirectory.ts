/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取项目类型文件夹
 */
export type ProgramDirectory = {
  /**
   * id
   */
  id?: number;
  /**
   * 父层级的 id
   */
  parent_id?: number;
  /**
   * 名称
   */
  name?: string;
  /**
   * 权限值
   */
  public?: string;
  /**
   * 权限名称
   */
  public_name?: string;
  /**
   * 关联项目
   */
  program?: string;
  /**
   * 关联类型
   */
  file_type?: string;
  /**
   * 关联类型的 id
   */
  file_id?: string;
  children_count?: string;
  /**
   * 是否查看附件历史版本
   */
  file_versions?: boolean;
  /**
   * 创建者
   */
  creator?: UserWithRemark;
  /**
   * 项目属性
   */
  category?: string;
  /**
   * 项目编号
   */
  ident?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

