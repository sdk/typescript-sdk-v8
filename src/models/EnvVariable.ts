/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 修改环境变量
 */
export type EnvVariable = {
  /**
   * 环境变量 id
   */
  id?: number;
  /**
   * 仓库 id
   */
  project_id?: number;
  /**
   * 环境变量名称
   */
  name?: string;
  /**
   * 环境变量值
   */
  value?: string;
  /**
   * 环境变量备注
   */
  remark?: string;
  /**
   * 修改时间
   */
  updated_at?: string;
  /**
   * 是否只读，1:true,0:false
   */
  read_only?: number;
  /**
   * 是否密钥，1:true,0:false
   */
  is_secret?: number;
};

