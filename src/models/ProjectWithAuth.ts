/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Namespace } from './Namespace';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取成员加入的仓库列表
 */
export type ProjectWithAuth = {
  /**
   * 仓库 ID
   */
  id?: number;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 仓库路径
   */
  path?: string;
  /**
   * 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
   */
  public?: number;
  /**
   * 企业 id
   */
  enterprise_id?: number;
  /**
   * 仓库创建时间
   */
  created_at?: Timestamp;
  /**
   * 仓库更新时间
   */
  updated_at?: Timestamp;
  /**
   * 是否允许用户创建涉及敏感信息的任务
   */
  security_hole_enabled?: boolean;
  /**
   * 仓库挂载的空间
   */
  namespace?: Namespace;
  /**
   * 仓库创建者
   */
  creator?: UserWithRemark;
  /**
   * 个人空间地址以及仓库路径
   */
  path_with_namespace?: string;
  /**
   * 个人空间名称以及仓库名称
   */
  name_with_namespace?: string;
  /**
   * 仓库是否开了 scan 质量门禁
   */
  scan_check_run?: boolean;
  /**
   * 能否编辑退出
   */
  can_quit?: boolean;
  /**
   * 当前成员在该仓库的角色
   */
  project_access?: number;
};

