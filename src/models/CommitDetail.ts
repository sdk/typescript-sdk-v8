/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Array } from './Array';
import type { CommitSignature } from './CommitSignature';
import type { Diff } from './Diff';
import type { Object } from './Object';

/**
 * commit 详情
 */
export type CommitDetail = {
  /**
   * Commit ID
   */
  id?: string;
  /**
   * Commit Short ID
   */
  short_id?: number;
  /**
   * Commit Title
   */
  title?: number;
  /**
   * Commit Html Title
   */
  title_html?: number;
  /**
   * Commit Description
   */
  description?: string;
  /**
   * Commit Html Description
   */
  description_html?: string;
  /**
   * Commit Message
   */
  message?: string;
  /**
   * Complete Commit Title
   */
  complete_title?: string;
  /**
   * Complete Commit Title Html
   */
  complete_title_html?: string;
  /**
   * Commit Html Message
   */
  message_html?: string;
  /**
   * Commit ��者
   */
  author?: number;
  /**
   * Commit 提交人
   */
  committer?: number;
  /**
   * 推送时间
   */
  authored_date?: number;
  /**
   * 提交时间
   */
  committed_date?: number;
  /**
   * 签名
   */
  signature?: CommitSignature;
  /**
   * Gitee Go 构建状态
   */
  build_state?: number;
  /**
   * 父节点
   */
  parents?: Array;
  /**
   * diff 文件大小
   */
  diff_files_size?: number;
  /**
   * 可渲染的 diff 文件大小
   */
  limit_diff_files_size?: number;
  /**
   * diff 大小是否超出限制
   */
  is_overflow?: boolean;
  /**
   * diff 是否过大
   */
  is_change_to_large?: boolean;
  /**
   * 新增行数
   */
  added_lines?: number;
  /**
   * 删除行数
   */
  removed_lines?: number;
  /**
   * 文件 diff
   */
  diffs?: Diff;
};

