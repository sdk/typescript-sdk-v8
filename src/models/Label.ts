/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 更新标签
 */
export type Label = {
  /**
   * 任务标签 ID
   */
  id?: number;
  /**
   * 任务标签的名称
   */
  name?: string;
  /**
   * 标签颜色
   */
  color?: string;
  /**
   * 任务标签创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务标签更新时间
   */
  updated_at?: Timestamp;
};

