/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';
import type { Project } from './Project';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 周报可关联动态列表
 */
export type Event = {
  /**
   * 动态 id
   */
  id?: number;
  target_type?: string;
  /**
   * 动态标识
   */
  ident?: string;
  /**
   * 是否合规
   */
  is_proper?: boolean;
  /**
   * 动态产生的时间
   */
  created_at?: string;
  /**
   * 动作
   */
  action?: string;
  /**
   * 动作名称
   */
  action_human_name?: string;
  /**
   * 产生动态的成员
   */
  author?: UserWithRemark;
  /**
   * 是否在仓库
   */
  in_project?: boolean;
  /**
   * 是否在企业
   */
  in_enterprise?: boolean;
  /**
   * 仓库
   */
  project?: Project;
  /**
   * 企业
   */
  enterprise?: string;
  /**
   * 动态来源对象类型
   */
  through_type?: string;
  /**
   * 动态来源对象类型
   */
  through?: Object;
  /**
   * 不同类型动态的内容
   */
  payload?: any;
};

