/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DocAttachFile } from './DocAttachFile';
import type { Program } from './Program';
import type { ScrumSprint } from './ScrumSprint';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取最近编辑的文档
 */
export type ScrumDocNode = {
  /**
   * id
   */
  id?: number;
  /**
   * 父层级的 id
   */
  parent_id?: number;
  /**
   * 名称
   */
  name?: string;
  /**
   * 权限值
   */
  public?: string;
  /**
   * 权限名称
   */
  public_name?: string;
  /**
   * 关联文档完整路径
   */
  file_path?: string;
  /**
   * 关联项目
   */
  program?: Program;
  /**
   * 关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile)
   */
  file_type?: string;
  /**
   * 关联类型的 id
   */
  file_id?: string;
  /**
   * 是否查看附件历史版本
   */
  file_versions?: boolean;
  /**
   * 创建者
   */
  creator?: UserWithRemark;
  /**
   * 是否已收藏
   */
  is_favour?: boolean;
  /**
   * 附件相关信息
   */
  attach_file?: DocAttachFile;
  /**
   * 是否 wiki
   */
  is_wiki?: boolean;
  /**
   * 编辑器类型
   */
  editor_type?: string;
  /**
   * 需要密码访问
   */
  need_password?: boolean;
  /**
   * 是否置顶
   */
  is_top?: boolean;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 删除时间
   */
  deleted_at?: Timestamp;
  /**
   * 文档所属的迭代详情
   */
  scrum_sprint?: ScrumSprint;
};

