/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取管理界面的字段列表
 */
export type IssueField = {
  /**
   * 字段 id
   */
  id?: number;
  /**
   * 字段名称
   */
  name?: string;
  /**
   * 字段类型
   */
  field_type?: string;
  /**
   * 字段可选值
   */
  options?: string;
  /**
   * 是否为系统字段
   */
  is_system?: boolean;
  /**
   * 系统字段英文名
   */
  system_name?: string;
  /**
   * 字段描述
   */
  description?: string;
};

