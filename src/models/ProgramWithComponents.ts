/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ProgramComponent } from './ProgramComponent';
import type { ProgramExtraField } from './ProgramExtraField';
import type { Timestamp } from './Timestamp';

export type ProgramWithComponents = {
  /**
   * 项目 id
   */
  id?: number;
  /**
   * 项目编号
   */
  ident?: string;
  /**
   * 项目名称
   */
  name?: string;
  /**
   * 项目描述
   */
  description?: string;
  /**
   * 项目状态（0:开始 1:暂停 2:关闭）
   */
  status?: number;
  /**
   * 是否外包项目
   */
  outsourced?: boolean;
  /**
   * 项目类型（内部、外包）
   */
  type?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 颜色
   */
  color?: string;
  /**
   * 项目类型
   */
  category?: string;
  /**
   * 项目自定义字段值
   */
  program_extra_fields?: ProgramExtraField;
  /**
   * 项目组件列表
   */
  components?: ProgramComponent;
};

