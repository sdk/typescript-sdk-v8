/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProjectCloneWaysBase = {
  /**
   * https 克隆 url
   */
  https_url?: string;
  /**
   * ssh 克隆 url
   */
  ssh_url?: string;
  /**
   * svn 克隆 url
   */
  svn_url?: string;
  /**
   * svn_and_ssh 克隆 url
   */
  svn_and_ssh_url?: string;
};

