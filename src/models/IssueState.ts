/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IssueType } from './IssueType';
import type { Timestamp } from './Timestamp';

/**
 * 更新任务状态
 */
export type IssueState = {
  /**
   * 任务状态 ID
   */
  id?: number;
  /**
   * 任务状态的名称
   */
  title?: string;
  /**
   * 任务状态的颜色
   */
  color?: string;
  /**
   * 任务状态的 Icon
   */
  icon?: string;
  /**
   * 任务状态的 指令
   */
  command?: string;
  /**
   * 任务状态的 排序
   */
  serial?: string;
  /**
   * 关联该任务状态的任务类型
   */
  issue_types?: IssueType;
  /**
   * 任务状态创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务状态更新时间
   */
  updated_at?: Timestamp;
};

