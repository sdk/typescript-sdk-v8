/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 取消 PR 表态
 */
export type IssueReaction = {
  /**
   * id
   */
  id?: number;
  /**
   * 表态对象 id
   */
  target_id?: number;
  /**
   * 表态对象类型
   */
  target_type?: string;
  /**
   * 表态
   */
  text?: string;
  /**
   * 表态人 id
   */
  user_id?: number;
  /**
   * 表态人名
   */
  user_name?: string;
};

