/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 更新文件
 */
export type WikiSortDetail = {
  /**
   * 文件 id
   */
  id?: number;
  /**
   * 文件名称
   */
  name?: string;
  /**
   * 父级 id
   */
  parent_id?: number;
  /**
   * 文件创建者
   */
  creator?: UserWithRemark;
  /**
   * 最后编辑者
   */
  editor?: UserWithRemark;
  /**
   * 文件类型 (文件：1; 文件夹：2)
   */
  file_type?: number;
  /**
   * 文件的最后一次版本号
   */
  version?: string;
  /**
   * 文件的最后编辑时间
   */
  last_edit_time?: Timestamp;
  /**
   * 文件的内容类型
   */
  content_type?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 文件内容
   */
  content?: string;
  /**
   * 文件详情 html
   */
  content_html?: string;
};

