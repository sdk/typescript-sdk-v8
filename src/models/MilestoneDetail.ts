/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Bollean } from './Bollean';
import type { IssueWorkloadOverview } from './IssueWorkloadOverview';
import type { Object } from './Object';
import type { Program } from './Program';
import type { Project } from './Project';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 编辑里程碑
 */
export type MilestoneDetail = {
  /**
   * 里程碑 ID
   */
  id?: number;
  /**
   * 里程碑标题名称
   */
  title?: string;
  /**
   * 里程碑状态
   */
  state?: string;
  /**
   * 里程碑起始日期
   */
  start_date?: Timestamp;
  /**
   * 里程碑结束日期
   */
  due_date?: Timestamp;
  /**
   * 任务标签创建时间
   */
  created_at?: Timestamp;
  /**
   * 任务标签更新时间
   */
  updated_at?: Timestamp;
  /**
   * 里程碑所属项目 ID
   */
  program_id?: number;
  /**
   * 描述
   */
  description?: string;
  /**
   * 描述 (html 格式)
   */
  description_html?: string;
  /**
   * 里程碑负责人
   */
  assignee?: UserWithRemark;
  /**
   * 里程碑作者
   */
  author?: UserWithRemark;
  /**
   * 里程碑 issues 数量
   */
  issue_all_count?: number;
  /**
   * 里程碑完成 issue 数量
   */
  issue_complete_count?: number;
  /**
   * 里程碑 pr 数量
   */
  pr_all_count?: number;
  /**
   * 里程碑完成 pr 数量
   */
  pr_complete_count?: number;
  /**
   * 里程碑关联仓库
   */
  projects?: Project;
  /**
   * 里程碑关联项目
   */
  program?: Program;
  /**
   * 是否置顶
   */
  top?: Bollean;
  /**
   * 删除权限
   */
  can_delete?: Bollean;
  /**
   * 编辑权限
   */
  can_modify?: Bollean;
  /**
   * 阅读 issue 权限
   */
  can_read_issue?: Bollean;
  /**
   * 阅读 pull request 权限
   */
  can_read_pull_request?: Bollean;
  /**
   * 延期的任务数
   */
  delay_issue_count?: number;
  /**
   * 任务类型统计
   */
  issues_type_statistics?: Object;
  /**
   * 是否有权限批量操作任务
   */
  can_batch_update_issues?: Bollean;
  /**
   * 工时概览
   */
  work_time_overview?: IssueWorkloadOverview;
  /**
   * 是否因为项目改类型而被锁定
   */
  lock_by_program?: boolean;
};

