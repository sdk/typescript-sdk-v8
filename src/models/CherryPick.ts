/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 创建 Cherry Pick
 */
export type CherryPick = {
  /**
   * 自动创建的 cherry pick 分支名
   */
  cherry_branch?: string;
  /**
   * 创建状态
   */
  status?: string;
};

