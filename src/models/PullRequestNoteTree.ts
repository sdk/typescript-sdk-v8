/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取 Pull Request 或相关提交的评论列表树
 */
export type PullRequestNoteTree = {
  /**
   * Pull Request id
   */
  pr_id?: number;
  /**
   * Pull Request 的评论数量
   */
  count?: number;
  /**
   * Pull Request 的操作日志、普通评论、文件行评论包含代码建议的集合
   */
  list?: any[];
  /**
   * Pull Request 的 diff position 社区版请求地址
   */
  diff_position_context_path?: string;
  /**
   * 是否能关闭 Pull Request
   */
  can_close_pr?: boolean;
};

