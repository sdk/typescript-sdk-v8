/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ProjectGroupDetail } from './ProjectGroupDetail';
import type { ProjectsList } from './ProjectsList';

/**
 * 获取授权用户参与的仓库列表 (按层级获取)
 */
export type UserProjectList = {
  /**
   * 总数
   */
  total_count?: number;
  /**
   * 仓库列表
   */
  projects?: ProjectsList;
  /**
   * 仓库组列表
   */
  project_groups?: ProjectGroupDetail;
};

