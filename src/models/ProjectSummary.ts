/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取仓库的概览数据
 */
export type ProjectSummary = {
  /**
   * 分支名称
   */
  ref?: string;
  /**
   * release 数
   */
  releases_count?: number;
  /**
   * 默认保护分支规则
   */
  default_protection_rule?: string;
  /**
   * 编程语言种类（数组）
   */
  languages?: any[];
  /**
   * 是否可创建轻量级 PR
   */
  create_lightweight_pr?: boolean;
  /**
   * 是否允许在线编辑
   */
  online_edit_enabled?: boolean;
  /**
   * http 克隆地址
   */
  https_url?: string;
  /**
   * ssh 克隆地址
   */
  ssh_url?: string;
  /**
   * svn 克隆地址
   */
  svn_url?: string;
  /**
   * svn+ssh 地址
   */
  svn_and_ssh_url?: string;
  /**
   * 是否允许下载 zip 包
   */
  can_download_zip?: boolean;
  /**
   * 是否允许创建 issue
   */
  can_create_issue?: boolean;
};

