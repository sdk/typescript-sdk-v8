/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 编辑周报模版
 */
export type WeekReportTemplate = {
  /**
   * 模版 ID
   */
  id?: number;
  /**
   * 模版内容
   */
  content?: string;
  /**
   * 是否是默认模版
   */
  is_default?: boolean;
  /**
   * 模版名称
   */
  name?: string;
};

