/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CommitSignature } from './CommitSignature';

export type CommitBase = {
  /**
   * Commit ID
   */
  id?: string;
  /**
   * Commit Short ID
   */
  short_id?: number;
  /**
   * Commit Title
   */
  title?: number;
  /**
   * Commit Html Title
   */
  title_html?: number;
  /**
   * Commit Description
   */
  description?: string;
  /**
   * Commit Html Description
   */
  description_html?: string;
  /**
   * Commit Message
   */
  message?: string;
  /**
   * Complete Commit Title
   */
  complete_title?: string;
  /**
   * Complete Commit Title Html
   */
  complete_title_html?: string;
  /**
   * Commit Html Message
   */
  message_html?: string;
  /**
   * Commit 作者
   */
  author?: number;
  /**
   * Commit 提交人
   */
  committer?: number;
  /**
   * 推送时间
   */
  authored_date?: number;
  /**
   * 提交时间
   */
  committed_date?: number;
  /**
   * 签名
   */
  signature?: CommitSignature;
  /**
   * Gitee Go 构建状态
   */
  build_state?: number;
};

