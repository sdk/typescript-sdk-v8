/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserWithRemark } from './UserWithRemark';

/**
 * 新增表态
 */
export type Reaction = {
  /**
   * id
   */
  id?: number;
  /**
   * 表态的类型。(PullRequest、Note、Issue)
   */
  target_type?: string;
  /**
   * 对应表态类型的 id
   */
  target_id?: number;
  /**
   * 用户的 id
   */
  user_id?: number;
  /**
   * 用户的详情信息
   */
  user?: UserWithRemark;
  /**
   * 表态内容
   */
  text?: string;
};

