/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

/**
 * 企业外的成员 - 从企业组织或仓库中移除 (单个/批量)
 */
export type User = {
  /**
   * 用户 id
   */
  id?: number;
  /**
   * 用户个性地址
   */
  username?: string;
  /**
   * 用户名称
   */
  name?: string;
  /**
   * 用户头像
   */
  avatar_url?: string;
  /**
   * 账号是否可用 (active: 可用 blocked: 已被系统屏蔽)
   */
  state?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 最近活跃时间
   */
  updated_at?: Timestamp;
  /**
   * 用户代码风格
   */
  user_color?: number;
  /**
   * 用户来源
   */
  authen_way?: string;
  /**
   * 邮箱
   */
  email?: string;
  /**
   * 是否需要重置密码
   */
  need_change_password?: boolean;
};

