/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取 Pull Request 操作日志
 */
export type PrOperateLog = {
  id?: number;
  /**
   * 图标
   */
  icon?: string;
  /**
   * 内容
   */
  content?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 操作者
   */
  user?: UserWithRemark;
};

