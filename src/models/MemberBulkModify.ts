/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Array } from './Array';
import type { Integer } from './Integer';

/**
 * 给成员添加/移出项目
 */
export type MemberBulkModify = {
  /**
   * 成功删除 id
   */
  removed_ids?: Array;
  /**
   * 成功添加 id
   */
  added_ids?: Array;
};

