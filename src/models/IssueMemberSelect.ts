/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取任务指派的成员列表
 */
export type IssueMemberSelect = {
  /**
   * 用户 id
   */
  id?: number;
  /**
   * 用户个性地址
   */
  username?: string;
  /**
   * 用户名称
   */
  name?: string;
  /**
   * 用户在企业的备注名
   */
  remark?: string;
  /**
   * 成员备注或名称拼音
   */
  pinyin?: string;
  /**
   * 用户头像
   */
  avatar_url?: string;
  /**
   * 是否企业成员
   */
  is_enterprise_member?: boolean;
  /**
   * 是否是已离职成员
   */
  is_history_member?: boolean;
  /**
   * 是否外包成员
   */
  outsourced?: boolean;
  /**
   * 是否项目成员
   */
  is_program_member?: boolean;
  /**
   * 是否仓库成员
   */
  is_project_member?: boolean;
};

