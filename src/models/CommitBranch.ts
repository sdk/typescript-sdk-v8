/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 获取 commit 的分支和 tag
 */
export type CommitBranch = {
  /**
   * 分支
   */
  branches?: any[];
  /**
   * tag
   */
  tags?: any[];
};

