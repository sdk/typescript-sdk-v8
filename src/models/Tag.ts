/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 新建标签
 */
export type Tag = {
  /**
   * 标签名称
   */
  name?: string;
  /**
   * 标签信息
   */
  message?: string;
  /**
   * 标签对应 commit
   */
  commit?: any;
  /**
   * 操作权限
   */
  operating?: any;
};

