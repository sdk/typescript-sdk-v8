/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProjectTagBase = {
  /**
   * id
   */
  id?: number;
  /**
   * 唯一标识
   */
  ident?: string;
  /**
   * 名称
   */
  name?: string;
};

