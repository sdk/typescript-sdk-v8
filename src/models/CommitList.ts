/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CommitBase } from './CommitBase';
import type { Timestamp } from './Timestamp';

/**
 * commits 列表
 */
export type CommitList = {
  /**
   * 日期
   */
  day?: Timestamp;
  /**
   * commit 数
   */
  count?: number;
  /**
   * commits 列表
   */
  commits?: CommitBase;
};

