/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DocAttachFile = {
  /**
   * 下载链接
   */
  download_url?: string;
  /**
   * 预览地址
   */
  preview_path?: string;
  /**
   * 文件类型
   */
  file_type?: string;
  /**
   * 文件大小
   */
  size?: string;
  /**
   * 文件后缀
   */
  ext?: string;
  /**
   * 是否仓库内的附件
   */
  belong_project?: boolean;
  /**
   * 关联分支
   */
  branch?: string;
  /**
   * 描述
   */
  description?: string;
  /**
   * 下载次数
   */
  download_count?: number;
};

