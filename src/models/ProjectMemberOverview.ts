/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 成员概览
 */
export type ProjectMemberOverview = {
  /**
   * 观察者数量
   */
  viewer?: number;
  /**
   * 报告者数量
   */
  reporter?: number;
  /**
   * 开发者数量
   */
  developer?: number;
  /**
   * 管理者数量
   */
  master?: number;
  /**
   * 所有成员数量
   */
  all?: number;
  /**
   * 待处理成员申请数量
   */
  padding_apply_count?: number;
  /**
   * 允许外部成员申请加
   */
  allow_external_apply?: boolean;
};

