/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { User } from './User';

/**
 * 更新企业公告内容
 */
export type Enterprise = {
  /**
   * 企业 id
   */
  id?: number;
  /**
   * 企业名称
   */
  name?: string;
  /**
   * 企业路径
   */
  path?: string;
  /**
   * 企业简介
   */
  description?: string;
  /**
   * 企业类型 (0: 未公开 1: 公开)
   */
  public?: string;
  /**
   * 企业官网
   */
  website?: string;
  /**
   * 企业邮箱
   */
  email?: string;
  /**
   * 企业手机号码
   */
  phone?: string;
  /**
   * 企业头像
   */
  avatar_url?: string;
  /**
   * 企业套餐所属级别
   */
  level?: number;
  /**
   * 企业套餐名称
   */
  title?: string;
  /**
   * 企业拥有者
   */
  owner?: User;
  /**
   * 企业公告
   */
  notice?: string;
  /**
   * 企业欢迎私信内容
   */
  welcome_message?: string;
  /**
   * 是否开启强制审核
   */
  force_verify?: boolean;
  /**
   * 企业默认角色 ID
   */
  default_role_id?: number;
  /**
   * 是否允许外部成员申请加入
   */
  open_application?: number;
  version?: number;
  /**
   * 套餐开始时间
   */
  start_at?: Timestamp;
  /**
   * 套餐结束时间（免费版企业为空）
   */
  stop_at?: Timestamp;
  /**
   * 是否过期企业
   */
  overdue?: boolean;
  /**
   * 是否付费企业
   */
  charged?: boolean;
  /**
   * 企业创建时间
   */
  created_at?: Timestamp;
  /**
   * 上次信息变更时间
   */
  updated_at?: Timestamp;
  /**
   * 是否开启水印
   */
  watermark?: boolean;
  /**
   * 是否开启 Gitee Search, not_open、opening、success、failed
   */
  gitee_search_status?: string;
  /**
   * 是否拥有对冲订单
   */
  has_hedge_order?: boolean;
  /**
   * [Premium] 是否开启 Ldap
   */
  ldap_enabled?: boolean;
  /**
   * [Premium] 是否开启用户注册
   */
  register_enabled?: boolean;
};

