/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 获取任务附件
 */
export type AttachFile = {
  /**
   * 附件 id
   */
  id?: number;
  /**
   * 附件名称
   */
  name?: string;
  /**
   * 附件的大小
   */
  size?: number;
  /**
   * 附件的类型
   */
  file_type?: string;
  /**
   * 预览链接
   */
  preview_url?: string;
  /**
   * 附件的创建者
   */
  creator?: UserWithRemark;
  /**
   * 附件的创建时间
   */
  created_at?: Timestamp;
  /**
   * 附件的更新时间
   */
  updated_at?: Timestamp;
  /**
   * 能否删除附件
   */
  can_delete?: boolean;
};

