/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Namespace } from './Namespace';
import type { Timestamp } from './Timestamp';
import type { User } from './User';

export type ProjectGroupDetail = {
  /**
   * 仓库组 id
   */
  id?: number;
  /**
   * 仓库组完整名称
   */
  complete_name?: string;
  /**
   * 仓库组完整路径
   */
  complete_path?: string;
  /**
   * 仓库组名称
   */
  name?: string;
  /**
   * 仓库组上级 id
   */
  parent_id?: number;
  /**
   * 仓库组路径
   */
  path?: string;
  /**
   * 仓库组简介
   */
  description?: string;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 负责人
   */
  owner?: User;
  /**
   * 归属团队或企业
   */
  root?: Namespace;
  /**
   * 创建者
   */
  creator?: User;
  /**
   * 父级 namespace
   */
  parent?: Namespace;
  /**
   * 不包含归属名称的全名称
   */
  complete_name_without_namespace?: string;
};

