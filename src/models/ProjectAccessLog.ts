/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { id } from './id';
import type { Project } from './Project';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 仓库代码日志
 */
export type ProjectAccessLog = {
  /**
   * id
   */
  id?: number;
  /**
   * uuid
   */
  uuid?: string;
  /**
   * 用户 ID
   */
  user_id?: id;
  /**
   * 操作用户，如果 user 是 null，表示已删除
   */
  user?: UserWithRemark;
  /**
   * ip
   */
  ip?: string;
  /**
   * 过滤后 IP
   */
  ip_filter?: string;
  /**
   * 操作：{0=>"HTTP_ACCESS", 1=>"SSH_PULL", 2=>"SVN_PULL", 3=>"HTTP_PULL", 4=>"SSH_PUSH", 5=>"SVN_PUSH", 6=>"HTTP_PUSH", 7=>"DOWNLOAD_ZIP"}
   */
  stat_type?: string;
  /**
   * 操作
   */
  stat_type_cn?: string;
  /**
   * 仓库 ID
   */
  project_id?: id;
  /**
   * 仓库，如果 project 是 null，表示已删除
   */
  project?: Project;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
};

