/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Timestamp } from './Timestamp';

export type OsProject = {
  /**
   * 更新日期
   */
  date?: Timestamp;
  /**
   * 仓库 id
   */
  id?: number;
  /**
   * 企业仓库路径
   */
  enterprise_project_path?: string;
  /**
   * 仓库名称
   */
  name?: string;
  /**
   * 带有命名空间的仓库名
   */
  name_with_namespace?: string;
  /**
   * 路径
   */
  path?: string;
  /**
   * 仓库公开与否。0：闭源、1：开源、2：内部开源
   */
  public?: number;
};

