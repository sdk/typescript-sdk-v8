/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 更新企业导航栏设置
 */
export type EnterpriseNavigate = {
  /**
   * 导航卡 id
   */
  id?: number;
  /**
   * 导航卡名称
   */
  name?: string;
  /**
   * 导航卡图标
   */
  icon?: string;
  /**
   * 排序
   */
  serial?: number;
  /**
   * 是否显示
   */
  active?: boolean;
  /**
   * 是否禁止修改
   */
  fixed?: boolean;
};

