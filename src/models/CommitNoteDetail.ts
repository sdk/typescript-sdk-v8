/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Object } from './Object';
import type { Reaction } from './Reaction';
import type { Timestamp } from './Timestamp';
import type { UserWithRemark } from './UserWithRemark';

/**
 * 评论、回复 Commit
 */
export type CommitNoteDetail = {
  /**
   * 评论的 id
   */
  id?: number;
  /**
   * 评论类型
   */
  type?: string;
  /**
   * 评论发起人
   */
  author?: UserWithRemark;
  /**
   * 评论内容 (markdown 格式)
   */
  content?: string;
  /**
   * 评论内容 (html 格式)
   */
  content_html?: string;
  /**
   * 代码建议汇集信息{changed: boolean 代码是否不一致，raw: string 建议的 code}
   */
  suggestions?: any[];
  /**
   * 表态
   */
  reactions?: Reaction;
  /**
   * 评论编辑、删除权限
   */
  operating?: Object;
  /**
   * 创建时间
   */
  created_at?: Timestamp;
  /**
   * 更新时间
   */
  updated_at?: Timestamp;
  /**
   * 代码行标记
   */
  line_code?: string;
  /**
   * 评论引用的代码块 id
   */
  diff_position_id?: number;
  /**
   * 上级评论的 ids，上下级关系为树组 index 顺序
   */
  ancestry_ids?: any[];
  /**
   * 代码评论是否过期
   */
  outdated?: boolean;
  /**
   * 代码评论是否解决
   */
  resolved?: boolean;
  /**
   * 代码评论信息
   */
  position?: Object;
  /**
   * 上级评论 id，评论人信息
   */
  parent?: Object;
  /**
   * 评论解决人
   */
  resolved_user?: UserWithRemark;
};

