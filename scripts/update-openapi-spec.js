const https = require('https');
const fs = require('fs');

const specUrl = 'https://gitee.com/api/v8/doc_json';

https.get(specUrl, (response) => {
    let responseData = '';
    response.on('data', (chunk) => {
        responseData += chunk;
    });
    response.on('end', () => {
        let data = JSON.parse(responseData);
        let fileName = `openapi-spec.json`;
        let fileContent = JSON.stringify(data);
        fs.writeFileSync(fileName, fileContent);
        console.log(`OpenAPI spec file was saved as ${fileName}`);
    });
}).on('error', (error) => {
    console.error('Error downloading JSON file:', error);
});