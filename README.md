## Introduce

TypeScript SDK for Gitee Enterprise OpenAPI(Gitee OpenAPI V8)

## Usage

```bash
npm i @gitee/typescript-sdk-v8
```

## LICENSE

TypeScript SDK for Gitee Enterprise OpenAPI is released under the [MIT License](https://opensource.org/licenses/MIT).